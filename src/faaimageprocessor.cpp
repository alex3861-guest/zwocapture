/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "faaimageprocessor.h"

faaImageProcessor::faaImageProcessor()
{

}

faaImageProcessor::IPR_ERRORS faaImageProcessor::faa16_BitReduction(uchar *ImageBuffer, long bufferSize, int bitNumber)
{
    uint16_t lsb,msb,result;
    for(int i=0;i < bufferSize;i+=2)
    {
        lsb = ImageBuffer[i];
        msb = ImageBuffer[i+1];
        result = uint16_t((lsb>>bitNumber)+(msb<<(8-bitNumber)));
        ImageBuffer[i]=result&255;
        ImageBuffer[i+1]= (result>>8)&255;
    }
    return IPR_SUCCEED;
}
