/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FAAIMAGEPROCESSOR_H
#define FAAIMAGEPROCESSOR_H

#include <inttypes.h>
#include <qglobal.h>

class faaImageProcessor
{
public:
    faaImageProcessor();

    typedef enum IPR_ERRORS{
        IPR_SUCCEED = 0,
        IPR_COMMON_ERROR
    }IPR_ERRORS;

    IPR_ERRORS faa16_BitReduction(uchar *ImageBuffer, long bufferSize, int bitNumber);
};

#endif // FAAIMAGEPROCESSOR_H
