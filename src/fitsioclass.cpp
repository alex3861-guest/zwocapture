/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fitsioclass.h"

FitsIOClass::FitsIOClass(QString FullFilename, QString Comments, int Width, int Height, ASI_IMG_TYPE imgType)
{
    std::string vFileName = FullFilename.toStdString();
    vCommetes = Comments;
    vWidth = Width;
    vHeight = Height;
    vImgType = imgType;
    vNaxes[0] = Width;
    vNaxes[1] = Height;
    vNElements = Width*Height;
    if(imgType==ASI_IMG_RGB24)
    {
        vNaxis = 3;
        vNaxes[2] = 3;
    }
    else
    {
        vNaxis = 2;
        vNaxes[2] = 1;
    }
    if(imgType==ASI_IMG_RAW16)
        vBitPix = USHORT_IMG;
    else
        vBitPix = BYTE_IMG;

    fits_create_file(&vFile,vFileName.c_str(),&vStatus);
}

void FitsIOClass::AddImage(uchar* Buffer)
{
    fits_create_img(vFile,vBitPix,vNaxis,vNaxes,&vStatus);
    if(vImgType==ASI_IMG_RAW8)
    {
        fits_write_img(vFile,TBYTE,1,vNElements,Buffer,&vStatus);
    }
    if(vImgType==ASI_IMG_RAW16)
    {
        fits_write_img(vFile,TUSHORT,1,vNElements,Buffer,&vStatus);
    }
    if(vImgType==ASI_IMG_RGB24)
    {
        uchar* outBuffer;
        outBuffer = new uchar[vNElements*3];
        int r=0,g=int(vNElements),b=int(vNElements+vNElements);
        for(int i=0;i<vNElements*3;i+=3)
        {
            outBuffer[r++] = Buffer[i+2];
            outBuffer[g++] = Buffer[i+1];
            outBuffer[b++] = Buffer[i];
        }
        fits_write_img(vFile,TBYTE,1,vNElements*3,outBuffer,&vStatus);
        delete [] outBuffer;
    }
    if(vImgType==ASI_IMG_Y8)
    {
        fits_write_img(vFile,TBYTE,1,vNElements,Buffer,&vStatus);
    }
}

void FitsIOClass::SaveFile()
{
    fits_close_file(vFile,&vStatus);
}
