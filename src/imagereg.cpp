/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "imagereg.h"

imageReg::imageReg()
{
    vParams.blobColor = 255;
    vMaxKeypointsCount = 20;
    vMatchingTolerance = 2.0;
    vGaussK = cv::Size(1,1);
}

void imageReg::setBaseImage(QImage *image)
{
    QImage tmpImg = image->convertToFormat(QImage::Format_Grayscale8);
    uchar *data;
    data = tmpImg.bits();
    cv::Mat tmpMat = cv::Mat(tmpImg.height(),tmpImg.width(),CV_8UC1,data);
    if(vGaussK.height > 1)
        cv::GaussianBlur(tmpMat,vBaseMat,vGaussK,0.0);
    else
        vBaseMat = tmpMat.clone();
    vMatColor = cv::Mat(tmpImg.height(),tmpImg.width(),CV_8UC(3));
    BaseImgProc();
}

void imageReg::setGaussKsize(int gaussK)
{
    vGaussK.height = gaussK;
    vGaussK.width = gaussK;
}

void imageReg::setParams(cv::SimpleBlobDetector::Params params, ulong maxKeypointsCount, float tolerance)
{
    vParams = params;
    vMaxKeypointsCount = maxKeypointsCount;
    vMatchingTolerance = tolerance;
}

QImage *imageReg::getBaseImage()
{
    uchar *data;
    data = vBaseMat.data;
    QImage *image;
    int w,h;
    w = vBaseMat.cols;
    h = vBaseMat.rows;
    image = new QImage(data,w,h,QImage::Format_Grayscale8);
    return image;
}

QPointF imageReg::getShift(QImage *image)
{
//    std::sort()
    QImage tmpImg = image->convertToFormat(QImage::Format_Grayscale8);
    uchar   *data;
    data = tmpImg.bits();
    vCurrentMat = cv::Mat(image->height(),image->width(),CV_8UC1,data);
    if(vGaussK.height > 1)
        cv::GaussianBlur(vCurrentMat,vCurrentMat,vGaussK,0.0);
    cv::Ptr<cv::SimpleBlobDetector> detector;
    detector = cv::SimpleBlobDetector().create(vParams);
    cv::Mat currentDescriptors;
    QList<cv::DMatch>   matches;
    detector->detect(vCurrentMat,vCurrentKeyPoints);
    keypointsReduction(vCurrentKeyPoints,vMaxKeypointsCount);
    compute(vCurrentKeyPoints,currentDescriptors);
    match(vBaseDescriptors,currentDescriptors,vMatchingTolerance,&matches);
//    delete detector;

    QPointF shift = QPointF(0.0,0.0);
    if(matches.count()==0)
    {
        shift.setX(SNAN);
        shift.setY(SNAN);
        return shift;
    }
    std::vector<double> dX(ulong(matches.count())),dY(ulong(matches.count()));
    for(int i=0;i<matches.count();i++)
    {
        dX[ulong(i)] = double(vCurrentKeyPoints[ulong(matches[i].queryIdx)].pt.x - vBaseKeyPoints[ulong(matches[i].trainIdx)].pt.x);
        dY[ulong(i)] = double(vCurrentKeyPoints[ulong(matches[i].queryIdx)].pt.y - vBaseKeyPoints[ulong(matches[i].trainIdx)].pt.y);
    }
    std::sort(dX.begin(),dX.end());
    std::sort(dY.begin(),dY.end());
    ulong k = ulong(matches.count()/2);
    if((ulong(matches.count())-k)!=k)
    {
        shift.setX(dX[k]);
        shift.setY(dY[k]);
    }
    else
    {
        shift.setX((dX[k-1]+dX[k])/2.0);
        shift.setY((dY[k-1]+dY[k])/2.0);
    }
    return shift;
}

QImage *imageReg::getKeyPointsImage(bool baseImage)
{
    QImage *image;
    if(baseImage)
        image = drawKeypoints(vBaseMat,vBaseKeyPoints);
    else
        image = drawKeypoints(vCurrentMat,vCurrentKeyPoints);
    return image;
}

QJsonObject imageReg::getProfile()
{

}

QImage *imageReg::testParams(int *keypointscount)
{
    BaseImgProc();
    *keypointscount = int(vBaseKeyPoints.size());
    return drawKeypoints(vBaseMat,vBaseKeyPoints);
}

void imageReg::setMatchingTolerance(float tolerance)
{
    vMatchingTolerance = tolerance;
}

void imageReg::setProfile(QJsonObject *object)
{

}

void imageReg::BaseImgProc()
{
    cv::SimpleBlobDetector *detector;
    detector = cv::SimpleBlobDetector().create(vParams);
    detector->detect(vBaseMat,vBaseKeyPoints);
    keypointsReduction(vBaseKeyPoints,vMaxKeypointsCount);
    compute(vBaseKeyPoints,vBaseDescriptors);
    delete detector;
}

//void imageReg::keypointsReduction(std::vector<cv::KeyPoint> &keypoints, int count)
void imageReg::keypointsReduction(std::vector<cv::KeyPoint> &keypoints, ulong count)
{
    ulong inputCount = (keypoints.size());
    if(inputCount<=count)
        return;
    std::vector<cv::KeyPoint> tmpKpt;
    tmpKpt.resize((count));
    for(ulong i=0;i<(count);i++)
    {
        float max = 0;
        ulong Ind = 0;
        for(ulong j=0;j<(inputCount);j++)
        {
            if(keypoints[j].size>max)
            {
                Ind = j;
                max = keypoints[j].size;
            }
        }
        tmpKpt[i] = keypoints[Ind];
        keypoints[Ind].size = 0;
    }
    keypoints = tmpKpt;
}

void imageReg::compute(std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors)
{
    //descriptor - 2 dimentional matrix with 5 chanels.
    //channel 0 - each row contain index of the keypoint as the first element and indexes of neighbours
    //channel 1 - contain respective delta x
    //channel 2 - respective delta y
    //channel 3 - respective size
    //channel 4 - respective dx*dx+dy*dy

    int keypointsCount = int(keypoints.size());
    descriptors = cv::Mat(keypointsCount,keypointsCount,CV_32SC(5));

    for(int i=0;i<keypointsCount;i++)
    {
        ulong i1=ulong(i);
        descriptors.at<cv::Vec<float,5>>(i,0)[0] = i;
        descriptors.at<cv::Vec<float,5>>(i,0)[1] = 0;
        descriptors.at<cv::Vec<float,5>>(i,0)[2] = 0;
        descriptors.at<cv::Vec<float,5>>(i,0)[3] = int(keypoints[i1].size);
        descriptors.at<cv::Vec<float,5>>(i,0)[4] = 0;
        int k = 1;
        for(ulong j=0;j<ulong(keypointsCount);j++)
        {
            if(int(j)==i)
                continue;
            descriptors.at<cv::Vec<float,5>>(i,k)[0] = j;
            descriptors.at<cv::Vec<float,5>>(i,k)[1] = int(keypoints[j].pt.x - keypoints[i1].pt.x);
            descriptors.at<cv::Vec<float,5>>(i,k)[2] = keypoints[j].pt.y - keypoints[i1].pt.y;
            descriptors.at<cv::Vec<float,5>>(i,k)[3] = keypoints[j].size;
            descriptors.at<cv::Vec<float,5>>(i,k)[4] = sqrtf(powf(descriptors.at<cv::Vec<float,5>>(i,k)[1],2)+
                                                            powf(descriptors.at<cv::Vec<float,5>>(i,k)[2],2));
            k++;
        }
    }
}

void imageReg::match(cv::Mat &desc1, cv::Mat &desc2, float tolerance, QList<cv::DMatch> *matches)
{
    int descRow1,descRow2,descCol1,descCol2;
    cv::Mat tmpMatches;

    descRow1 = desc1.rows;
    descRow2 = desc2.rows;
    descCol1 = desc1.cols;
    descCol2 = desc2.cols;
    tmpMatches = cv::Mat(descRow1,descRow2,CV_16SC1);

    for(int i=0;i<descRow1;i++)
    {
        for(int j=0;j<descRow2;j++)
        {
            short pointMatches = 0;
            for(int i1=1;i1<descCol1;i1++)
            {
                for(int i2=1;i2<descCol2;i2++)
                {
                    float error = fabsf(desc1.at<cv::Vec<float,5>>(i,i1)[4]-desc2.at<cv::Vec<float,5>>(j,i2)[4]);
                    if(error < tolerance)
                    {
                        pointMatches++;
                    }
                }
            }
            tmpMatches.at<short>(i,j) = pointMatches;
        }
    }

    if((descRow1<4) |
       (descRow2<4))
    {
        for(int i=0;i<descRow1;i++)
        {
            for(int j=0;j<descRow2;j++)
            {
                int pointMatches = 0;
                for(int i1=1;i1<descCol1;i1++)
                {
                    for(int i2=1;i2<descCol2;i2++)
                    {
                        float error = fabsf(desc1.at<cv::Vec<float,5>>(i,i1)[3]-desc2.at<cv::Vec<float,5>>(j,i2)[3]);
                        if(error < tolerance)
                        {
                            pointMatches++;
                        }
                    }
                }
                tmpMatches.at<short>(i,j) += pointMatches;
            }
        }
    }

    if((descRow1==1) |
       (descRow2==1))
    {
        for(int i=0;i<descRow1;i++)
        {
            for(int j=0;j<descRow2;j++)
            {
                short pointMatches = 0;
                float error = fabsf(desc1.at<cv::Vec<float,5>>(i,0)[3]-desc2.at<cv::Vec<float,5>>(j,0)[3]);
                if(error < tolerance)
                {
                    pointMatches=2;
                }
                tmpMatches.at<short>(i,j) = pointMatches;
            }
        }
    }

    int MatchThreshold;
    int k;
    if(descRow1<descRow2)
        k = descRow1;
    else
        k = descRow2;
    MatchThreshold = int(k*0.75);
    matches->clear();
    for(int i=0;i<descRow1;i++)
    {
        for(int j=0;j<descRow2;j++)
        {
            if(tmpMatches.at<short>(i,j)>MatchThreshold)
            {
                cv::DMatch match;
                match.imgIdx = 0;
                match.trainIdx = i;
                match.queryIdx = j;
                match.distance = .0;
                matches->append(match);
            }
        }
    }
}

void imageReg::convertToColorMat(cv::Mat &mat)
{
    uchar *data,*data1;
    data1 = vMatColor.data;
    data = mat.data;
    //copy data from Mat to Color Mat
    ulong tot1;
    tot1 = mat.total();
    int k = 0;
    for(ulong i=0;i<tot1;i++)
    {
        for(int j=0;j<3;j++)
        {
            data1[k]=data[i];
            k++;
        }
    }

}

QImage *imageReg::drawKeypoints(cv::Mat &mat, std::vector<cv::KeyPoint> &kpts)
{
    uchar *data,*data1;
    data1 = vMatColor.data;
    data = mat.data;
    //copy data from Base Mat to Color Mat
    ulong total;
    total = mat.total();
    int k = 0;
    for(ulong i=0;i<total;i++)
    {
        for(int j=0;j<3;j++)
        {
            data1[k]=data[i];
            k++;
        }
    }
    cv::drawKeypoints(vMatColor,kpts,vMatColor,cv::Scalar::all(-1),
                      cv::DrawMatchesFlags::DRAW_OVER_OUTIMG|cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
    QImage *image;
    image = new QImage(vMatColor.data,vMatColor.cols,vMatColor.rows,QImage::Format_RGB888);
    return image;
}

