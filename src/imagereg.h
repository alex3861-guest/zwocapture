/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IMAGEREG_H
#define IMAGEREG_H

#include <QImage>
#include <QJsonObject>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>


class imageReg
{
    public:
        imageReg();
        void    setBaseImage(QImage *image);
        void    setGaussKsize(int gaussK);
        void    setParams(cv::SimpleBlobDetector::Params params,ulong maxKeypointsCount=20, float tolerance=2.0);
        QImage *getBaseImage();
        QPointF getShift(QImage *image);
        QImage *getKeyPointsImage(bool baseImage = true);
        QJsonObject getProfile();
        QImage *testParams(int *keypointscount);
        void    setMatchingTolerance(float tolerance);
        void    setProfile(QJsonObject *object);

    private:
        void BaseImgProc();
//        void keypointsReduction(std::vector<cv::KeyPoint>& keypoints,int count);
        void keypointsReduction(std::vector<cv::KeyPoint>& keypoints,ulong count);
        void compute(std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors);
        void match(cv::Mat& desc1, cv::Mat& desc2, float tolerance, QList<cv::DMatch>*  matches);
        void convertToColorMat(cv::Mat& mat);
        QImage *drawKeypoints(cv::Mat& mat,std::vector<cv::KeyPoint>& kpts);

        cv::Mat                         vBaseMat;
        cv::Mat                         vMatColor;
        cv::Mat                         vCurrentMat;
        cv::SimpleBlobDetector::Params  vParams;
        std::vector<cv::KeyPoint>        vBaseKeyPoints,vCurrentKeyPoints;
        cv::Mat                         vBaseDescriptors;
        ulong                           vMaxKeypointsCount;
        float                           vMatchingTolerance;
        cv::Size                        vGaussK;

        //debug
        QImage  debImage;
};

#endif // IMAGEREG_H
