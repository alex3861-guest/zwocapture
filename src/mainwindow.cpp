/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    vVideoStreamingBox = new zwoStreaming(this);
    connect(vVideoStreamingBox, SIGNAL(finished(int)),this,SLOT(on_vVideoStreamingBox_closed(int)));
    connect(vVideoStreamingBox,SIGNAL(ShootingConpleted()),SLOT(on_ShootingCompleted()));

    vTimer = new QTimer();
    vTimer->setInterval(2000);
    vTimer->setSingleShot(false);
    vTimer->stop();
    connect(vTimer, SIGNAL(timeout()), this, SLOT(onTimer()));

    vHomePath = vDir.homePath();
    vConfigFileName = vHomePath+"/.Faa/zwocapture.conf";
    LoadProfile();

    vFileDialog = new QFileDialog();
    vFileDialog->setFileMode(QFileDialog::DirectoryOnly);
    connect(vFileDialog, SIGNAL(accepted()),this,SLOT(on_FileSelected()));



//  Name lables array initialization
    aControlNames[0] = ui->lbControlName;
    aControlNames[1] = ui->lbControlName_2;
    aControlNames[2] = ui->lbControlName_3;
    aControlNames[3] = ui->lbControlName_4;
    aControlNames[4] = ui->lbControlName_5;
    aControlNames[5] = ui->lbControlName_6;
    aControlNames[6] = ui->lbControlName_7;
    aControlNames[7] = ui->lbControlName_8;
    aControlNames[8] = ui->lbControlName_9;
    aControlNames[9] = ui->lbControlName_10;
    aControlNames[10] = ui->lbControlName_11;
    aControlNames[11] = ui->lbControlName_12;
    aControlNames[12] = ui->lbControlName_13;
    aControlNames[13] = ui->lbControlName_14;
    aControlNames[14] = ui->lbControlName_15;
    aControlNames[15] = ui->lbControlName_16;
    aControlNames[16] = ui->lbControlName_17;
    aControlNames[17] = ui->lbControlName_18;
    aControlNames[18] = ui->lbControlName_19;
    aControlNames[19] = ui->lbControlName_20;
    aControlNames[20] = ui->lbControlName_21;
    aControlNames[21] = ui->lbControlName_22;
    aControlNames[22] = ui->lbControlName_23;

//  MinMax values lable array initialization
    aMinMaxVal[0] = ui->lbMinMaxVal;
    aMinMaxVal[1] = ui->lbMinMaxVal_2;
    aMinMaxVal[2] = ui->lbMinMaxVal_3;
    aMinMaxVal[3] = ui->lbMinMaxVal_4;
    aMinMaxVal[4] = ui->lbMinMaxVal_5;
    aMinMaxVal[5] = ui->lbMinMaxVal_6;
    aMinMaxVal[6] = ui->lbMinMaxVal_7;
    aMinMaxVal[7] = ui->lbMinMaxVal_8;
    aMinMaxVal[8] = ui->lbMinMaxVal_9;
    aMinMaxVal[9] = ui->lbMinMaxVal_10;
    aMinMaxVal[10] = ui->lbMinMaxVal_11;
    aMinMaxVal[11] = ui->lbMinMaxVal_12;
    aMinMaxVal[12] = ui->lbMinMaxVal_13;
    aMinMaxVal[13] = ui->lbMinMaxVal_14;
    aMinMaxVal[14] = ui->lbMinMaxVal_15;
    aMinMaxVal[15] = ui->lbMinMaxVal_16;
    aMinMaxVal[16] = ui->lbMinMaxVal_17;
    aMinMaxVal[17] = ui->lbMinMaxVal_18;
    aMinMaxVal[18] = ui->lbMinMaxVal_19;
    aMinMaxVal[19] = ui->lbMinMaxVal_20;
    aMinMaxVal[20] = ui->lbMinMaxVal_21;
    aMinMaxVal[21] = ui->lbMinMaxVal_22;
    aMinMaxVal[22] = ui->lbMinMaxVal_23;

//  Current values lable array initialization
    aCurrentVal[0] = ui->lbCurrentValue;
    aCurrentVal[1] = ui->lbCurrentValue_2;
    aCurrentVal[2] = ui->lbCurrentValue_3;
    aCurrentVal[3] = ui->lbCurrentValue_4;
    aCurrentVal[4] = ui->lbCurrentValue_5;
    aCurrentVal[5] = ui->lbCurrentValue_6;
    aCurrentVal[6] = ui->lbCurrentValue_7;
    aCurrentVal[7] = ui->lbCurrentValue_8;
    aCurrentVal[8] = ui->lbCurrentValue_9;
    aCurrentVal[9] = ui->lbCurrentValue_10;
    aCurrentVal[10] = ui->lbCurrentValue_11;
    aCurrentVal[11] = ui->lbCurrentValue_12;
    aCurrentVal[12] = ui->lbCurrentValue_13;
    aCurrentVal[13] = ui->lbCurrentValue_14;
    aCurrentVal[14] = ui->lbCurrentValue_15;
    aCurrentVal[15] = ui->lbCurrentValue_16;
    aCurrentVal[16] = ui->lbCurrentValue_17;
    aCurrentVal[17] = ui->lbCurrentValue_18;
    aCurrentVal[18] = ui->lbCurrentValue_19;
    aCurrentVal[19] = ui->lbCurrentValue_20;
    aCurrentVal[20] = ui->lbCurrentValue_21;
    aCurrentVal[21] = ui->lbCurrentValue_22;
    aCurrentVal[22] = ui->lbCurrentValue_23;

//  Auto status values lable array initialization
    aAutoStatus[0] = ui->lbAutoStatus;
    aAutoStatus[1] = ui->lbAutoStatus_2;
    aAutoStatus[2] = ui->lbAutoStatus_3;
    aAutoStatus[3] = ui->lbAutoStatus_4;
    aAutoStatus[4] = ui->lbAutoStatus_5;
    aAutoStatus[5] = ui->lbAutoStatus_6;
    aAutoStatus[6] = ui->lbAutoStatus_7;
    aAutoStatus[7] = ui->lbAutoStatus_8;
    aAutoStatus[8] = ui->lbAutoStatus_9;
    aAutoStatus[9] = ui->lbAutoStatus_10;
    aAutoStatus[10] = ui->lbAutoStatus_11;
    aAutoStatus[11] = ui->lbAutoStatus_12;
    aAutoStatus[12] = ui->lbAutoStatus_13;
    aAutoStatus[13] = ui->lbAutoStatus_14;
    aAutoStatus[14] = ui->lbAutoStatus_15;
    aAutoStatus[15] = ui->lbAutoStatus_16;
    aAutoStatus[16] = ui->lbAutoStatus_17;
    aAutoStatus[17] = ui->lbAutoStatus_18;
    aAutoStatus[18] = ui->lbAutoStatus_19;
    aAutoStatus[19] = ui->lbAutoStatus_20;
    aAutoStatus[20] = ui->lbAutoStatus_21;
    aAutoStatus[21] = ui->lbAutoStatus_22;
    aAutoStatus[22] = ui->lbAutoStatus_23;

//  Auto set values check box array initialization
    aAutoSet[0] = ui->cbAutoSet;
    aAutoSet[1] = ui->cbAutoSet_2;
    aAutoSet[2] = ui->cbAutoSet_3;
    aAutoSet[3] = ui->cbAutoSet_4;
    aAutoSet[4] = ui->cbAutoSet_5;
    aAutoSet[5] = ui->cbAutoSet_6;
    aAutoSet[6] = ui->cbAutoSet_7;
    aAutoSet[7] = ui->cbAutoSet_8;
    aAutoSet[8] = ui->cbAutoSet_9;
    aAutoSet[9] = ui->cbAutoSet_10;
    aAutoSet[10] = ui->cbAutoSet_11;
    aAutoSet[11] = ui->cbAutoSet_12;
    aAutoSet[12] = ui->cbAutoSet_13;
    aAutoSet[13] = ui->cbAutoSet_14;
    aAutoSet[14] = ui->cbAutoSet_15;
    aAutoSet[15] = ui->cbAutoSet_16;
    aAutoSet[16] = ui->cbAutoSet_17;
    aAutoSet[17] = ui->cbAutoSet_18;
    aAutoSet[18] = ui->cbAutoSet_19;
    aAutoSet[19] = ui->cbAutoSet_20;
    aAutoSet[20] = ui->cbAutoSet_21;
    aAutoSet[21] = ui->cbAutoSet_22;
    aAutoSet[22] = ui->cbAutoSet_23;

//  set values line edits array initialization
    aSetValues[0] = ui->leSetVal;
    aSetValues[1] = ui->leSetVal_2;
    aSetValues[2] = ui->leSetVal_3;
    aSetValues[3] = ui->leSetVal_4;
    aSetValues[4] = ui->leSetVal_5;
    aSetValues[5] = ui->leSetVal_6;
    aSetValues[6] = ui->leSetVal_7;
    aSetValues[7] = ui->leSetVal_8;
    aSetValues[8] = ui->leSetVal_9;
    aSetValues[9] = ui->leSetVal_10;
    aSetValues[10] = ui->leSetVal_11;
    aSetValues[11] = ui->leSetVal_12;
    aSetValues[12] = ui->leSetVal_13;
    aSetValues[13] = ui->leSetVal_14;
    aSetValues[14] = ui->leSetVal_15;
    aSetValues[15] = ui->leSetVal_16;
    aSetValues[16] = ui->leSetVal_17;
    aSetValues[17] = ui->leSetVal_18;
    aSetValues[18] = ui->leSetVal_19;
    aSetValues[19] = ui->leSetVal_20;
    aSetValues[20] = ui->leSetVal_21;
    aSetValues[21] = ui->leSetVal_22;
    aSetValues[22] = ui->leSetVal_23;

    //EFW controls init
    vFilters.append(Filter{ui->leFilter1,ui->chbFilter1,ui->dsFilterExp1});
    vFilters.append(Filter{ui->leFilter2,ui->chbFilter2,ui->dsFilterExp2});
    vFilters.append(Filter{ui->leFilter3,ui->chbFilter3,ui->dsFilterExp3});
    vFilters.append(Filter{ui->leFilter4,ui->chbFilter4,ui->dsFilterExp4});
    vFilters.append(Filter{ui->leFilter5,ui->chbFilter5,ui->dsFilterExp5});
    vFilters.append(Filter{ui->leFilter6,ui->chbFilter6,ui->dsFilterExp6});
    vFilters.append(Filter{ui->leFilter7,ui->chbFilter7,ui->dsFilterExp7});
    vFilters.append(Filter{ui->leFilter8,ui->chbFilter8,ui->dsFilterExp8});
    DisableEFWcontrols();
    RenameEFWcontrol();
    vEFWslotsNumber = 0;
    vEFWconnected = false;
    vStartShooting = false;
    vEFWmenu = new QMenu();
    actionSetEFWexposure = vEFWmenu->addAction("set current");
    actionSetAllEFWexposure = vEFWmenu->addAction("set current for all");
    connect(actionSetAllEFWexposure,SIGNAL(triggered(bool)),this,SLOT(setAllEFWexposure()));
    connect(actionSetEFWexposure,SIGNAL(triggered(bool)),this,SLOT(setEFWexposure()));

    vGuidingPorts = 0;
    vUSB2ST4connected = false;
    this->setWindowTitle("ZWO capture");
    ui->cbImageFormat->addItems(QStringList()<<"FITS single"
                                <<"FITS multiple"
                                <<"SER"
                                <<"TIFF"
                                <<"PNG");
    ui->cbVideoFormat->addItem("SER");
    ui->cbImageFormat->setCurrentIndex(2);

    SetTabsEnabled(false);
    ui->tabWidget->setTabEnabled(4,false);
    ui->tabWidget->setCurrentIndex(0);
    fHideCameraControls();
    fCameraScan();
    EFWScan();
}

MainWindow::~MainWindow()
{
    SaveProfile();
    delete ui;
}

void MainWindow::SetTabsEnabled(bool enabled)
{
    if(enabled)
    {
        ui->tabWidget->setTabEnabled(1,true);
        ui->tabWidget->setTabEnabled(2,true);
        ui->tabWidget->setTabEnabled(3,true);
        ui->tabWidget->setTabEnabled(5,true);
    }
    else
    {
        ui->tabWidget->setTabEnabled(1,false);
        ui->tabWidget->setTabEnabled(2,false);
        ui->tabWidget->setTabEnabled(3,false);
        ui->tabWidget->setTabEnabled(5,false);
    }
}

//  Scaning system to find any connected ZWO camaras and getting info parameters from
//  the first one if any camera is connected
void    MainWindow::fCameraScan()
{
    vCamerasNumber = ASIGetNumOfConnectedCameras();
    vString = vString.setNum(vCamerasNumber);
    ui->lbCamerasNumber->setText(vString);
    for(int i=0;i < vCamerasNumber;i++)
    {
        vError = ASIGetCameraProperty(&vCameraInfo,i);
        if(vError == ASI_SUCCESS)
        {
            vString = vString.fromUtf8(vCameraInfo.Name);
            ui->cbCameras->addItem(vString);
        }
        else
            fErrorProcessing(vError);
    };
    if(vCamerasNumber > 0)
        fGetCameraInfo(0);
    else
    {
//      Set all camera's infor parameters to Undefined state

        ui->lbName->setText(vString.fromStdString("Undefined"));
        ui->lbID->setText(vString.fromStdString("Undefined"));
        ui->lbMaxHeight->setText(vString.fromStdString("Undefined"));
        ui->lbMaxWidth->setText(vString.fromStdString("Undefined"));
        ui->lbColorCamera->setText(vString.fromStdString("Undefined"));
        ui->lbBayerPattern->setText(vString.fromStdString("Undefined"));
        ui->lbBins->setText(vString.fromStdString("Undefined"));
        ui->lbVideoFormats->setText(vString.fromStdString("Undefined"));
        ui->lbMechanicalShutter->setText(vString.fromStdString("Undefined"));
        ui->lbST4Port->setText(vString.fromStdString("Undefined"));
        ui->lbCoolerCamera->setText(vString.fromStdString("Undefined"));
        ui->lbUSB3Status->setText(vString.fromStdString("Undefined"));
        ui->lbUSB3Support->setText(vString.fromStdString("Undefined"));
        ui->lbPixelSize->setText(vString.fromStdString("Undefined"));
        ui->lbSystemGain->setText(vString.fromStdString("Undefined"));
        ui->cbCameras->clear();
    }
}

QString MainWindow::ComposeFileName()
{
    QString s = ui->leWorkFolder->text()+"/"+ui->leFileName->text();
    if(vEFWconnected)
    {
        s += "-"+vFilters[vFiltersInvolved[vEFWInvolvedIndex]].Name->text();
    }
    if(ui->chbGain->isChecked())
        s += "-g"+QString().setNum(vGain);
    if(ui->chbExposure->isChecked())
    {
        if(vExposure>1000000)
            s += "-e"+QString().setNum(vExposure/1000000)+"s";
        else
            s += "-e"+QString().setNum(vExposure/1000)+"ms";
    }
    if(ui->chbFlip->isChecked())
        s += "-f"+QString().setNum(vFlip);
    if(ui->chbTemperature->isChecked())
    {
        if(vCooler==1)
            s += "-t"+QString().setNum(vTemperature);
    }
    if(ui->chbImgFormat->isChecked())
    {
        switch (vImageType) {
        case ASI_IMG_RAW8: s += "-RAW8";
            break;
        case ASI_IMG_RAW16: s+= "-RAW16";
            break;
        case ASI_IMG_RGB24: s += "-RGB";
            break;
        case ASI_IMG_Y8: s += "-Y8";
            break;
        case ASI_IMG_END: break;
        }
    }
    if(ui->chbAlignment->isChecked())
        s += "-al"+QString().setNum(16-vCameraInfo.BitDepth);
    if(ui->chbTimeStamp->isChecked())
        s += "-"+QDateTime().currentDateTime().toString("yyyyMMddhhmmss");
    return s;
}

void MainWindow::fGetCameraInfo(int vCameraNumber)
{
    QString vStringLocal;
    vError = ASIGetCameraProperty(&vCameraInfo,vCameraNumber);
    if(vError == ASI_SUCCESS)
    {
        ui->lbName->setText(vString.fromUtf8(vCameraInfo.Name));
        ui->lbID->setText(vString.setNum(vCameraInfo.CameraID));
        ui->lbMaxHeight->setText(vString.setNum(vCameraInfo.MaxHeight));
        ui->lbMaxHeight1->setText(vString.setNum(vCameraInfo.MaxHeight));
        vImageMaxHeight = vCameraInfo.MaxHeight;
        ui->lbMaxWidth->setText(vString.setNum(vCameraInfo.MaxWidth));
        ui->lbMaxWidth1->setText(vString.setNum(vCameraInfo.MaxWidth));
        vImageMaxWidth = vCameraInfo.MaxWidth;
        ui->lbColorCamera->setText(fBoolToString(vCameraInfo.IsColorCam));
        switch (vCameraInfo.BayerPattern)
        {
            case ASI_BAYER_BG: vString = vString.fromStdString("Bayer BG");
                break;
            case ASI_BAYER_GB: vString = vString.fromStdString("Bayer GB");
                break;
            case ASI_BAYER_GR: vString = vString.fromStdString("Bayer GR");
                break;
            case ASI_BAYER_RG: vString = vString.fromStdString("Bayer RG");
                break;
//            default: vString = vString.fromStdString("Not applicable");
//                break;
        }
        ui->lbBayerPattern->setText(vString);
        vString.clear();
        ui->cbSetBin->clear();
        for(int i = 0;i < 16;i++)
        {
            if(vCameraInfo.SupportedBins[i] == 0)
                break;
            vStringLocal.setNum(vCameraInfo.SupportedBins[i]);
            vString.append(vStringLocal+";");
            ui->cbSetBin->addItem(vStringLocal);
        }
        ui->lbBins->setText(vString);
        vStringLocal.clear();
        ui->cbSetImgType->clear();
        for(int i = 0;i < 8;i++)
        {
            if(vCameraInfo.SupportedVideoFormat[i] == ASI_IMG_END)
                break;
            vString = fImgTypeToString(vCameraInfo.SupportedVideoFormat[i]);
            vStringLocal.append(vString + ";");
            ui->cbSetImgType->addItem(vString);
        }
        ui->lbVideoFormats->setText(vStringLocal);
        ui->lbMechanicalShutter->setText(fBoolToString(vCameraInfo.MechanicalShutter));
        ui->lbST4Port->setText(fBoolToString(vCameraInfo.ST4Port));
        ui->lbCoolerCamera->setText(fBoolToString(vCameraInfo.IsCoolerCam));
        ui->lbUSB3Status->setText(fBoolToString(vCameraInfo.IsUSB3Host));
        ui->lbUSB3Support->setText(fBoolToString(vCameraInfo.IsUSB3Camera));
        ui->lbPixelSize->setText(vString.setNum(vCameraInfo.PixelSize));
        ui->lbSystemGain->setText(vString.setNum(vCameraInfo.ElecPerADU));

    }
    fErrorProcessing(vError);
}

void MainWindow::fErrorProcessing(ASI_ERROR_CODE verror)
{
    ui->statusBar->showMessage(zwoErrorProcessing(verror));
    if(verror==ASI_ERROR_CAMERA_REMOVED)
    {
        ui->lbConnectStatus->setText("Not connected");
        vVideoStreamingBox->Terminate();
    }
}

void MainWindow::fHideCameraControls()
{
    for(int i=0;i < 23;i++)
    {
        aControlNames[i]->hide();
        aMinMaxVal[i]->hide();
        aCurrentVal[i]->hide();
        aAutoStatus[i]->hide();
        aSetValues[i]->hide();
        aSetValues[i]->setEnabled(true);
        aAutoSet[i]->hide();
        aAutoSet[i]->setEnabled(true);
    }
    ui->label_41->hide();
    ui->leObserver->hide();
    ui->leInstrument->hide();
    ui->leTelescope->hide();
    ui->label_56->hide();
    ui->label_57->hide();
    ui->label_58->hide();
    ui->label_55->hide();
}

void MainWindow::fGetControlValues()
{
    QString vLocalString;
    vLocalString.clear();
    for(int i=0;i < vControlsNumber;i++)
    {
        vError = ASIGetControlCaps(vCameraInfo.CameraID,i,&vControlCups);
        if(vError == ASI_SUCCESS)
        {
            vError = ASIGetControlValue(vCameraInfo.CameraID,vControlCups.ControlType,&vCurrentControlVal,&vAutoStatus);
            if(vError == ASI_SUCCESS)
            {
                //calculate values for exposure and sensor temparature
                switch (vControlCups.ControlType)
                {
                case ASI_EXPOSURE:
                    vLocalString = vString.setNum(float(vCurrentControlVal)/1000000);
                    vExposure = int(vCurrentControlVal);
                    break;
                case ASI_TEMPERATURE:
                    vLocalString = vString.setNum(float(vCurrentControlVal)/10,'f',1);
                    vTemperature = vCurrentControlVal;
                    break;
                case ASI_GAIN:
                    vGain = vCurrentControlVal;
                    vLocalString = vString.setNum(vCurrentControlVal);
                    break;
                case ASI_COOLER_ON:
                    vCooler = vCurrentControlVal;
                    vLocalString = vString.setNum(vCurrentControlVal);
                    break;
                case ASI_FLIP:
                    vFlip = vCurrentControlVal;
                    vLocalString = vString.setNum(vCurrentControlVal);
                    break;
                default: vLocalString = vString.setNum(vCurrentControlVal);
                    break;
                }
            }
            aCurrentVal[i]->setText(vLocalString);
            aAutoStatus[i]->setText(fBoolToString(vAutoStatus));
        }
    }
    if(vError!=ASI_SUCCESS)
        fErrorProcessing(vError);
}

void MainWindow::fGetImageFormat()
{
    vError = ASIGetROIFormat(vCameraInfo.CameraID,&vImageWidth,&vImageHeight,&vImageBin,&vImageType);
    if(vError==ASI_SUCCESS)
    {
        ui->lbCurrentWidth->setText(vString.setNum(vImageWidth));
        ui->lbCurrentHeight->setText(vString.setNum(vImageHeight));
        ui->lbCurrentBin->setText(vString.setNum(vImageBin));
        ui->lbCurrentImgType->setText(fImgTypeToString(vImageType));
        vError = ASIGetStartPos(vCameraInfo.CameraID,&vStartX,&vStartY);
        if(vError==ASI_SUCCESS)
        {
            ui->lbCurrentStartX->setText(vString.setNum(vStartX));
            ui->lbCurrentStartY->setText(vString.setNum(vStartY));
        }
        if(vImageType!=ASI_IMG_RAW16)
            ui->chbRightAlignAndOffset->setEnabled(false);
        else
            ui->chbRightAlignAndOffset->setEnabled(true);
    }
    fSetRightAligning();
    fErrorProcessing(vError);
}

void MainWindow::fSetRightAligning()
{
    vVideoStreamingBox->vRightAligning = ui->chbRightAlignAndOffset->isChecked();
}

void MainWindow::fSetControlValue(int vIndex)
{
    QString vLocalSting;
    long        vValue;
    ASI_BOOL    vAuto;
    vLocalSting.clear();
    vError = ASIGetControlCaps(vCameraInfo.CameraID,vIndex,&vControlCups);
    if(vError == ASI_SUCCESS)
    {
        vLocalSting = aSetValues[vIndex]->text();
        switch (vControlCups.ControlType)
        {
            case ASI_EXPOSURE: vValue = long(vLocalSting.toFloat()*1000000);
                break;
        default: vValue = vLocalSting.toLong();
            break;
        }
        if(aAutoSet[vIndex]->isChecked())
            vAuto = ASI_TRUE;
        else
            vAuto = ASI_FALSE;

        vError = ASISetControlValue(vCameraInfo.CameraID,vControlCups.ControlType,vValue,vAuto);
    }
    fErrorProcessing(vError);
}

void MainWindow::onTimer()
{
    USB2ST4_ERROR_CODE usb2st4Error;
    vUSB2ST4Number = USB2ST4GetNum();
    if(vUSB2ST4Number>0)
    {
        if(!vUSB2ST4connected)
        {
            usb2st4Error = USB2ST4GetID(0,&vUSB2ST4_ID);
            if(usb2st4Error==USB2ST4_SUCCESS)
            {
                usb2st4Error = USB2ST4Open(vUSB2ST4_ID);
                if(usb2st4Error==USB2ST4_SUCCESS)
                {
                    vUSB2ST4connected = true;
                    ui->lbUSB3Status->setText("USB2ST4 adapter is connected");
                    vGuidingPorts |= Tracking::Guiding_USB2ST4adapter;
                }
            }
        }
    }
    else
    {
        if(vUSB2ST4connected)
            usb2st4Disconnect();
    }
    fGetControlValues();
    int position=0;
    if(vEFWconnected)
    {
        EFWGetPosition(vEFW_ID,&position);
        ui->lbEFWactualPosition->setText(QString().setNum(position+1));
    }
    if(vStartShooting)
    {
        if(vEFWconnected)
        {
            if(position==vFiltersInvolved[vEFWInvolvedIndex])
                StartShooting();
        }
        else
            StartShooting();
    }
    vVideoStreamingBox->SetExposure(int(vExposure));
    if(vVideoStreamingBox->vShooting)
        ui->lbShootingStatus->setText("Shooting in progress");
    else
        ui->lbShootingStatus->setText("Idle");
}

void MainWindow::on_btScan_clicked()
{
    ui->cbCameras->clear();
    fCameraScan();
    EFWScan();
}

void MainWindow::on_btSetImgFormat_clicked()
{
    if(vVideoStreamingBox->vStreaming)
        vVideoStreamingBox->StopStreaming();
    if(!vVideoStreamingBox->vStreaming)
    {
        int vWidth, vHeight, vBin;
        ASI_IMG_TYPE vImgType;
        vWidth = ui->sbImgWidth->value();
        if(vWidth > vImageMaxWidth)
            vWidth = int(vImageMaxWidth);
        vHeight = ui->sbImgHeight->value();
        if(vHeight > vImageMaxHeight)
            vHeight = int(vImageMaxHeight);
        vBin = ui->cbSetBin->currentText().toInt();
        vString = ui->cbSetImgType->currentText();
        vImgType = fStrToAsiImgType(&vString);
        vError = ASISetROIFormat(vCameraInfo.CameraID,vWidth,vHeight,vBin,vImgType);
        if(vError==ASI_SUCCESS)
        {
            fGetImageFormat();
            vVideoStreamingBox->StartStriming(&vCameraInfo,vImageWidth,vImageHeight,vImageType,vGuidingPorts);
        }

        fErrorProcessing(vError);
    }
}

void MainWindow::on_btSetMaxSize_clicked()
{
    ui->sbImgHeight->setValue(int(vImageMaxHeight));
    ui->sbImgWidth->setValue(int(vImageMaxWidth));
    on_btSetImgFormat_clicked();
}

void MainWindow::on_pbStartShooting_clicked()
{
    if(vVideoStreamingBox->vStreaming)
    {
        if(vVideoStreamingBox->vShooting)
            fOkMsgBox("Info !","Shooting in progress already.");
        else
        {
            vFiltersInvolved.clear();
            vEFWInvolvedIndex =0;
            int totalEstimatedTime;
            if(vEFWconnected)
            {
                for(int i=0;i<vEFWslotsNumber;i++)
                {
                    if(vFilters[i].enabled->isChecked())
                        vFiltersInvolved.append(i);
                }
                if(vFiltersInvolved.count()<1)
                {
                    fOkMsgBox("Error !","No one filter is shosen.");
                    return;
                }

//                QMessageBox ------------------------
                {
                    QMessageBox     vMsgBox;
                    QString s = "Exposure time have to be set separately for each filter.\n";
                    s += "Are you sure to start shooting with existing settings?";
                    vMsgBox.setText("Warning!");
                    vMsgBox.setInformativeText(s);
                    vMsgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
                    vMsgBox.setDefaultButton(QMessageBox::No);
                    int result = vMsgBox.exec();
                    if(result == QMessageBox::No)
                        return;
                }
//              --------------------------------------------------

                EFWSetPosition(vEFW_ID,vFiltersInvolved[0]);
                double totalTime = 0;
                for(int i=0;i<vFiltersInvolved.count();i++)
                    totalTime += vFilters[vFiltersInvolved[i]].exposure->value()*ui->sbShotsNumber->value();
                totalEstimatedTime = int(totalTime);
            }
            else
                totalEstimatedTime = int(vExposure * ui->sbShotsNumber->value())/1000000;
            vVideoStreamingBox->SetTotalEstimatedTime(totalEstimatedTime);
            vVideoStreamingBox->SetObserver(ui->leObserver->text());
            vVideoStreamingBox->SetInstrument(ui->leInstrument->text());
            vVideoStreamingBox->SetTelescope(ui->leTelescope->text());
            vStartShooting = true;
        }
    }
    else
        fOkMsgBox("Error!!!","Start streaming first, please.");
}

void MainWindow::on_pbStopStriming_clicked()
{
    vVideoStreamingBox->Terminate();
}

void MainWindow::on_pbStartStriming_clicked()
{
    vVideoStreamingBox->StartStriming(&vCameraInfo,vImageWidth,vImageHeight,vImageType,vGuidingPorts);
    vVideoStreamingBox->show();
}

void MainWindow::on_pbStopShooting_clicked()
{
    vFiltersInvolved.clear();
    if(vVideoStreamingBox->vShooting)
        vVideoStreamingBox->StopShooting();
}

void MainWindow::on_pbStartVideoRec_clicked()
{
    if(vVideoStreamingBox->vStreaming)
    {
        if(vVideoStreamingBox->vVideoRecording)
            fOkMsgBox("Info !","Video Recording in progress already.");
        else
        {
            if(vExposure>1000000)
            {
                fOkMsgBox("Error!!!","Exposure too big for video recirdubg!\nUse Shooting instead");
                return;
            }
            int vNum = ui->sbVideoFramesNumber->value();
            QDateTime datetime;
            datetime = datetime.currentDateTime();
            vVideoStreamingBox->SetObserver(ui->leObserver->text());
            vVideoStreamingBox->SetInstrument(ui->leInstrument->text());
            vVideoStreamingBox->SetTelescope(ui->leTelescope->text());
            vVideoStreamingBox->StartVideoRecording(vExposure,ui->sbVideoRecordingTime->value(),
                                                    vNum,ComposeFileName(),zwoStreaming::IMG_FORMAT_SER);
        }
    }
    else
        fOkMsgBox("Error!!!","Start streaming first, please.");

}

void MainWindow::on_pbStopVideoRec_clicked()
{
    vVideoStreamingBox->StopVideoRecording();
}
void MainWindow::on_tbWorkFolder_clicked()
{
    vFileDialog->show();
}

void MainWindow::on_cbCameras_currentIndexChanged(int index)
{
    fGetCameraInfo(index);
}

void MainWindow::on_leSetVal_editingFinished()
{
    fSetControlValue(0);
}

void MainWindow::on_leSetVal_2_editingFinished()
{
    fSetControlValue(1);
}

void MainWindow::on_leSetVal_3_editingFinished()
{
    fSetControlValue(2);
}

void MainWindow::on_leSetVal_4_editingFinished()
{
    fSetControlValue(3);
}

void MainWindow::on_leSetVal_5_editingFinished()
{
    fSetControlValue(4);
}

void MainWindow::on_leSetVal_6_editingFinished()
{
    fSetControlValue(5);
}

void MainWindow::on_leSetVal_7_editingFinished()
{
    fSetControlValue(6);
}

void MainWindow::on_leSetVal_8_editingFinished()
{
    fSetControlValue(7);
}

void MainWindow::on_leSetVal_9_editingFinished()
{
    fSetControlValue(8);
}

void MainWindow::on_leSetVal_10_editingFinished()
{
    fSetControlValue(9);
}

void MainWindow::on_leSetVal_11_editingFinished()
{
    fSetControlValue(10);
}

void MainWindow::on_leSetVal_12_editingFinished()
{
    fSetControlValue(11);
}

void MainWindow::on_leSetVal_13_editingFinished()
{
    fSetControlValue(12);
}

void MainWindow::on_leSetVal_14_editingFinished()
{
    fSetControlValue(13);
}

void MainWindow::on_leSetVal_15_editingFinished()
{
    fSetControlValue(14);
}

void MainWindow::on_leSetVal_16_editingFinished()
{
    fSetControlValue(15);
}

void MainWindow::on_leSetVal_17_editingFinished()
{
    fSetControlValue(16);
}

void MainWindow::on_leSetVal_18_editingFinished()
{
    fSetControlValue(17);
}

void MainWindow::on_leSetVal_19_editingFinished()
{
    fSetControlValue(18);
}

void MainWindow::on_leSetVal_20_editingFinished()
{
    fSetControlValue(19);
}

void MainWindow::on_leSetVal_21_editingFinished()
{
    fSetControlValue(20);
}

void MainWindow::on_leOffset_editingFinished()
{
    fSetRightAligning();
}

void MainWindow::on_FileSelected()
{
    vDir = vFileDialog->directory();
    ui->leWorkFolder->setText(vDir.path());
}

void MainWindow::on_vVideoStreamingBox_closed(int result)
{
    ui->pbStartStriming->setChecked(false);
}

void MainWindow::on_chbRightAlignAndOffset_clicked()
{
    fSetRightAligning();
}


void MainWindow::LoadProfile()
{
    QFile *File = new QFile(vConfigFileName);
    if(!File->open(QFile::ReadOnly))
       return;
    QJsonDocument jsonDoc;
    jsonDoc = jsonDoc.fromJson(File->readAll());
    QJsonObject jsonObj;
    jsonObj = jsonDoc.object();
    if(jsonObj.value("document name").toString() == "zwocapture configuration file")
    {
        ui->leWorkFolder->setText(jsonObj.value("saving path").toString());
        ui->chbGain->setChecked(jsonObj.value("filename.gain").toBool());
        ui->chbExposure->setChecked(jsonObj.value("filename.exposure").toBool());
        ui->chbFlip->setChecked(jsonObj.value("filename.flip").toBool());
        ui->chbTemperature->setChecked(jsonObj.value("filename.temperature").toBool());
        ui->chbImgFormat->setChecked(jsonObj.value("filename.imgFormat").toBool());
        ui->chbAlignment->setChecked(jsonObj.value("filename.alignment").toBool());
        ui->chbTimeStamp->setChecked(jsonObj.value("filename.time").toBool());
        ui->sbVideoRecordingTime->setValue(jsonObj.value("video recording time").toInt());
        ui->sbVideoFramesNumber->setValue(jsonObj.value("video frames number").toInt());
        ui->cbVideoFormat->setCurrentText(jsonObj.value("video file format").toString());
        ui->sbShotsNumber->setValue(jsonObj.value("snapshots number").toInt());
        ui->cbImageFormat->setCurrentText(jsonObj.value("image file format").toString());
        ui->chbRightAlignAndOffset->setChecked(jsonObj.value("right alignment.enabled").toBool());

        ui->leFilter1->setText(jsonObj.value("filter1").toString());
        ui->leFilter2->setText(jsonObj.value("filter2").toString());
        ui->leFilter3->setText(jsonObj.value("filter3").toString());
        ui->leFilter4->setText(jsonObj.value("filter4").toString());
        ui->leFilter5->setText(jsonObj.value("filter5").toString());
        ui->leFilter6->setText(jsonObj.value("filter6").toString());
        ui->leFilter7->setText(jsonObj.value("filter7").toString());
        ui->leFilter8->setText(jsonObj.value("filter8").toString());

        zwoStreaming::stProfile *profile;
        profile = new zwoStreaming::stProfile;
        profile->DisplayRefreshTime = jsonObj.value("didplay refresh time").toInt();
        profile->DisplayRefreshTimeManual = jsonObj.value("refresh time is manual").toBool();
        profile->MinThreshold = jsonObj.value("min threshold").toDouble();
        profile->MaxThreshold = jsonObj.value("max threshold").toDouble();
        profile->ThresholdStep = jsonObj.value("threshold step").toDouble();
        profile->MinDistance = jsonObj.value("min distance").toDouble();
        profile->MinRepeatablility = jsonObj.value("min repeatability").toInt();
        profile->ColorFlter = jsonObj.value("color filter").toInt();
        profile->MinArea = jsonObj.value("min area").toDouble();
        profile->MaxArea = jsonObj.value("max area").toDouble();
        profile->MinCircle = jsonObj.value("min circularity").toDouble();
        profile->MaxCircle = jsonObj.value("max circularity").toDouble();
        profile->MinInertia = jsonObj.value("min inertia").toDouble();
        profile->MaxInertia = jsonObj.value("max inertia").toDouble();
        profile->MinConvexity = jsonObj.value("min convexity").toDouble();
        profile->MaxConvexity = jsonObj.value("max convexity").toDouble();
        profile->GaussFilter = jsonObj.value("Gauss K").toInt();
        profile->Tolerance = jsonObj.value("tolerance").toDouble();;
        profile->KeypointsLimit = jsonObj.value("keypoints limit").toInt();
        profile->ColorFilterEnabled = jsonObj.value("color filter enabled").toBool();
        profile->AreaFilterEnabled = jsonObj.value("area filter enabled").toBool();
        profile->CirculFilterEnabled = jsonObj.value("circularity filter enabled").toBool();
        profile->InertiaFilterEnabled = jsonObj.value("inertia filter enabled").toBool();
        profile->ConvexityFilterEnabled = jsonObj.value("convexity filter enabled").toBool();
        profile->RAsetpoint = jsonObj.value("ra setpoint").toDouble();
        profile->RAgain = jsonObj.value("ra gain").toDouble();
        profile->RAresetTime = jsonObj.value("ra reset time").toDouble();
        profile->RAderivativeTime = jsonObj.value("ra derivative time").toDouble();
        profile->RAdeadBand = jsonObj.value("ra dead band").toDouble();
        profile->RAoutputInversed = jsonObj.value("ra pid output inversed").toBool();
        profile->DCsetpoint = jsonObj.value("dc setpoint").toDouble();
        profile->DCgain = jsonObj.value("dc gain").toDouble();
        profile->DCresetTime = jsonObj.value("dc reset time").toDouble();
        profile->DCderivativeTime = jsonObj.value("dc derivative time").toDouble();
        profile->DCdeadBand = jsonObj.value("dc dead band").toDouble();
        profile->DCoutputInversed = jsonObj.value("dc pid output inversed").toBool();
        vVideoStreamingBox->SetProfile(profile);
        delete profile;
    }
    File->close();
}

void MainWindow::SaveProfile()
{
    QFile *File = new QFile(vConfigFileName);
    File->open(QFile::WriteOnly | QFile::Truncate);
    QJsonObject jsonObj;
    {
        jsonObj.insert("document name",QJsonValue("zwocapture configuration file"));
        jsonObj.insert("saving path",QJsonValue(ui->leWorkFolder->text()));
        jsonObj.insert("filename.gain",QJsonValue(ui->chbGain->isChecked()));
        jsonObj.insert("filename.exposure",QJsonValue(ui->chbExposure->isChecked()));
        jsonObj.insert("filename.flip",QJsonValue(ui->chbFlip->isChecked()));
        jsonObj.insert("filename.temperature",QJsonValue(ui->chbTemperature->isChecked()));
        jsonObj.insert("filename.imgFormat",QJsonValue(ui->chbImgFormat->isChecked()));
        jsonObj.insert("filename.alignment",QJsonValue(ui->chbAlignment->isChecked()));
        jsonObj.insert("filename.time",QJsonValue(ui->chbTimeStamp->isChecked()));
        jsonObj.insert("video recording time",QJsonValue(ui->sbVideoRecordingTime->value()));
        jsonObj.insert("video frames number",QJsonValue(ui->sbVideoFramesNumber->value()));
        jsonObj.insert("video file format",QJsonValue(ui->cbVideoFormat->currentText()));
        jsonObj.insert("snapshots number",QJsonValue(ui->sbShotsNumber->value()));
        jsonObj.insert("image file format",QJsonValue(ui->cbImageFormat->currentText()));
        jsonObj.insert("right alignment.enabled",QJsonValue(ui->chbRightAlignAndOffset->isChecked()));

        //EFW
        jsonObj.insert("filter1",QJsonValue(ui->leFilter1->text()));
        jsonObj.insert("filter2",QJsonValue(ui->leFilter2->text()));
        jsonObj.insert("filter3",QJsonValue(ui->leFilter3->text()));
        jsonObj.insert("filter4",QJsonValue(ui->leFilter4->text()));
        jsonObj.insert("filter5",QJsonValue(ui->leFilter5->text()));
        jsonObj.insert("filter6",QJsonValue(ui->leFilter6->text()));
        jsonObj.insert("filter7",QJsonValue(ui->leFilter7->text()));
        jsonObj.insert("filter8",QJsonValue(ui->leFilter8->text()));

        zwoStreaming::stProfile *profile;
        profile = new zwoStreaming::stProfile;
        vVideoStreamingBox->GetProfile(profile);
        jsonObj.insert("didplay refresh time",QJsonValue(profile->DisplayRefreshTime)); //= jsonObj.value("").toInt();
        jsonObj.insert("refresh time is manual",QJsonValue(profile->DisplayRefreshTimeManual));
        jsonObj.insert("min threshold",QJsonValue(profile->MinThreshold));
        jsonObj.insert("max threshold",QJsonValue(profile->MaxThreshold));
        jsonObj.insert("threshold step",QJsonValue(profile->ThresholdStep));
        jsonObj.insert("min distance",QJsonValue(profile->MinDistance));
        jsonObj.insert("min repeatability",QJsonValue(profile->MinRepeatablility));
        jsonObj.insert("color filter",QJsonValue(profile->ColorFlter));
        jsonObj.insert("min area",QJsonValue(profile->MinArea));
        jsonObj.insert("max area",QJsonValue(profile->MaxArea));
        jsonObj.insert("min circularity",QJsonValue(profile->MinCircle));
        jsonObj.insert("max circularity",QJsonValue(profile->MaxCircle));
        jsonObj.insert("min inertia",QJsonValue(profile->MinInertia));
        jsonObj.insert("max inertia",QJsonValue(profile->MaxInertia));
        jsonObj.insert("min convexity",QJsonValue(profile->MinConvexity));
        jsonObj.insert("max convexity",QJsonValue(profile->MaxConvexity));
        jsonObj.insert("Gauss K",QJsonValue(profile->GaussFilter));
        jsonObj.insert("tolerance",QJsonValue(profile->Tolerance));
        jsonObj.insert("keypoints limit",QJsonValue(profile->KeypointsLimit));
        jsonObj.insert("color filter enabled",QJsonValue(profile->ColorFilterEnabled));
        jsonObj.insert("area filter enabled",QJsonValue(profile->AreaFilterEnabled));
        jsonObj.insert("circularity filter enabled",QJsonValue(profile->CirculFilterEnabled));
        jsonObj.insert("inertia filter enabled",QJsonValue(profile->InertiaFilterEnabled));
        jsonObj.insert("convexity filter enabled",QJsonValue(profile->ConvexityFilterEnabled));
        jsonObj.insert("ra setpoint",QJsonValue(profile->RAsetpoint));
        jsonObj.insert("ra gain",QJsonValue(profile->RAgain));
        jsonObj.insert("ra reset time",QJsonValue(profile->RAresetTime));
        jsonObj.insert("ra derivative time",QJsonValue(profile->RAderivativeTime));
        jsonObj.insert("ra dead band",QJsonValue(profile->RAdeadBand));
        jsonObj.insert("ra pid output inversed",QJsonValue(profile->RAoutputInversed));
        jsonObj.insert("dc setpoint",QJsonValue(profile->DCsetpoint));
        jsonObj.insert("dc gain",QJsonValue(profile->DCgain));
        jsonObj.insert("dc reset time",QJsonValue(profile->DCresetTime));
        jsonObj.insert("dc derivative time",QJsonValue(profile->DCderivativeTime));
        jsonObj.insert("dc dead band",QJsonValue(profile->DCdeadBand));
        jsonObj.insert("dc pid output inversed",QJsonValue(profile->DCoutputInversed));
        delete profile;
    }
    QJsonDocument jsonDoc = QJsonDocument(jsonObj);
    File->write(jsonDoc.toJson());
    File->close();
    delete File;
}

void MainWindow::ConnectToCamera()
{
    QString vLocalSting, vLocalSting1;
    vLocalSting.clear();
    fHideCameraControls();
    vError = ASIOpenCamera(vCameraInfo.CameraID);
    if(vError == ASI_SUCCESS)
    {
        ui->lbConnectStatus->setText("Connected to "+vString.fromUtf8(vCameraInfo.Name));
        vError = ASIInitCamera(vCameraInfo.CameraID);
        if(vError == ASI_SUCCESS)
        {
            vError = ASIGetNumOfControls(vCameraInfo.CameraID,&vControlsNumber);
            if(vError == ASI_SUCCESS)
            {
                fGetControlValues();
                for(int i=0;i < vControlsNumber;i++)
                {
                    vError = ASIGetControlCaps(vCameraInfo.CameraID,i,&vControlCups);
                    if(vError == ASI_SUCCESS)
                    {
                        vLocalSting.append(vString.fromUtf8(vControlCups.Name));
                        vLocalSting.append(" - "+vString.fromUtf8(vControlCups.Description)+"\n");
                        vLocalSting.append("Default val - "+vString.setNum(vControlCups.DefaultValue)+"; Auto support - "+fBoolToString(vControlCups.IsAutoSupported));
                        vLocalSting.append("; Write support - "+fBoolToString(vControlCups.IsWritable)+"\n\n");
                        aControlNames[i]->setText(vString.fromUtf8(vControlCups.Name));
                        vLocalSting1.clear();

                        //calculate values for exposure and sensor temparature
                        switch (vControlCups.ControlType)
                        {
                            case ASI_EXPOSURE:
                                {
                                   vLocalSting1.append("; "+vString.setNum(float(vControlCups.MinValue)/1000000,'f')+" - ");
                                   vLocalSting1.append(vString.setNum(float(vControlCups.MaxValue)/1000000)+" sec");
                                }
                                break;
                            case ASI_TEMPERATURE:
                                {
                                   vLocalSting1.append("; "+vString.setNum(float(vControlCups.MinValue)/10,'f',1)+" - ");
                                   vLocalSting1.append(vString.setNum(float(vControlCups.MaxValue)/10,'f',1)+" C");
                                }
                                break;
                            default:
                                {
                                   vLocalSting1.append("; "+vString.setNum(vControlCups.MinValue)+" - ");
                                   vLocalSting1.append(vString.setNum(vControlCups.MaxValue));
                                }
                                break;
                        }

                        aMinMaxVal[i]->setText(vLocalSting1);

                        //disable not supported controls or copy current value to set value
                        if(!vControlCups.IsWritable)
                            aSetValues[i]->setEnabled(false);
                        else
                            aSetValues[i]->setText(aCurrentVal[i]->text());
                        if(!vControlCups.IsAutoSupported)
                            aAutoSet[i]->setEnabled(false);
                        else
                        {   if(aAutoStatus[i]->text() == "Yes")
                                aAutoSet[i]->setChecked(true);
                            else
                                aAutoSet[i]->setChecked(false);
                        }

                        aControlNames[i]->show();
                        aMinMaxVal[i]->show();
                        aCurrentVal[i]->show();
                        aAutoStatus[i]->show();
                        aSetValues[i]->show();
                        aAutoSet[i]->show();

                    }
                }

                //get Image format and copy data to set values
                fGetImageFormat();
                ui->sbImgWidth->setValue(vImageWidth);
                ui->sbImgHeight->setValue(vImageHeight);
                ui->sbStartX->setValue(vStartX);
                ui->sbStartY->setValue(vStartY);

                vTimer->start();

                ui->lbControlsInfo->setText(vLocalSting);
                for(int i=vControlsNumber;i < 23;i++)
                {
                    aControlNames[i]->hide();
                    aMinMaxVal[i]->hide();
                    aCurrentVal[i]->hide();
                    aAutoStatus[i]->hide();
                    aSetValues[i]->hide();
                    aAutoSet[i]->hide();
                }
            }
        }
    }
    fErrorProcessing(vError);
    if(vError == ASI_SUCCESS)
    {
        SetTabsEnabled(true);
        if(vCameraInfo.ST4Port==ASI_TRUE)
            vGuidingPorts |= Tracking::Guiding_cameraST4port;
        else
        {
            vGuidingPorts &= ~Tracking::Guiding_cameraST4port;
        }
        ui->btConnect->setText("Disconnect");
    }
}

void MainWindow::DisconnectCamera()
{
    vTimer->stop();
    vVideoStreamingBox->Terminate();
    vError = ASICloseCamera(vCameraInfo.CameraID);
    ui->lbConnectStatus->setText("Not connected");
    SetTabsEnabled(false);
    ui->btConnect->setText("Connect");
    ui->btConnect->setChecked(false);
}

void MainWindow::ActivateEFWcontrols()
{
    for(int i=0;i<vEFWslotsNumber;i++)
    {
        vFilters[i].Name->setEnabled(true);
        vFilters[i].enabled->setEnabled(true);
    }
}

void MainWindow::DisableEFWcontrols()
{
    for(int i=0;i<8;i++)
    {
        vFilters[i].Name->setEnabled(false);
        vFilters[i].enabled->setEnabled(false);
    }
}

void MainWindow::RenameEFWcontrol()
{
    for(int i=0;i<8;i++)
    {
        if(vFilters[i].Name->text() != "")
            vFilters[i].enabled->setText(vFilters[i].Name->text());
        else
            vFilters[i].enabled->setText("Filter "+QString().setNum(i+1));
    }
}

void MainWindow::StartShooting()
{
    int vNum = ui->sbShotsNumber->value();
    QDateTime datetime;
    datetime = datetime.currentDateTime();
    if(vEFWconnected)
    {
        vVideoStreamingBox->SetFilterName(vFilters[vFiltersInvolved[vEFWInvolvedIndex]].Name->text());
        vError = ASISetControlValue(vCameraInfo.CameraID,ASI_EXPOSURE,
                                    int(vFilters[vFiltersInvolved[vEFWInvolvedIndex]].exposure->value()*1000000),ASI_FALSE);
        if(vError!=ASI_SUCCESS)
        {
            fErrorProcessing(vError);
            return;
        }
        fGetControlValues();
    }
    else
        vVideoStreamingBox->SetFilterName("");
    vVideoStreamingBox->StartShooting(vExposure,vNum,ComposeFileName(),
                                      zwoStreaming::IMG_FILE_FORMATS(ui->cbImageFormat->currentIndex()));
     vStartShooting = false;
}

void MainWindow::EFWScan()
{
    EFW_ERROR_CODE error;
    EFW_INFO info;
    int ID;
    QString devName;
    vEFWnumber = EFWGetNum();
    ui->lbFWNumber->setText(QString().setNum(vEFWnumber));
    ui->cbFWs->clear();
    if(vEFWnumber > 0)
    {
        for(int i=0;i<vEFWnumber;i++)
        {
            error = EFWGetID(i, &ID);
            if(error == EFW_SUCCESS)
            {
                error = EFWOpen(ID);
                if(error == EFW_SUCCESS)
                {
                    error = EFWGetProperty(ID, &info);
                    if(error == EFW_SUCCESS)
                    {
                        devName = devName.fromUtf8(info.Name);
                        ui->cbFWs->addItem(devName);
                    }
                }
                error = EFWClose(ID);
            }
        }
        ui->cbFWs->setCurrentIndex(0);
        getEFWinfo(0);
    }
}

int MainWindow::getEFWinfo(int index)
{
    int ID;
    EFW_INFO info;
    QString s;
    EFW_ERROR_CODE error;
    error = EFWGetID(index, &ID);
    if(error == EFW_SUCCESS)
    {
        error = EFWOpen(ID);
        if(error == EFW_SUCCESS)
        {
            error = EFWGetProperty(ID, &info);
            if(error == EFW_SUCCESS)
            {
                s = "EFW Name: "+QString().fromUtf8(info.Name)+"\n";
                s += "EFW slots Number: "+QString().setNum(info.slotNum);
                ui->lbEFWinfo->setText(s);
            }
        }
        error = EFWClose(ID);
    }
    return error;
}

void MainWindow::showEFWmenu(const QPoint &pos)
{
    vEFWmenu->setWindowFlags(Qt::Popup);
    vEFWmenu->popup(pos);
}

void MainWindow::usb2st4Disconnect()
{
    USB2ST4Close(vUSB2ST4_ID);
    vUSB2ST4connected = false;
    vGuidingPorts &= ~Tracking::Guiding_USB2ST4adapter;
    ui->lbUSB3Status->setText("--");
}

void MainWindow::setAllEFWexposure()
{
    for(int i=0;i<8;i++)
    {
        vFilters[i].exposure->setValue(double(vExposure)/1000000.0);
    }
}

void MainWindow::setEFWexposure()
{
    vFilters[vEFWexposureIndex].exposure->setValue(double(vExposure)/1000000.0);
}


void MainWindow::on_btConnect_toggled(bool checked)
{
    if(checked)
    {
        ConnectToCamera();
    }
    else
    {
        DisconnectCamera();
        usb2st4Disconnect();
    }
}

void MainWindow::on_pbConnectToFW_toggled(bool checked)
{
    if(checked)
    {
        int error = EFWGetID(ui->cbFWs->currentIndex(),&vEFW_ID);
        if(error == EFW_SUCCESS)
        {
            error = EFWOpen(vEFW_ID);
            if(error == EFW_SUCCESS)
            {
                EFWGetProperty(vEFW_ID,&vEFWinfo);
                vEFWslotsNumber = vEFWinfo.slotNum;
                ActivateEFWcontrols();
                ui->lbEFWconnectStatus->setText("Connected");
                ui->pbConnectToFW->setText("Disconnect");
                ui->tabWidget->setTabEnabled(4,true);
                vEFWconnected = true;
            }
            else
            {
                ui->lbEFWconnectStatus->setText("failed");
                DisableEFWcontrols();
                vEFWconnected = false;
            }
        }
    }
    else
    {
        EFWClose(vEFW_ID);
        ui->lbEFWconnectStatus->setText("Not connected");
        ui->pbConnectToFW->setText("Connect");
        ui->tabWidget->setTabEnabled(4,false);
        vEFWslotsNumber = 0;
        DisableEFWcontrols();
        vEFWconnected = false;
    }
}

void MainWindow::on_cbFWs_currentIndexChanged(int index)
{
    getEFWinfo(index);
}

void MainWindow::on_pbEFWcalibrate_clicked()
{
    EFWCalibrate(vEFW_ID);
}

void MainWindow::on_pbEFWsetPosition_clicked()
{
    EFWSetPosition(vEFW_ID,ui->sbEFWtargetPosition->value()-1);
}

void MainWindow::on_ShootingCompleted()
{
    if(vEFWconnected)
    {
        vEFWInvolvedIndex++;
        if(vEFWInvolvedIndex<vFiltersInvolved.count())
        {
            EFWSetPosition(vEFW_ID,vFiltersInvolved[vEFWInvolvedIndex]);
            vStartShooting = true;
        }
    }
}

void MainWindow::on_dsFilterExp1_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp1->mapToGlobal(pos);
    vEFWexposureIndex = 0;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp2_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp2->mapToGlobal(pos);
    vEFWexposureIndex = 1;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp3_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp3->mapToGlobal(pos);
    vEFWexposureIndex = 2;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp4_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp4->mapToGlobal(pos);
    vEFWexposureIndex = 3;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp5_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp5->mapToGlobal(pos);
    vEFWexposureIndex = 4;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp6_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp6->mapToGlobal(pos);
    vEFWexposureIndex = 5;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp7_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp7->mapToGlobal(pos);
    vEFWexposureIndex = 6;
    showEFWmenu(Position);
}

void MainWindow::on_dsFilterExp8_customContextMenuRequested(const QPoint &pos)
{
    QPoint Position = ui->dsFilterExp8->mapToGlobal(pos);
    vEFWexposureIndex = 7;
    showEFWmenu(Position);
}

void MainWindow::on_chbRightAlignAndOffset_stateChanged(int arg1)
{

}
