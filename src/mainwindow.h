/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qstring.h>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QCheckBox>
#include <QTimer>
#include <QFileDialog>
#include <QDir>
#include <QDateTime>
#include <QDoubleSpinBox>
#include <QAction>

#include <ASICamera2.h>
#include <EFW_filter.h>

#include "zwostreaming.h"
#include "zwoFunctions.h"
#include "tracking.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btScan_clicked();
    void on_cbCameras_currentIndexChanged(int index);
    void onTimer();
    void on_leSetVal_editingFinished();
    void on_leSetVal_2_editingFinished();
    void on_leSetVal_3_editingFinished();
    void on_leSetVal_4_editingFinished();
    void on_leSetVal_5_editingFinished();
    void on_leSetVal_6_editingFinished();
    void on_leSetVal_7_editingFinished();
    void on_leSetVal_8_editingFinished();
    void on_leSetVal_9_editingFinished();
    void on_leSetVal_10_editingFinished();
    void on_leSetVal_11_editingFinished();
    void on_leSetVal_12_editingFinished();
    void on_leSetVal_13_editingFinished();
    void on_leSetVal_14_editingFinished();
    void on_leSetVal_15_editingFinished();
    void on_leSetVal_16_editingFinished();
    void on_leSetVal_17_editingFinished();
    void on_leSetVal_18_editingFinished();
    void on_leSetVal_19_editingFinished();
    void on_leSetVal_20_editingFinished();
    void on_leSetVal_21_editingFinished();
    void on_btSetImgFormat_clicked();
    void on_tbWorkFolder_clicked();
    void on_FileSelected();
    void on_pbStartStriming_clicked();
    void on_vVideoStreamingBox_closed(int result);
    void on_btSetMaxSize_clicked();
    void on_pbStartShooting_clicked();
    void on_pbStopStriming_clicked();
    void on_chbRightAlignAndOffset_clicked();
    void on_leOffset_editingFinished();
    void on_pbStopShooting_clicked();
    void on_pbStartVideoRec_clicked();
    void on_pbStopVideoRec_clicked();
    void on_btConnect_toggled(bool checked);
    void on_pbConnectToFW_toggled(bool checked);
    void on_cbFWs_currentIndexChanged(int index);
    void on_pbEFWcalibrate_clicked();
    void on_pbEFWsetPosition_clicked();
    void on_ShootingCompleted();

    void on_dsFilterExp1_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp2_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp3_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp4_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp5_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp6_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp7_customContextMenuRequested(const QPoint &pos);

    void on_dsFilterExp8_customContextMenuRequested(const QPoint &pos);
    void    setAllEFWexposure();
    void    setEFWexposure();

    void on_chbRightAlignAndOffset_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;

    zwoStreaming::stProfile *vstProfile;
    int                 vGuidingPorts;
    int                 vCamerasNumber;
    int                 vControlsNumber;
    long                vCurrentControlVal;
    ASI_BOOL            vAutoStatus;
    int                 vImageWidth;
    int                 vImageHeight;
    int                 vImageBin;
    ASI_IMG_TYPE        vImageType;
    int                 vStartX;
    int                 vStartY;
    long                 vImageMaxWidth;
    long                 vImageMaxHeight;
    long                 vGain;
    int                 vExposure;
    long                 vFlip;
    long                 vTemperature;
    long                 vCooler;

    QLabel*             aControlNames[23];
    QLabel*             aMinMaxVal[23];
    QLabel*             aCurrentVal[23];
    QLabel*             aAutoStatus[23];
    QLineEdit*          aSetValues[23];
    QCheckBox*          aAutoSet[23];
    QTimer*             vTimer;
    QFileDialog*        vFileDialog;
    QDir                vDir;
    QString             vHomePath;
    QString             vConfigFileName;

    ASI_CONTROL_CAPS    vControlCups;
    QString             vString;
    ASI_ERROR_CODE      vError;
    ASI_CAMERA_INFO     vCameraInfo;
    zwoStreaming        *vVideoStreamingBox;

    void    fCameraScan();
    void    fGetCameraInfo(int vCameraNumber);
    void fErrorProcessing(ASI_ERROR_CODE verror);
    void fHideCameraControls();
    void fGetControlValues();
    void fSetControlValue(int vIndex);
    void fGetImageFormat();
    void fSetRightAligning();
    void SetTabsEnabled(bool enabled);
    QString ComposeFileName();
    void LoadProfile();
    void SaveProfile();
    void ConnectToCamera();
    void DisconnectCamera();
    void ActivateEFWcontrols();
    void DisableEFWcontrols();
    void RenameEFWcontrol();
    void StartShooting();

    // EFW methods
    void    EFWScan();
    int     getEFWinfo(int index);

    // EFW variables
    struct  Filter
    {
        QLineEdit*      Name;
        QCheckBox*      enabled;
        QDoubleSpinBox* exposure;
    };
    QMenu       *vEFWmenu;
    QAction     *actionSetAllEFWexposure;
    QAction     *actionSetEFWexposure;
    QList<Filter>       vFilters;
    QList<int>          vFiltersInvolved;
    EFW_INFO    vEFWinfo;
    int         vEFWnumber;
    int         vEFW_ID;
    int         vEFWslotsNumber;
    int         vEFWInvolvedIndex;
    int         vEFWexposureIndex;
    bool        vEFWconnected;
    bool        vStartShooting;
    //EFW methods
    void    showEFWmenu(const QPoint &pos);
    //usb2st4 adapter
    int         vUSB2ST4Number;
    bool        vUSB2ST4connected;
    int         vUSB2ST4_ID;
    void        usb2st4Disconnect();
};

#endif // MAINWINDOW_H
