/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "pidcontrol.h"

PIDcontrol::PIDcontrol()
{
    vIntegrative = 0;
    vLastOutputVal = 0;
    vLastError = 0;
    vLastInputVal = 0;
    vSetpoint = 0;
    vInputRange = 100;
    vOutputRange = 100;
    vMinOutput = 0;
    vMaxOutput = 100;
    setOutputInversed(false);
}

void PIDcontrol::setGain(double gain)
{
    this->vGain = gain;
}

void PIDcontrol::setResetTime(double time)
{
    vResetTime = time;
}

void PIDcontrol::setDerivativeTime(double time)
{
    vDerivativeTime = time;
}

void PIDcontrol::setSetpoint(double Setpoint)
{
    vSetpoint = Setpoint;
}

void PIDcontrol::setInputRange(double min, double max)
{
    vMinInput = min;
    vMaxInput = max;
    vInputRange = max - min;
}

void PIDcontrol::setOutputRange(double min, double max)
{
    vMinOutput = min;
    vMaxOutput = max;
    vOutputRange = max - min;
}

void PIDcontrol::setOutputVal(double outputVal)
{
    vLastOutputVal = outputVal;
}

void PIDcontrol::setOutputInversed(bool Inversed)
{
    vOutputInversed = Inversed;
    if(Inversed)
        vOutputInversionK = -1.0;
    else
        vOutputInversionK = 1.0;
}

void PIDcontrol::setDeadBand(double value)
{
    vDeadBand = value;
}

double PIDcontrol::calculate(double inputVal)
{
    if(isnan(inputVal))
        return vLastOutputVal;
    QDateTime currentDateTime = QDateTime().currentDateTime();
    double error,RelativeError,Der,dOutput,Output;
    qint64 dTime = vLastDateTime.msecsTo(currentDateTime);
    double dT = dTime/1000.0;
    error = (vSetpoint - inputVal);
    RelativeError = error/vInputRange;
    if(isnan(vIntegrative))
        vIntegrative = 0;
    if(fabs(error) > (vDeadBand/2.0))
    {
        if(dT < vResetTime)
            vIntegrative += RelativeError*dT/vResetTime;
    }
    Der = (inputVal - vLastInputVal)/vInputRange * vDerivativeTime / double(dT);
    if(isnan(Der))
        Der = 0.0;
    dOutput = vOutputInversionK * vGain * (RelativeError + vIntegrative + Der);
    Output = dOutput * vOutputRange;
    if(Output > vMaxOutput)
    {
        vLastOutputVal = vMaxOutput;
        vIntegrative = (Output - vLastOutputVal)/vOutputRange*vOutputInversionK/vGain - RelativeError - Der;
    }
    else
    {
        if(Output < vMinOutput)
        {
            vLastOutputVal = vMinOutput;
            vIntegrative = (Output - vLastOutputVal)/vOutputRange*vOutputInversionK/vGain - RelativeError - Der;
        }
        else
            vLastOutputVal = Output;
    }
    vLastInputVal = inputVal;
    vLastDateTime = currentDateTime;
    return vLastOutputVal;
}
