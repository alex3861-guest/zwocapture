/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PIDCONTROL_H
#define PIDCONTROL_H
#include <QDateTime>
#include <math.h>

class PIDcontrol
{
public:
    PIDcontrol();
    void    setGain(double gain);
    void    setIntegrativeTime(double time);
    void    setResetTime(double time);
    void    setDerivativeTime(double time);
    void    setSetpoint(double Setpoint);
    void    setInputRange(double min,double max);
    void    setOutputRange(double min,double max);
    void    setOutputVal(double outputVal);
    void    setOutputInversed(bool Inversed);
    void    setDeadBand(double value);
    double  calculate(double inputVal);

private:
    //calculation parameters
    double  vGain;
    double  vResetTime;      //seconds
    double  vDerivativeTime;       //seconds
    double  vSetpoint;
    double  vMinInput;
    double  vMaxInput;
    double  vInputRange;
    double  vMinOutput;
    double  vMaxOutput;
    double  vOutputRange;
    bool    vOutputInversed;
    double  vOutputInversionK;
    double  vDeadBand;

    //variable data
    double  vLastOutputVal;
    double  vIntegrative;
    double  vDerivative;
    double  vLastInputVal;
    double  vLastError;
    double  vLastDerivative;
    QDateTime vLastDateTime;

protected:

};

#endif // PIDCONTROL_H
