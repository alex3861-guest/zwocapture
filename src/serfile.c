/*
This module is a part of zwocapture programm and is a
small set of tools to manipulate with *.ser file
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "serfile.h"
#include <string.h>
#include <stdlib.h>

size_t vSize = 178;
time_t vTime;

typedef enum _funcToColl{
    _RW_INT_32,
    _RW_INT_64,
    _RW_CHAR_14,
    _RW_CHAR_40
}_funcToColl;

void    fLEndianToHost32(const u_char* vDataIn, int32_t* vDataOut);
void    fHostToLEndian32(int32_t* vDataIn, u_char* vDataOut);
void    fLEndianToHost64(const u_char* vDataIn, int64_t* vDataOut);
void    fHostToLEndian64(int64_t* vDataIn, u_char* vDataOut);

int fReadInt32(FILE *vFile,int32_t* vData, long *vStartPosition);
int fReadInt64(FILE *vFile,int64_t* vData, long *vStartPosition);
int fReadChar(FILE *vFile,char* vData, long *vStartPosition, size_t vSize);
int fParameterFuncAndPosition(SerFileHeaderPar* vParameter, long *vPosition, _funcToColl* vFunctionToCall);
int fWriteInt32(FILE *vFile,int32_t* vData, long *vStartPosition);
int fWriteInt64(FILE *vFile,int64_t* vData, long *vStartPosition);
int fWriteChar(FILE *vFile,char* vData, long *vStartPosition, size_t vSize);

// Create SER file with a particular Header
FILE* SerFileOpenCreate(const char* vFilename, SerFileHeader* vHeader)
{
    u_char vBuffer[178];

//  filling buffer from vHeader structur
    memcpy(vBuffer,vHeader->FileID,14);
    memcpy(vBuffer+42,vHeader->Observer,40);
    memcpy(vBuffer+82,vHeader->Instrument,40);
    memcpy(vBuffer+122,vHeader->Telescope,40);
    fHostToLEndian32(&(vHeader->LuID), vBuffer+14);
    fHostToLEndian32(&(vHeader->ColorID), vBuffer+18);
    fHostToLEndian32(&(vHeader->LittleEndian), vBuffer+22);
    fHostToLEndian32(&(vHeader->ImageWidth), vBuffer+26);
    fHostToLEndian32(&(vHeader->ImageHeight), vBuffer+30);
    fHostToLEndian32(&(vHeader->PixelDepthPerPlane), vBuffer+34);
    fHostToLEndian32(&(vHeader->FrameCount), vBuffer+38);
    fHostToLEndian64(&(vHeader->DateTime), vBuffer+162);
    fHostToLEndian64(&(vHeader->DateTime_UTC), vBuffer+170);

//  Create file and flash it with vBuffer data
    FILE* vFile = fopen(vFilename,"w+b");
    size_t vWrittenSize = fwrite(vBuffer, vSize, 1, vFile);
    if(vWrittenSize!=1)
        vFile = NULL;
    return vFile;
}

FILE* SerFileOpenRead(const char* vFilename)
{
    FILE* vFile = fopen(vFilename,"rb");
    return vFile;
}


FILE* SerFileOpenRW(const char *vFilename)
{
    FILE* vFile = fopen(vFilename,"r+b");
    return vFile;
}

// read out *.ser file header information

size_t SerFileGetHeader(FILE* vFile, SerFileHeader* vHeader)
{
    int32_t vtmp;
    int64_t vtmp64;
    u_char vBuffer[178];
    size_t vReadSize = fread(vBuffer, 1, 178, vFile);
    memcpy(vHeader->FileID,vBuffer,14);
    memcpy(vHeader->Observer,vBuffer+42,40);
    memcpy(vHeader->Instrument,vBuffer+82,40);
    memcpy(vHeader->Telescope,vBuffer+122,40);
    fLEndianToHost32(vBuffer+14,&vtmp);
    vHeader->LuID = vtmp;
    fLEndianToHost32(vBuffer+18,&vtmp);
    vHeader->ColorID = vtmp;
    fLEndianToHost32(vBuffer+22,&vtmp);
    vHeader->LittleEndian = vtmp;
    fLEndianToHost32(vBuffer+26,&vtmp);
    vHeader->ImageWidth = vtmp;
    fLEndianToHost32(vBuffer+30,&vtmp);
    vHeader->ImageHeight = vtmp;
    fLEndianToHost32(vBuffer+34,&vtmp);
    vHeader->PixelDepthPerPlane = vtmp;
    fLEndianToHost32(vBuffer+38,&vtmp);
    vHeader->FrameCount = vtmp;
    fLEndianToHost64(vBuffer+162,&vtmp64);
    vHeader->DateTime = vtmp64;
    fLEndianToHost64(vBuffer+170,&vtmp64);
    vHeader->DateTime_UTC = vtmp64;
    return vReadSize;
}

// Close opened file
int SerFileClose(FILE* vFile)
{
    return fclose(vFile);
}

// Add image to an existed opened SER file
// image have to be comply with the SER file Header
// vTimeStamp is optional. If it is present the time stamps for eadh
// individual picture will be added to the end of file
// Note: the time stamp shall be presented for all incuded pictures
//       otherwise no information will be added.
int SerFileAddImage(FILE* vFile,void *vImage, int64_t *vTimeStamp)
{
    int vError = ser_NO_ERROR;
    int32_t vFrameCount, vHeight, vWidth, vPixelDepth, vColorID;
    long  vFilePosition;
    size_t vImageSize,ByteReadWritten;

    //calculate image size and start position+++++++++++++++++++
    SerFileReadHeaderParameter(vFile,ser_FrameCount,&vFrameCount);
    SerFileReadHeaderParameter(vFile,ser_ImageHeight,&vHeight);
    SerFileReadHeaderParameter(vFile,ser_ImageWidth,&vWidth);
    SerFileReadHeaderParameter(vFile,ser_PixelDepthPerPlane,&vPixelDepth);
    SerFileReadHeaderParameter(vFile,ser_ColorID,&vColorID);
    vImageSize = (size_t)(vHeight * vWidth);
    if(vPixelDepth>8)
        vImageSize *= 2;
    if((vColorID==ser_Color_RGB) |
       (vColorID==ser_Color_BGR))
        vImageSize *= 3;
    vFilePosition = (long)(178 + vImageSize * (ulong)vFrameCount);
    //**********************************************************

    fseek(vFile,vFilePosition,SEEK_SET);
    // read previewsly added time stamps if time stamp is given+++++++++
    size_t vTimeStampsSize = 0;
    u_char *vTimeStampsTmp = 0;
    if(*vTimeStamp!=0 && vFrameCount > 0)
    {
        vTimeStampsSize = sizeof(int64_t)*(ulong)vFrameCount;
        vTimeStampsTmp = (u_char *)malloc(vTimeStampsSize);
        ByteReadWritten = fread(vTimeStampsTmp,1,vTimeStampsSize,vFile);
        if(ByteReadWritten != vTimeStampsSize)
        {
            *vTimeStamp = 0;
            free(vTimeStampsTmp);
        }
    }
    //*******************************************************************

    //add image to end of file and inctement FraimCount parameter++++++++
    fseek(vFile,vFilePosition,SEEK_SET);
    ByteReadWritten = fwrite(vImage,1,vImageSize,vFile);
    if(ByteReadWritten!=vImageSize)
    {
        vError = ser_IO_ERROR;
        return vError;
    }
    vFrameCount++;
    //*******************************************************************

    // save previewsly added time stamps if time stamp is given+++++++++
    if(*vTimeStamp != 0)
    {
        if(vFrameCount>1)
        {
            ByteReadWritten = fwrite(vTimeStampsTmp,1,vTimeStampsSize,vFile);
            free(vTimeStampsTmp);
        }
        if(ByteReadWritten != vTimeStampsSize)
            vError = ser_IO_ERROR;
        vFilePosition += (vTimeStampsSize+vImageSize);
        fWriteInt64(vFile,vTimeStamp,&vFilePosition);
    }
    SerFileWriteHeaderParameter(vFile,ser_FrameCount,&vFrameCount);
    return vError;
}


// extract vImageNumber image from ser file vFile and put it to vImage buffer
// vImageNumber start from 0
int SerFileGetImage(FILE *vFile, void *vImage, int *vImageNumber)
{
    int vError = ser_NO_ERROR;
    int32_t vFrameCount, vHeight, vWidth, vPixelDepth;
    long  vFilePosition;
    size_t vImageSize,ByteReadWritten;

    //calculate image size and start position+++++++++++++++++++
    SerFileReadHeaderParameter(vFile,ser_FrameCount,&vFrameCount);
    if(vFrameCount<*vImageNumber)
        return ser_IO_ERROR;
    SerFileReadHeaderParameter(vFile,ser_ImageHeight,&vHeight);
    SerFileReadHeaderParameter(vFile,ser_ImageWidth,&vWidth);
    SerFileReadHeaderParameter(vFile,ser_PixelDepthPerPlane,&vPixelDepth);
    vImageSize = vHeight * vWidth;
    if(vPixelDepth>8)
        vImageSize *= 2;
    vFilePosition = 178 + vImageSize * (*vImageNumber);
    //**********************************************************

    //load image from file to vImage buffer++++++++++++++++++++++
    fseek(vFile,vFilePosition,SEEK_SET);
    ByteReadWritten = fread(vImage,1,vImageSize,vFile);
    if(ByteReadWritten!=vImageSize)
    {
        vError = ser_IO_ERROR;
        return vError;
    }
    //*******************************************************************

    return vError;
}


// Read particular SER file Header parameter
int SerFileReadHeaderParameter(FILE* vFile,SerFileHeaderPar vParameter, void *vValue)
{
    _funcToColl vFunctionToCall;
    long vPosition;
    int vError = 0;
    vError = fParameterFuncAndPosition(&vParameter,&vPosition,&vFunctionToCall);
    switch (vFunctionToCall) {
    case _RW_CHAR_14: vError = fReadChar(vFile,vValue,&vPosition,14);
        break;
    case _RW_CHAR_40: vError = fReadChar(vFile,vValue,&vPosition,40);
        break;
    case _RW_INT_32: vError = fReadInt32(vFile,vValue,&vPosition);
        break;
    case _RW_INT_64: vError = fReadInt64(vFile,vValue,&vPosition);
        break;
    default:
        break;
    }
    return vError;
}

// Write particular SER file Header parameter
int SerFileWriteHeaderParameter(FILE* vFile,SerFileHeaderPar vParameter, void *vValue)
{
    _funcToColl vFunctionToCall;
    long vPosition;
    int vError = 0;
    vError = fParameterFuncAndPosition(&vParameter,&vPosition,&vFunctionToCall);
    switch (vFunctionToCall) {
    case _RW_CHAR_14: vError = fWriteChar(vFile,vValue,&vPosition,14);
        break;
    case _RW_CHAR_40: vError = fWriteChar(vFile,vValue,&vPosition,40);
        break;
    case _RW_INT_32: vError = fWriteInt32(vFile,vValue,&vPosition);
        break;
    case _RW_INT_64: vError = fWriteInt64(vFile,vValue,&vPosition);
        break;
    default:
        break;
    }
    return vError;
}

// Read Image Time Stamp from the trail (end of file)
int64_t SerFileGetImageTimeStamp(FILE *vFile, int *vImageNumber)
{
    int vError = ser_NO_ERROR;
    int32_t vFrameCount, vHeight, vWidth, vPixelDepth;
    long vImageSize, vFilePosition;
    int64_t vTimeStamp=0;
    vError = SerFileReadHeaderParameter(vFile,ser_FrameCount,&vFrameCount)|
    SerFileReadHeaderParameter(vFile,ser_ImageHeight,&vHeight) |
    SerFileReadHeaderParameter(vFile,ser_ImageWidth,&vWidth)|
    SerFileReadHeaderParameter(vFile,ser_PixelDepthPerPlane,&vPixelDepth);
    if(vError!=ser_NO_ERROR)
        return 0;
    vImageSize = vHeight * vWidth;
    if(vPixelDepth>8)
        vImageSize *= 2;
    vFilePosition = 178 + vImageSize * vFrameCount + *vImageNumber * sizeof(int64_t);
    fseek(vFile,vFilePosition,SEEK_SET);
    if(feof(vFile)==0)
    {
        vError = fReadInt64(vFile,&vTimeStamp,&vFilePosition);
    }
    if(vError==ser_NO_ERROR)
        return vTimeStamp;
    else
        return 0;
}

//convert u_char array in Little Endian sequence to int32_t of the Host system
//useful to work with files.
void    fLEndianToHost32(const u_char* vDataIn, int32_t* vDataOut)
{
    *vDataOut = *(vDataIn+3);
    for(int i=2;i>=0;i--)
    {
        *vDataOut = (*vDataOut<<8) + (*(vDataIn+i));
    }
}

//convert u_char array in Little Endian sequence to int64_t of the Host system
//useful to work with files.
void    fLEndianToHost64(const u_char* vDataIn, int64_t* vDataOut)
{
    *vDataOut = *(vDataIn+7);
    for(int i=6;i>=0;i--)
    {
        *vDataOut = (*vDataOut<<8) + (*(vDataIn+i));
    }
}

//convert int32_t of the Host system to u_char array in Little Endian sequence
//useful to work with files.
void    fHostToLEndian32(int32_t* vDataIn, u_char* vDataOut)
{
    u_char vtmp;
    vtmp = *vDataIn & 0xff;
    *vDataOut = vtmp;
    vtmp = *vDataIn>>8 & 0xff;
    *(vDataOut+1) = vtmp;
    vtmp = *vDataIn>>16 & 0xff;
    *(vDataOut+2) = vtmp;
    vtmp = *vDataIn>>24 & 0xff;
    *(vDataOut+3) = vtmp;
}

//convert int64_t of the Host system to u_char array in Little Endian sequence
//useful to work with files.
void    fHostToLEndian64(int64_t* vDataIn, u_char* vDataOut)
{
    u_char vtmp;
    vtmp = *vDataIn & 0xff;
    *vDataOut = vtmp;
    vtmp = *vDataIn>>8 & 0xff;
    *(vDataOut+1) = vtmp;
    vtmp = *vDataIn>>16 & 0xff;
    *(vDataOut+2) = vtmp;
    vtmp = *vDataIn>>24 & 0xff;
    *(vDataOut+3) = vtmp;
    vtmp = *vDataIn>>32 & 0xff;
    *(vDataOut+4) = vtmp;
    vtmp = *vDataIn>>40 & 0xff;
    *(vDataOut+5) = vtmp;
    vtmp = *vDataIn>>48 & 0xff;
    *(vDataOut+6) = vtmp;
    vtmp = *vDataIn>>56 & 0xff;
    *(vDataOut+7) = vtmp;
}

// Read four bytes from the SER file at the particular position
// Convert those bytes to int32_t
int fReadInt32(FILE *vFile,int32_t* vData, long *vStartPosition)
{
    u_char vTmp[4];
    fseek(vFile,*vStartPosition,SEEK_SET);
    if(fread(vTmp,4,1,vFile)!=1)
        return ser_IO_ERROR;
    else
    {
        fLEndianToHost32(vTmp,vData);
        return ser_NO_ERROR;
    }
}

// Read eight bytes from the SER file at the particular position
// Convert those bytes to int64_t
int fReadInt64(FILE *vFile,int64_t* vData, long *vStartPosition)
{
    u_char vTmp[8];
    fseek(vFile,*vStartPosition,SEEK_SET);
    if(fread(vTmp,8,1,vFile)!=1)
        return ser_IO_ERROR;
    else
    {
        fLEndianToHost64(vTmp,vData);
        return ser_NO_ERROR;
    }
}

// Read vSize of bytes from SER file at the particular position
// Convert them to char
int fReadChar(FILE *vFile,char* vData, long *vStartPosition, size_t vSize)
{
    fseek(vFile,*vStartPosition,SEEK_SET);
    if(fread(vData,vSize,1,vFile)!=1)
        return ser_IO_ERROR;
    else
        return ser_NO_ERROR;
}

// Write four bytes to the SER file at the particular position
// Convert those bytes from int32_t
int fWriteInt32(FILE *vFile,int32_t* vData, long *vStartPosition)
{
    u_char vTmp[4];
    fseek(vFile,*vStartPosition,SEEK_SET);
    fHostToLEndian32(vData,vTmp);
    if(fwrite(vTmp,4,1,vFile)!=1)
        return ser_IO_ERROR;
    else
        return ser_NO_ERROR;
}

// Write eight bytes to the SER file at the particular position
// Convert those bytes from int64_t
int fWriteInt64(FILE *vFile,int64_t* vData, long *vStartPosition)
{
    u_char vTmp[8];
    fseek(vFile,*vStartPosition,SEEK_SET);
    fHostToLEndian64(vData,vTmp);
    if(fwrite(vTmp,8,1,vFile)!=1)
        return ser_IO_ERROR;
    else
        return ser_NO_ERROR;
}

// Write vSize of bytes to SER file at the particular position
int fWriteChar(FILE *vFile,char* vData, long *vStartPosition, size_t vSize)
{
    fseek(vFile,*vStartPosition,SEEK_SET);
    if(fwrite(vData,vSize,1,vFile)!=1)
        return ser_IO_ERROR;
    else
        return ser_NO_ERROR;
}
//service function to mutch Header parameter with file position and function to work with
int fParameterFuncAndPosition(SerFileHeaderPar* vParameter, long *vPosition, _funcToColl* vFunctionToCall)
{
    int vError = 0;
    switch (*vParameter)
    {
        case ser_FileID:
        {
            *vPosition = 0;
            *vFunctionToCall = _RW_CHAR_14;
        }
            break;
        case ser_LuID:
        {
            *vPosition = 14;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_ColorID:
        {
            *vPosition = 18;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_LittleEndian:
        {
            *vPosition = 22;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_ImageWidth:
        {
            *vPosition = 26;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_ImageHeight:
        {
            *vPosition = 30;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_PixelDepthPerPlane:
        {
            *vPosition = 34;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_FrameCount:
        {
            *vPosition = 38;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_Observer:
        {
            *vPosition = 42;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_Instrument:
        {
            *vPosition = 82;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_Telescope:
        {
            *vPosition = 122;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_DateTime:
        {
            *vPosition = 162;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        case ser_DateTime_UTC:
        {
            *vPosition = 170;
            *vFunctionToCall = _RW_INT_32;
        }
        break;
        default: vError = 1;
            break;
    }
    return vError;
}
