/*
This module is a part of zwocapture programm and is a
small set of tools to manipulate with *.ser file

Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <sys/types.h>
#include <time.h>

typedef enum Ser_ColorIDs
{
    ser_Color_MONO = 0,
    ser_Color_Bayer_RGGB = 8,
    ser_Color_Bayer_GRBG = 9,
    ser_Color_Bayer_GBRG = 10,
    ser_Color_Bayer_BGGR = 11,
    ser_Color_Bayer_CYYM = 16,
    ser_Color_Bayer_YCMY = 17,
    ser_Color_Bayer_YMCY = 18,
    ser_Color_Bayer_MYYC = 19,
    ser_Color_RGB = 100,
    ser_Color_BGR = 101
};

typedef struct SerFileHeader
{
//Content: "LUCAM-RECORDER" (fix)
    char FileID[14];

//Format: Integer_32 (little-endian)
//Length: 4 Byte
//Content: Lumenera camera series ID (currently unused; default = 0)
    int32_t LuID;

//    Format: Integer_32 (little-endian)
//Length:4 Byte;Content:MONO = 0,BAYER_RGGB = 8,BAYER_GRBG = 9,BAYER_GBRG = 10
//BAYER_BGGR = 11;BAYER_CYYM = 16;BAYER_YCMY = 17;BAYER_YMCY = 18;BAYER_MYYC = 19
//RGB = 100;BGR = 101
    int32_t ColorID;

//Format:Integer_32 (little-endian)
//Length:4 Byte
//Content:0 (FALSE) for big-endian byte order in 16 bit image data
//        1 (TRUE) for little-endian byte order in 16 bit image data
    int32_t LittleEndian;

//Format:Integer_32 (little-endian)
//Length:4 Byte
//Content:Width of every image in pixel
    int32_t ImageWidth;

//Format:Integer_32 (little-endian)
//Length:4 Byte
//Content:Height of every image in pixel
    int32_t ImageHeight;

//Format:Integer_32 (little-endian)
//Length:4 Byte
//Content:True bit depth per pixel per plane
    int32_t PixelDepthPerPlane;

//Format: Integer_32 (little-endian)
//Length: 4 Byte
//Content: Number of image frames in SER file
    int32_t FrameCount;

//Format: String
//Length: 40 Byte (40 ASCII characters {32...126 dec.}, fill unused characters with 0 dec.)
//Content: Name of observer
    char Observer[40];

//Format: String
//Length: 40 Byte (40 ASCII characters {32...126 dec.}, fill unused characters with 0 dec.)
//Content: Name of used camera
    char Instrument[40];

//Format: String
//Length: 40 Byte (40 ASCII characters {32...126 dec.}, fill unused characters with 0 dec.)
//Content: Name of used telescope
    char Telescope[40];

//Format: Date / Integer_64 (little-endian)
//Length: 8 Byte
//Content: Start time of image stream (local time)
//If 12_DateTime <= 0 then 12_DateTime is invalid and the SER file does not contain a
//Time stamp trailer.
    time_t DateTime;

//Format: Date / Integer_64 (little-endian)
//Length: 8 Byte
//Content: Start time of image stream in UTC
    time_t DateTime_UTC;
}SerFileHeader;

// SerFileHeaderPar is used with SerFileReadHeaderParameter and
//SerFileWriteHeaderParameter functions
typedef enum SerFileHeaderPar
{
    ser_FileID,
    ser_LuID,
    ser_ColorID,
    ser_LittleEndian,
    ser_ImageWidth,
    ser_ImageHeight,
    ser_PixelDepthPerPlane,
    ser_FrameCount,
    ser_Observer,
    ser_Instrument,
    ser_Telescope,
    ser_DateTime,
    ser_DateTime_UTC
}SerFileHeaderPar;

// Errors
typedef enum SerFileErrors{
    ser_NO_ERROR,
    ser_IO_ERROR,
    ser_READ_SIZE_MISMATCH
}SerFileErrors;

// Functions
// Create SER file with a particular Header
FILE*   SerFileOpenCreate(const char *vFilename, SerFileHeader* vHeader);


FILE*   SerFileOpenRead(const char *vFilename);
FILE*   SerFileOpenRW(const char *vFilename);

// read out *.ser file header information
size_t  SerFileGetHeader(FILE* vFile, SerFileHeader* vHeader);

// Add image to an existed opened SER file
// image have to be comply with the SER file Header
// vTimeStamp is optional. If it is present the time stamps for each
// individual picture will be added to the end of file
// Note: the time stamp shall be presented for all incuded pictures
//       otherwise no information will be added.
int     SerFileAddImage(FILE* vFile,void *vImage, int64_t *vTimeStamp);

// extract vImageNumber image from ser file vFile and put it to vImage buffer
// vImageNumber start from 0
int     SerFileGetImage(FILE* vFile,void *vImage,int *vImageNumber);

int     SerFileClose(FILE* vFile);

// Read particular SER file Header parameter
int     SerFileReadHeaderParameter(FILE* vFile,SerFileHeaderPar vParameter, void *vValue);

// Write particular SER file Header parameter
int     SerFileWriteHeaderParameter(FILE* vFile,SerFileHeaderPar vParameter, void *vValue);

// Read Image Time Stamp from the trail (end of file)
int64_t SerFileGetImageTimeStamp(FILE* vFile,int *vImageNumber);

#ifdef __cplusplus
}
#endif


