/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "serioclass.h"
#include <QMessageBox>

SerIOClass::SerIOClass(QString FullFilename, QString FileID, int Width, int Height, ASI_IMG_TYPE imgType)
{
    QMessageBox vMsgBox;
    vSerFileHeader.LuID = 0;
    vSerFileHeader.FrameCount = 0;
    vSerFileHeader.ImageHeight = Height;
    vSerFileHeader.ImageWidth = Width;
    vSerFileHeader.LittleEndian = 0;
    switch (imgType)
    {
        case ASI_IMG_RAW8:
            vSerFileHeader.ColorID = 0;
            vSerFileHeader.PixelDepthPerPlane = 8;
            break;
        case ASI_IMG_RAW16:
            vSerFileHeader.ColorID = 0;
            vSerFileHeader.PixelDepthPerPlane = 16;
            break;
        case ASI_IMG_RGB24:
            vSerFileHeader.ColorID = 100;
            vSerFileHeader.PixelDepthPerPlane = 8;
            break;
        case ASI_IMG_Y8:
            vSerFileHeader.ColorID = 0;
            vSerFileHeader.PixelDepthPerPlane = 8;
            break;
        case ASI_IMG_END: break;
    }
    if(FileID.count()>14)
        FileID = FileID.left(14);
    else
        for(int i=0;i<14;i++)
            vSerFileHeader.FileID[i] = 0;
    for(int i=0;i<40;i++)
    {
        vSerFileHeader.Observer[i] = 0;
        vSerFileHeader.Instrument[i] = 0;
        vSerFileHeader.Telescope[i] = 0;
    }
    std::string st = FileID.toStdString();
    memcpy(vSerFileHeader.FileID,st.data(),14);
    vSerFileHeader.DateTime_UTC = time(NULL);
    st = FullFilename.toStdString();
    vFile = SerFileOpenCreate(st.data(),&vSerFileHeader);
}

void SerIOClass::AddImage(uchar* Buffer)
{
    int64_t vTimeStamp = 0;
    SerFileAddImage(vFile,Buffer,&vTimeStamp);
}

void SerIOClass::SaveFile()
{
    SerFileClose(vFile);
}

int SerIOClass::GetParameter(SerFileHeaderPar Parameter, int32_t *Value)
{
    return SerFileReadHeaderParameter(vFile,Parameter,Value);
}

int SerIOClass::GetParameter(SerFileHeaderPar Parameter, int64_t *Value)
{
    return SerFileReadHeaderParameter(vFile,Parameter,Value);
}

int SerIOClass::GetParameter(SerFileHeaderPar Parameter, QString Value)
{
    char* vTmp;
    switch (Parameter)
    {
        case ser_FileID: vTmp = (char*)(malloc(14));
            break;
        default: vTmp = (char*)(malloc(40));
    }
    int result = SerFileReadHeaderParameter(vFile,Parameter,vTmp);
    Value = Value.fromUtf8(vTmp);
    free(vTmp);
    return result;
}

int SerIOClass::SetParameter(SerFileHeaderPar Parameter, int32_t Value)
{
    return SerFileWriteHeaderParameter(vFile,Parameter,&Value);
}

int SerIOClass::SetParameter(SerFileHeaderPar Parameter, int64_t Value)
{
    return SerFileWriteHeaderParameter(vFile,Parameter,&Value);
}

int SerIOClass::SetParameter(SerFileHeaderPar Parameter, QString Value)
{
    int vCount;
    switch (Parameter)
    {
        case ser_FileID:
        {
            vCount = 14;
        }
            break;
        default:
        {
            vCount = 40;
        }
    }

    Value = Value.leftJustified(vCount,0,true);
    return SerFileWriteHeaderParameter(vFile,Parameter,Value.data());
}
