/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SERIOCLASS_H
#define SERIOCLASS_H

#include "serfile.h"
#include <qstring.h>
#include <qchar.h>
#include <ASICamera2.h>

class SerIOClass
{
public:
    SerIOClass(QString FullFilename, QString FileID, int Width, int Height, ASI_IMG_TYPE imgType);
    void    AddImage(uchar* Buffer);
    void    SaveFile();
    int     GetParameter(SerFileHeaderPar Parameter, int32_t* Value);
    int     GetParameter(SerFileHeaderPar Parameter, int64_t* Value);
    int     GetParameter(SerFileHeaderPar Parameter, QString Value);
    int     SetParameter(SerFileHeaderPar Parameter, int32_t Value);
    int     SetParameter(SerFileHeaderPar Parameter, int64_t Value);
    int     SetParameter(SerFileHeaderPar Parameter, QString Value);
private:
    SerFileHeader   vSerFileHeader;
    FILE*           vFile;

};

#endif // SERIOCLASS_H
