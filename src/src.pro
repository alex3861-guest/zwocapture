# Created by and for Qt Creator. This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = zwocapture

icon.path = /usr/share/pixmaps
icon.files = ../ZWOx64.png

INSTALLS += icon

launcher.path = /usr/share/applications
launcher.files = ../zwo_capture.desktop

INSTALLS += launcher

target.path = /usr/bin

INSTALLS += target

docs.path = /usr/share/doc/zwocapture
docs.files = ../README

INSTALLS += docs


TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    zwostreaming.cpp \
    zwoFunctions.cpp \
    fitsioclass.cpp \
    faaimageprocessor.cpp \
    serioclass.cpp \
    serfile.c \
    tracking.cpp \
    imagereg.cpp \
    pidcontrol.cpp

HEADERS  += mainwindow.h \
    zwostreaming.h \
    zwoFunctions.h \
    fitsioclass.h \
    faaimageprocessor.h \
    serioclass.h \
    serfile.g \
    serfile.h \
    tracking.h \
    imagereg.h \
    pidcontrol.h \
    zwo/ASICamera2.h \
    zwo/EFW_filter.h \
    zwo/USB2ST4_Conv.h
FORMS    += mainwindow.ui \
    zwostreaming.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ -lcfitsio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/ -lcfitsio
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lcfitsio

unix:!macx: LIBS += -L$$PWD/../../Libraries/ -lserf

unix:!macx: LIBS += -L$$PWD/../../Libraries/ -lASICamera2



INCLUDEPATH += $$PWD/../../Libraries/Include
INCLUDEPATH += ./zwo
DEPENDPATH += $$PWD/../../Libraries/Include

DISTFILES += \
    ../README \
    ../copyright \
    zwo/ZWO_license.txt \
    ../debian/compat \
    ../debian/control \
    ../debian/copyright \
    ../debian/watch \
    ../debian/changelog \
    ../debian/rules \
    ../debian/README.Debian \
    ../debian/patches/series \
    ../debian/source/format \
    ../debian/source/local-options


unix:!macx: LIBS += -lopencv_core

unix:!macx: LIBS += -lopencv_imgproc

unix:!macx: LIBS += -lopencv_features2d

unix:!macx: LIBS += -lopencv_imgcodecs

unix:!macx: LIBS += -lEFWFilter

unix:!macx: LIBS += -lUSB2ST4Conv

unix:!macx: LIBS += -lOpenImageIO
