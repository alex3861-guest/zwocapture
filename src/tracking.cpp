/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tracking.h"

Tracking::Tracking(QObject *parent) : QObject(parent)
{
    imageReg();
    PIDcontrol();
    vI2Caddress = 0;
    vI2CFileName = "";
    vI2CConnected = false;
    vI2CRegisterData = 0xff;
    vI2CInterfaceIsSet = false;
    vDriverControlCalibrated = false;
    vInverseDCdirection = false;
    vInverseRAdirection = false;
    vGuidingOutput = Guiding_cameraST4port;
    setMovinCommands();
    vMovingTimer = new QTimer();
    vMovingTimer->setTimerType(Qt::PreciseTimer);
    vMovingTimer->stop();
    vMovingTimer->setSingleShot(true);
    connect(vMovingTimer,SIGNAL(timeout()),this,SLOT(on_MovingTimer()));
    vRAtrackingTimer = new QTimer();
    vRAtrackingTimer->setTimerType(Qt::PreciseTimer);
    vRAtrackingTimer->stop();
    vRAtrackingTimer->setSingleShot(true);
    connect(vRAtrackingTimer,SIGNAL(timeout()),this,SLOT(on_RAtrackingTimer()));
    vDCtrackingTimer = new QTimer();
    vDCtrackingTimer->setTimerType(Qt::PreciseTimer);
    vDCtrackingTimer->stop();
    vDCtrackingTimer->setSingleShot(true);
    connect(vDCtrackingTimer,SIGNAL(timeout()),this,SLOT(on_DCtrackingTimer()));
    vCalibrationStep = 0;
    vCalibrationIsRunning = false;
    vCalibrationData.MovingTime_ms = 1000;
    vShiftThreshold = 3.0;
    vTrackingRunning = false;
    vTrackingData.TrackingMoveTime = 200;
}

void Tracking::CalibrateDriverControl(QImage *image, QDateTime dateTime)
{
    if((vGuidingOutput == Guiding_customI2C_GPIO) ||
            (vGuidingOutput == Guiding_customI2C_GPIOanalog))
    {
        if(!vI2CInterfaceIsSet)
        {
            showMessage("Error!!! i2c bus is not set.");
            return;
        }
    }
    vCalibrationIsRunning = true;

    //go step1 - set base image and move forward N sec
    if(vCalibrationStep==0)
    {
        setBaseImage(image);
        if(vCalibrationData.Automatic)
        {
            MoveEast(vCalibrationData.MovingTime_ms);
            vCalibrationStep++;
            emit statusChanged("Calibration step 1");
        }
        else
        {
            vLastImgTime = dateTime;
            vCalibrationStep = 5;
            vMovingTimer->start(vShiftEstimationTime);
            emit statusChanged("Calibration step 5");
        }
        return;
    }
    if(vCalibrationStep==1)
    {
        vCalibrationData.RAshift = getShift(image);
        if(NANCheck(vCalibrationData.RAshift.x()))
            return;
        setBaseImage(image);
        vCalibrationStep++;
        MoveNorth(vCalibrationData.MovingTime_ms/5);
        emit statusChanged("Calibration step 2");
        return;
    }
    if(vCalibrationStep==2)
    {
        vCalibrationData.DCshift = getShift(image);
        if(NANCheck(vCalibrationData.DCshift.x()))
            return;
        double a,b;
        a =fabs(vCalibrationData.DCshift.x());
        b = fabs(vCalibrationData.DCshift.y());
        if((a > vShiftThreshold) |
           (b > vShiftThreshold))
        {
            vCalibrationStep++;
            setBaseImage(image);
            MoveNorth(vCalibrationData.MovingTime_ms);
            emit statusChanged("Calibration step 3");
            return;
        }
        else
        {
            MoveNorth(vCalibrationData.MovingTime_ms/5);
            emit statusChanged("Calibration step 2");
            return;
        }

    }
    if(vCalibrationStep==3)
    {
        vCalibrationData.DCshift = getShift(image);
        if(NANCheck(vCalibrationData.DCshift.x()))
            return;
        vCalibrationStep++;
        MoveSouth(vCalibrationData.MovingTime_ms);
        emit statusChanged("Calibration step 4");
        return;
    }
    if(vCalibrationStep==4)
    {
        vCalibrationData.DCgap = getShift(image);
        if(NANCheck(vCalibrationData.DCgap.x()))
            return;
        //RA tranformation angle
        double c = sqrt(pow(vCalibrationData.RAshift.x(),2.0)+pow(vCalibrationData.RAshift.y(),2.0));
        if(fabs(vCalibrationData.RAshift.x()) > fabs(vCalibrationData.RAshift.y()))
        {
            if(vCalibrationData.RAshift.y()>0)
                vCalibrationData.TrAngle.setX(acos(vCalibrationData.RAshift.x()/c));
            else
                vCalibrationData.TrAngle.setX(-acos(vCalibrationData.RAshift.x()/c));
        }
        else
        {
            vCalibrationData.TrAngle.setX(asin(vCalibrationData.RAshift.y()/c));
            if(vCalibrationData.RAshift.x()<0)
            {
                vCalibrationData.TrAngle.setX(M_PI - vCalibrationData.TrAngle.x());
            }
        }
        if(vCalibrationData.TrAngle.x() > M_PI)
            vCalibrationData.TrAngle.setX(vCalibrationData.TrAngle.x()-M_PI-M_PI);
        if(vCalibrationData.TrAngle.x() < -M_PI)
            vCalibrationData.TrAngle.setX(vCalibrationData.TrAngle.x()+M_PI+M_PI);

        //DC transformation angle
        c = sqrt(pow(vCalibrationData.DCshift.x(),2.0)+pow(vCalibrationData.DCshift.y(),2.0));
        if(fabs(vCalibrationData.DCshift.x()) > fabs(vCalibrationData.DCshift.y()))
        {
            if(vCalibrationData.DCshift.y()>0)
                vCalibrationData.TrAngle.setY(acos(vCalibrationData.DCshift.x()/c));
            else
                vCalibrationData.TrAngle.setY(-acos(vCalibrationData.DCshift.x()/c));
        }
        else
        {
            vCalibrationData.TrAngle.setY(asin(vCalibrationData.DCshift.y()/c));
            if(vCalibrationData.DCshift.x()<0)
            {
                vCalibrationData.TrAngle.setY(M_PI - vCalibrationData.TrAngle.y());
            }
        }
        if(vCalibrationData.TrAngle.y() > M_PI)
            vCalibrationData.TrAngle.setY(vCalibrationData.TrAngle.y()-M_PI-M_PI);
        if(vCalibrationData.TrAngle.x() < -M_PI)
            vCalibrationData.TrAngle.setY(vCalibrationData.TrAngle.y()+M_PI+M_PI);
        if(((vCalibrationData.TrAngle.x()<-M_PI_2)&&(vCalibrationData.TrAngle.y()>M_PI_2)) ||
                ((vCalibrationData.TrAngle.x()>M_PI_2)&&(vCalibrationData.TrAngle.y()<-M_PI_2)))
        {
            double difference;
            if(vCalibrationData.TrAngle.x()<-M_PI_2)
                difference = vCalibrationData.TrAngle.y()-(M_PI+M_PI+vCalibrationData.TrAngle.x());
            else
                difference = M_PI+M_PI+vCalibrationData.TrAngle.y()-(vCalibrationData.TrAngle.x());
            if(difference <0)
                vCalibrationData.PolarDirection = -1;
            else
                vCalibrationData.PolarDirection = 1;
        }
        else
        {
            if((vCalibrationData.TrAngle.y() < vCalibrationData.TrAngle.x()))
                vCalibrationData.PolarDirection = -1;
            else
                vCalibrationData.PolarDirection = 1;
        }

        vCalibrationData.RAvelocity = c*1000.0/vCalibrationData.MovingTime_ms;
        c = sqrt(pow(vCalibrationData.DCshift.x(),2.0)+pow(vCalibrationData.DCshift.y(),2.0));
        vCalibrationData.DCvelocity = c*1000.0/vCalibrationData.MovingTime_ms;
        setBaseImage(image);
        vLastImgTime = dateTime;
        vCalibrationStep++;
        vMovingTimer->start(vShiftEstimationTime);
        emit statusChanged("Calibration step 5");
        return;
    }
    if(vCalibrationStep==5)
    {
        vCalibrationData.EstimatedShift = shiftTransformation(getShift(image));
        qint64 interval = vLastImgTime.msecsTo(dateTime)/1000;//seconds

        vCalibrationData.DCestimatedVelocity = vCalibrationData.EstimatedShift.y()/double(interval);
        vCalibrationData.RAestimatedVelocity = vCalibrationData.EstimatedShift.x()/double(interval);

        vCalibrationIsRunning = false;
        vCalibrationStep = 0;
        RApidcontrol.setOutputRange(-vCalibrationData.RAvelocity,vCalibrationData.RAvelocity);
        RApidcontrol.setOutputVal(vCalibrationData.RAestimatedVelocity);
        DCpidcontrol.setOutputRange(-vCalibrationData.DCvelocity,vCalibrationData.DCvelocity);
        DCpidcontrol.setOutputVal(0.0);
        vDriverControlCalibrated = true;
        emit statusChanged("Calibration completed");
        emit driverControlCalibrated(vCalibrationData);
    }
}

QJsonObject Tracking::getTrakingProfile()
{

}

QJsonObject Tracking::getRegProfile()
{
    return getProfile();
}

void Tracking::StopCalibration()
{
    vCalibrationIsRunning = false;
    vCalibrationStep = 0;
    StopMoving();
}

void Tracking::SetI2C(QString i2cdev, uchar i2caddress)
{
    vI2CFileName = i2cdev;
    vI2Caddress = i2caddress;
    if(PrepareI2CFile())
    {
        showMessage("Error!!! Failed to connect.");
    }
    else
        vI2CInterfaceIsSet = true;
}

void Tracking::setCalibrationMovingTime(int time)
{
    vCalibrationData.MovingTime_ms = time;
}

void Tracking::setDriverClibrationAuto(bool Auto)
{
    vCalibrationData.Automatic = Auto;
}

void Tracking::setCurrentImage(QImage *image)
{
    QDateTime currentTime = QDateTime().currentDateTime();
    if(!vTrackingRunning)
        return;
    QPointF outputShift = getShift(image);
    emit shiftCalculated(outputShift);
    if(isnan(outputShift.x()) |
       isnan(outputShift.y()))
    {
        emit DriversStoped();
        return;
    }
    QPointF shift = shiftTransformation(outputShift);
    vTrackingData.RAvelocity = RApidcontrol.calculate(shift.x());
    vTrackingData.DCvelocity = DCpidcontrol.calculate(shift.y());
    if(vOngoingControl)
    {
        vTrackingData.RAtrackInterval = int(-(vCalibrationData.RAvelocity * vTrackingData.TrackingMoveTime)/
                                            vTrackingData.RAvelocity);
        int newInterval = abs(vTrackingData.RAtrackInterval);
        int pastTime = vRAtrackingTimer->interval() - vRAtrackingTimer->remainingTime();
        if(pastTime >= newInterval)
            vRAtrackingTimer->start(0);
        else
            vRAtrackingTimer->start(newInterval - pastTime);

        vTrackingData.DCtrackInterval = int(-(vCalibrationData.DCvelocity * vTrackingData.TrackingMoveTime)/
                                            vTrackingData.DCvelocity);
        newInterval = abs(vTrackingData.DCtrackInterval);
        pastTime = vDCtrackingTimer->interval() - vDCtrackingTimer->remainingTime();
        if(pastTime >= newInterval)
            vDCtrackingTimer->start(0);
        else
            vDCtrackingTimer->start(newInterval - pastTime);
    }
    else
    {
        vTrackingData.RAtrackInterval = int(vTrackingData.RAvelocity*1000.0/vCalibrationData.RAvelocity);
        vTrackingData.DCtrackInterval = int(vTrackingData.DCvelocity*1000.0/vCalibrationData.DCvelocity);
        startRAmoving();
    }
    vLastImgTime = currentTime;
    emit trackingDataCalculated(vTrackingData);
}

void Tracking::setGuidingOutput(int GuidingID,int GuidingOutput)
{
    vGuidingOutput = GuidingOutput;
    vGuidingID = GuidingID;
    setMovinCommands();
}

void Tracking::setInverseDirection(bool inverseRA, bool inverseDC)
{
    if(inverseRA)
        vInverseRAdirection = true;
    else
        vInverseRAdirection = false;
    if(inverseDC)
        vInverseDCdirection = true;
    else
        vInverseDCdirection = false;
}

void Tracking::setShiftEstimationTime(int time_ms)
{
    vShiftEstimationTime = time_ms;
}

void Tracking::setShiftThreshold(double threshold)
{
    vShiftThreshold = threshold;
}

void Tracking::setCalRAvelocity(double velocity)
{
    vCalibrationData.RAvelocity = velocity;
}

void Tracking::setCalDCvelocity(double velocity)
{
    vCalibrationData.DCvelocity = velocity;
}

void Tracking::setCalTrAngle(double angle)
{
    vCalibrationData.TrAngle.setX(angle);
}

void Tracking::setTrackingProfile(QJsonObject *object)
{

}

void Tracking::setRegProfile(QJsonObject *object)
{
    setProfile(object);
}

void Tracking::StartTracking(bool ongoing)
{
    if(!vDriverControlCalibrated)
    {
        showMessage("Error!!! Drivers control is not calibrated.");
        return;
    }
    RApidcontrol.setOutputVal(vCalibrationData.RAestimatedVelocity);
    DCpidcontrol.setOutputVal(0.0);
    vOngoingControl = ongoing;
    if(ongoing)
    {
        vTrackingData.RAvelocity = vCalibrationData.RAestimatedVelocity;
        vTrackingData.RAvelocityIncrement = fabs(vCalibrationData.RAestimatedVelocity/10.0);
        vTrackingData.DCvelocity = vCalibrationData.DCestimatedVelocity;
        vTrackingData.DCvelocityIncrement = fabs(vCalibrationData.DCestimatedVelocity/10.0);
        double a = -(vCalibrationData.RAvelocity * vTrackingData.TrackingMoveTime)/
                     vTrackingData.RAvelocity;
        vTrackingData.RAtrackInterval = int(a);
        a = -(vCalibrationData.DCvelocity * vTrackingData.TrackingMoveTime)/
                vTrackingData.DCvelocity;
        vTrackingData.DCtrackInterval = int(a);
        vRAtrackingTimer->start(abs(vTrackingData.RAtrackInterval));
        vDCtrackingTimer->start(abs(vTrackingData.DCtrackInterval));
    }
    vLastImgTime = vLastImgTime.currentDateTime();
    vTrackingRunning = true;
}

void Tracking::StopTracking()
{
    vTrackingRunning = false;
    vRAtrackingTimer->stop();
    vDCtrackingTimer->stop();
    emit DriversStoped();
}

void Tracking::StartMoving(uchar direction, int movingTime)
{
    if(vMovingTimer->isActive())
        return;
    vRegisterData = direction;
    vMovingTimer->setInterval(movingTime);
    switch (vGuidingOutput)
    {
    case Guiding_cameraST4port:
        ASIPulseGuideOn(vGuidingID,ASI_GUIDE_DIRECTION(vRegisterData));
        break;
    case Guiding_USB2ST4adapter:
        USB2ST4PulseGuide(vGuidingID,USB2ST4_DIRECTION(vRegisterData),true);
        break;
    case Guiding_customI2C_GPIO:
        write(vI2CFile, &vRegisterData, 1);
        break;
    case Guiding_customI2C_GPIOanalog:
        break;
    default:
        break;
    }
    if(vGuidingOutput!=Guiding_customI2C_GPIOanalog)
        vMovingTimer->start();

    QString vMovingStatusString;
    if(vRegisterData == vMovingEast)
        vMovingStatusString = "Moving east";
    if(vRegisterData == vMovingWest)
        vMovingStatusString = "Moving west";
    if(vRegisterData == vMovingNorth)
        vMovingStatusString = "Moving north";
    if(vRegisterData == vMovingSouth)
        vMovingStatusString = "Moving south";
    emit statusChanged(vMovingStatusString);
}

void Tracking::MoveNorth(int time)
{
    StartMoving(vMovingNorth,time);
}

void Tracking::MoveSouth(int time)
{
    StartMoving(vMovingSouth,time);
}

void Tracking::MoveEast(int time)
{
    StartMoving(vMovingEast,time);
}

void Tracking::MoveWest(int time)
{
    StartMoving(vMovingWest,time);
}

void Tracking::StopMoving()
{
    switch (vGuidingOutput)
    {
    case Guiding_cameraST4port:
        ASIPulseGuideOff(vGuidingID,ASI_GUIDE_EAST);
        ASIPulseGuideOff(vGuidingID,ASI_GUIDE_WEST);
        ASIPulseGuideOff(vGuidingID,ASI_GUIDE_NORTH);
        ASIPulseGuideOff(vGuidingID,ASI_GUIDE_SOUTH);
        break;
    case Guiding_USB2ST4adapter:
        USB2ST4PulseGuide(vGuidingID,USB2ST4_EAST,false);
        USB2ST4PulseGuide(vGuidingID,USB2ST4_WEST,false);
        USB2ST4PulseGuide(vGuidingID,USB2ST4_NORTH,false);
        USB2ST4PulseGuide(vGuidingID,USB2ST4_SOUTH,false);
        break;
    case Guiding_customI2C_GPIO:
        vRegisterData = 0xFF;
        write(vI2CFile, &vRegisterData, 1);
        break;
    case Guiding_customI2C_GPIOanalog:
        break;
    default:
        break;
    }
    vMovingTimer->stop();
    emit statusChanged("Idle");


}

int Tracking::PrepareI2CFile()
{
    std::string vFileName;
    if (vI2Caddress > 127)
    {
        showMessage("Error!!! I2C Device address is wrong.");
        return 1;
    }
    vFileName = vI2CFileName.toStdString();
    vI2CFile = open(vFileName.data(), O_RDWR);
    if (vI2CFile<0)
    {
        showMessage("Error!!! Probably wrong i2c bus file name.");
        return 1;
    }
    ioctl(vI2CFile,I2C_SLAVE,vI2Caddress);
    vI2CRegisterData = 0xFF;
    ssize_t vsize = write(vI2CFile, &vI2CRegisterData, 1);
    if(vsize!=1)
    {
        vI2CConnected = false;
        emit i2c_connected_changed(false);
        emit statusChanged("i2c connection failed");
        close(vI2CFile);
        return 1;
    }
    else
    {
        vI2CConnected = true;
        emit i2c_connected_changed(true);
        emit statusChanged("i2c connected");
        return 0;
    }
}

void Tracking::showMessage(QString msgString)
{
    QMessageBox msgbox;
    msgbox.setText(msgString);
    msgbox.exec();
}

void Tracking::setMovinCommands()
{
    switch (vGuidingOutput) {
    case Guiding_cameraST4port:
        vMovingEast = ASI_GUIDE_EAST;
        vMovingWest = ASI_GUIDE_WEST;
        vMovingNorth = ASI_GUIDE_NORTH;
        vMovingSouth = ASI_GUIDE_SOUTH;
        break;
    case Guiding_USB2ST4adapter:
        vMovingEast = USB2ST4_EAST;
        vMovingWest = USB2ST4_WEST;
        vMovingNorth = USB2ST4_NORTH;
        vMovingSouth = USB2ST4_SOUTH;
        break;
    case Guiding_customI2C_GPIO:
        vMovingEast = MovingEast;
        vMovingWest = MovingWest;
        vMovingNorth = MovingNorth;
        vMovingSouth = MovingSouth;
        break;
    case Guiding_customI2C_GPIOanalog:
        break;
    }
}

bool Tracking::NANCheck(double value)
{
    if(isnan(value)!=0)
    {
        showMessage("No keypoints matched!\nCalibration is aborted!");
        emit statusChanged("No keypoints matched!\nCalibration is aborted!");
        return true;
    }
    return false;
}

QPointF Tracking::shiftTransformation(QPointF shift)
{
    QPointF outputShift;
    double r,alfa;
    //convert to polar coordinates
    r = sqrt(pow(shift.x(),2)+pow(shift.y(),2));
    if(r < 1.5)
    {
        outputShift.setX(0);
        outputShift.setY(0);
    }
    else
    {
        alfa = acos(shift.x()/r);
        if(shift.y()<0)
            alfa *= -1;

        //add calibration angle
        alfa -= vCalibrationData.TrAngle.x();

        //convert to cartesian coordinates
        outputShift.setX(r*cos(alfa));
        outputShift.setY(r*sin(alfa)*vCalibrationData.PolarDirection);
    }
    return outputShift;
}

void Tracking::startRAmoving()
{
    if(vTrackingRunning)
    {
        uchar RegData;
        if(vTrackingData.RAtrackInterval<0)
            RegData = vMovingWest;
        else
            RegData = vMovingEast;
        if(vOngoingControl)
        {
            StartMoving(RegData,vTrackingData.TrackingMoveTime);
            vRAtrackingTimer->start(abs(vTrackingData.RAtrackInterval));
        }
        else
        {
            vTrackingData.RAmoving = true;
            if(abs(vTrackingData.RAtrackInterval)<vTrackingData.TrackingMoveTime)
                on_MovingTimer();
            else
                StartMoving(RegData,abs(vTrackingData.RAtrackInterval));
        }
    }
}

void Tracking::startDCmoving()
{
    if(vTrackingRunning)
    {
        uchar RegData;
        if(vTrackingData.DCtrackInterval<0)
            RegData = vMovingNorth;
        else
            RegData = vMovingSouth;
        if(vOngoingControl)
        {
            StartMoving(RegData,vTrackingData.TrackingMoveTime);
            vDCtrackingTimer->start(abs(vTrackingData.DCtrackInterval));
        }
        else
        {
            vTrackingData.DCmoving = true;
            if(abs(vTrackingData.DCtrackInterval)<vTrackingData.TrackingMoveTime)
                on_MovingTimer();
            else
                StartMoving(RegData,abs(vTrackingData.DCtrackInterval));
        }
    }
}

void Tracking::on_MovingTimer()
{
    switch (vGuidingOutput)
    {
    case Guiding_cameraST4port:
        ASIPulseGuideOff(vGuidingID,ASI_GUIDE_DIRECTION(vRegisterData));
        break;
    case Guiding_USB2ST4adapter:
        USB2ST4PulseGuide(vGuidingID,USB2ST4_DIRECTION(vRegisterData),false);
        break;
    case Guiding_customI2C_GPIO:
        vRegisterData = 0xFF;
        write(vI2CFile, &vRegisterData, 1);
        break;
    case Guiding_customI2C_GPIOanalog:
        break;
    default:
        break;
    }

    emit statusChanged("Idle");
    if(vCalibrationIsRunning)
    {
        emit imageRequest();
    }
    if(vTrackingRunning & !vOngoingControl)
    {
        if(vTrackingData.RAmoving)
        {
            vTrackingData.RAmoving = false;
            startDCmoving();
        }
        else
        {
            if(vTrackingData.DCmoving)
            {
                vTrackingData.DCmoving = false;
                emit DriversStoped();
            }
        }
    }
}

void Tracking::on_RAtrackingTimer()
{
    startRAmoving();
}

void Tracking::on_DCtrackingTimer()
{
    startDCmoving();
}

