/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2019>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKING_H
#define TRACKING_H

#include <QObject>
#include <QTimer>
#include <QImage>
#include <QMessageBox>
#include <QDateTime>
#include <QJsonObject>

//opencv
#ifdef OPENCV_3
#include <opencv2/core/cvstd.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/hal/interface.h>
#include <opencv2/imgproc.hpp>
#else
#include <opencv2/core/core.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/core/
#endif

//i2c control
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>

//ZWO
#include "zwo/ASICamera2.h"
#include "zwo/EFW_filter.h"
#include "zwo/USB2ST4_Conv.h"

//local
#include "imagereg.h"
#include "pidcontrol.h"


class Tracking : public QObject, public imageReg
{
    Q_OBJECT
public:
    explicit        Tracking(QObject *parent = 0);
    void            CalibrateDriverControl(QImage *image,QDateTime dateTime = QDateTime().currentDateTime());
    QJsonObject     getTrakingProfile();
    QJsonObject     getRegProfile();
//    CalibrationData GetCalibrationData();
    void            StopCalibration();
    void            SetI2C(QString i2cdev,uchar i2caddress);
    void            setCalibrationMovingTime(int time);
//    void            setCalibrationData(CalibrationData data);
    void            setDriverClibrationAuto(bool Auto);
    void            setCurrentImage(QImage *image);
    void            setGuidingOutput(int GuidingID,int GuidingOutput);
    void            setInverseDirection(bool inverseRA, bool inverseDC);
    void            setShiftEstimationTime(int time_ms);
    void            setShiftThreshold(double threshold);
    void            setCalRAvelocity(double velocity);
    void            setCalDCvelocity(double velocity);
    void            setCalTrAngle(double angle);
    void            setTrackingProfile(QJsonObject *object);
    void            setRegProfile(QJsonObject *object);
    void            StartTracking(bool ongoing);
    void            StopTracking();
    void            StartMoving(uchar direction,int movingTime);
    void            MoveNorth(int time);
    void            MoveSouth(int time);
    void            MoveEast(int time);
    void            MoveWest(int time);
    void            StopMoving();

    enum movingDirection_I2C_GPIO
    {
        MovingEast = 0xFE,
        MovingWest = 0xFD,
        MovingNorth = 0xFB,
        MovingSouth = 0xF7
    } movingDirection;

    enum guidingOutput
    {
        Guiding_NotSupported = 0,
        Guiding_cameraST4port = 1,
        Guiding_USB2ST4adapter = 2,
        Guiding_customI2C_GPIO = 4,
        Guiding_customI2C_GPIOanalog = 8
    };

    struct CalibrationData
    {
        int     MovingTime_ms;      //miliseconds
        QPointF RAshift;            //pixel
        double  RAvelocity;         //pixel per second
        QPointF DCshift;            //pixel
        double  DCvelocity;         //pixel per second
        QPointF DCgap;              //pixel
        QPointF TrAngle;            //transformation angles
        QPointF EstimatedShift;     //estimated shift
        int     PolarDirection;     //direction of polar coordinates 1-clockwise, -1-counterclockwise
        double  RAestimatedVelocity;
        double  DCestimatedVelocity;
        bool    Automatic;
    };
    struct TrackingData
    {
        int         RAtrackInterval;       //milliseconds
        int         DCtrackInterval;       //milliseconds
        int         TrackingMoveTime;      //milliseconds
        int         RAincrementTime;       //ms
        int         DCincrementTime;       //ms
        bool        RAmoving;
        bool        DCmoving;
        double      RAvelocityIncrement;    //pixel per second
        double      DCvelocityIncrement;
        double      RAvelocity;
        double      DCvelocity;
    };

    PIDcontrol  RApidcontrol;
    PIDcontrol  DCpidcontrol;


signals:
    void        i2c_connected_changed(bool);
    void        driverControlCalibrated(Tracking::CalibrationData data);
    void        imageRequest();
    void        shiftCalculated(QPointF shift);
    void        statusChanged(QString str);
    void        trackingDataCalculated(Tracking::TrackingData data);
    void        DriversStoped();

public slots:


private:
    int         PrepareI2CFile();
    void        showMessage(QString msgString);
    void        setMovinCommands();
    bool        NANCheck(double value);
    QPointF     shiftTransformation(QPointF shift);
    void        startRAmoving();
    void        startDCmoving();

    //opencv variables
//    cv::Mat     vBaseMatrix;
//    bool        vBaseMatrixIsSet;

    //I2C variable
    QTimer  *vMovingTimer;
    QTimer  *vRAtrackingTimer;
    QTimer  *vDCtrackingTimer;
    QString vI2CFileName;
    uchar   vI2Caddress;
    int     vI2CFile;
    uchar   vI2CRegisterData;
    bool    vI2CConnected;
    bool    vInverseRAdirection;
    bool    vInverseDCdirection;
    uchar   vMovingEast;
    uchar   vMovingWest;
    uchar   vMovingNorth;
    uchar   vMovingSouth;
    uchar   vRegisterData;


    //tracking variables

    int         vGuidingOutput;
    int         vGuidingID;
    bool        vDriverControlCalibrated;
    bool        vI2CInterfaceIsSet;
    bool        vCalibrationIsRunning;
    bool        vOngoingControl;
    bool        vTrackingRunning;
    double      vShiftInterval_s;
    CalibrationData vCalibrationData;
    TrackingData    vTrackingData;
    int         vCalibrationStep;
    int         vShiftEstimationTime;   //milliseconds
    double      vShiftThreshold;
    QDateTime   vLastImgTime;
    uchar       vRAtrackingMove;
    uchar       vDCtrackingMove;
    QPointF     vCurrentShift;

private slots:
    void    on_MovingTimer();
    void    on_RAtrackingTimer();
    void    on_DCtrackingTimer();
};

#endif // TRACKING_H
