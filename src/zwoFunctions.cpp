/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "zwoFunctions.h"



//zwo ASI_ERROR_CODE processing
QString zwoErrorProcessing(ASI_ERROR_CODE verror)
{
    QString vString;
    switch (verror)
    {
        case ASI_SUCCESS: vString = vString.fromStdString("Operation successful");
            break;
        case ASI_ERROR_INVALID_INDEX: vString = vString.fromStdString("Error!!! Invalid camera index");
            break;
        case ASI_ERROR_INVALID_ID: vString = vString.fromStdString("Error!!! Invalid camera ID");
            break;
        case ASI_ERROR_INVALID_CONTROL_TYPE: vString = vString.fromStdString("Error!!! Invalid control type");
            break;
        case ASI_ERROR_CAMERA_CLOSED: vString = vString.fromStdString("Error!!! Camera isn't opened");
            break;
        case ASI_ERROR_CAMERA_REMOVED: vString = vString.fromStdString("Error!!! Can't find camera, maybe it is removed");
            break;
        case ASI_ERROR_INVALID_PATH: vString = vString.fromStdString("Error!!! Can't find the file");
            break;
        case ASI_ERROR_INVALID_FILEFORMAT: vString = vString.fromStdString("Error!!! Invalid file format");
            break;
        case ASI_ERROR_INVALID_SIZE: vString = vString.fromStdString("Error!!! Invalid image size");
            break;
        case ASI_ERROR_INVALID_IMGTYPE: vString = vString.fromStdString("Error!!! Invalid image type");
            break;
        case ASI_ERROR_INVALID_MODE: vString = vString.fromStdString("Error!!! Invalid Mode");
            break;
        case ASI_ERROR_OUTOF_BOUNDARY: vString = vString.fromStdString("Error!!! The start coordinate is out of boundary");
            break;
        case ASI_ERROR_TIMEOUT: vString = vString.fromStdString("Error!!! Time out");
            break;
        case ASI_ERROR_INVALID_SEQUENCE: vString = vString.fromStdString("Error!!! Invalid operate sequence");
            break;
        case ASI_ERROR_BUFFER_TOO_SMALL: vString = vString.fromStdString("Error!!! image buffer isn't big enough");
            break;
        case ASI_ERROR_VIDEO_MODE_ACTIVE: vString = vString.fromStdString("Error!!! Video capture is working, can't snap");
            break;
        case ASI_ERROR_EXPOSURE_IN_PROGRESS: vString = vString.fromStdString("Error!!! snap is working, can't capture video");
            break;
        case ASI_ERROR_GENERAL_ERROR: vString = vString.fromStdString("Error!!! General error");
            break;
        case ASI_ERROR_END: break;
    }
    return vString;
}

//convert string image type to ASI_IMG_TYPE
ASI_IMG_TYPE fStrToAsiImgType(QString *vString)
{
    ASI_IMG_TYPE vImgType = ASI_IMG_END;
    if(*vString=="RAW8")
        vImgType = ASI_IMG_RAW8;
    if(*vString=="RAW16")
        vImgType = ASI_IMG_RAW16;
    if(*vString=="RGB24")
        vImgType = ASI_IMG_RGB24;
    if(*vString=="Y8")
        vImgType = ASI_IMG_Y8;
    return vImgType;
}

//convert ASI_IMG_TYPE to string
QString fImgTypeToString(ASI_IMG_TYPE vImgType)
{
    QString vLocalString;
    switch (vImgType)
    {
        case ASI_IMG_RAW8: vLocalString = vLocalString.fromStdString("RAW8");
            break;
        case ASI_IMG_RAW16: vLocalString = vLocalString.fromStdString("RAW16");
            break;
        case ASI_IMG_RGB24: vLocalString = vLocalString.fromStdString("RGB24");
            break;
        case ASI_IMG_Y8: vLocalString = vLocalString.fromStdString("Y8");
            break;
        case ASI_IMG_END: break;
    }
    return vLocalString;
}

//Return bytes number for given image type
int fImgTypeSize(ASI_IMG_TYPE vImgType)
{
    int vOutput = 0;
    switch (vImgType)
    {
        case ASI_IMG_RAW8: vOutput = 1;
            break;
        case ASI_IMG_RAW16: vOutput = 2;
            break;
        case ASI_IMG_RGB24: vOutput = 3;
            break;
        case ASI_IMG_Y8: vOutput = 1;
            break;
        case ASI_IMG_END: break;
    }
    return vOutput;
}

//convert ASI_BOOL to string "Yes" or "No"
QString fBoolToString(ASI_BOOL vBool)
{
    QString vString;
    if(vBool == ASI_TRUE)
        vString = vString.fromStdString("Yes");
    else
        vString = vString.fromStdString("No");
    return vString;
}

bool fYesNoMsgBox(QString vText,QString vInformativeText)
{
    QMessageBox     vMsgBox;
    vMsgBox.setText(vText);
    vMsgBox.setInformativeText(vInformativeText);
    vMsgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    vMsgBox.setDefaultButton(QMessageBox::No);
    int out = vMsgBox.exec();
    if(out==QMessageBox::Yes)
        return true;
    else
        return false;
}

void fOkMsgBox(QString vText,QString vInformativeText)
{
    QMessageBox     vMsgBox;
    vMsgBox.setText(vText);
    vMsgBox.setInformativeText(vInformativeText);
    vMsgBox.setStandardButtons(QMessageBox::Ok);
    vMsgBox.setDefaultButton(QMessageBox::Ok);
    vMsgBox.exec();
}

void fConfigFileCheck(QString vPath)
{
    QString str;
    QDir *vdir;
    vdir = new QDir(vPath);
    bool vstatus = vdir->exists(vPath);
    if(vstatus)
        delete vdir;
    else
    {
        str = vdir->homePath() + "/.Faa";
        if(!vdir->exists(str))
            vdir->mkdir(str);
        QFile *vfile;
        vfile = new QFile(vdir->path());
        if(vfile->open(QIODevice::ReadWrite))
        {
            vfile->write(vdir->homePath().toUtf8());
            vfile->close();
        }
        else
            fOkMsgBox("Warning!!!", "Can not create configuration file!!");
        delete vdir;
    }
}

QString fSecondsToHMSString(int seconds)
{
    QString string1,tmpString;
    int tmpResult;
    tmpResult = seconds/3600;
    if(tmpResult!=0)
        tmpString = string1.setNum(tmpResult)+"h ";
    tmpResult = (seconds%3600)/60;
    if(tmpResult!=0)
        tmpString += string1.setNum(tmpResult)+"m ";
    tmpResult = seconds%60;
    tmpString += string1.setNum(tmpResult)+"s";
    return tmpString;
}
