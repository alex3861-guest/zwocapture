/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ZWOERROR_H
#define ZWOERROR_H

#endif // ZWOERROR_H

#include <ASICamera2.h>
#include <qstring.h>
#include <QMessageBox>
#include <qdir.h>
#include <qfile.h>
#include <qiodevice.h>


QString         zwoErrorProcessing(ASI_ERROR_CODE verror);
ASI_IMG_TYPE    fStrToAsiImgType(QString *vString);
QString         fImgTypeToString(ASI_IMG_TYPE vImgType);
QString         fBoolToString(ASI_BOOL vBool);
QString         fSecondsToHMSString(int seconds);
int             fImgTypeSize(ASI_IMG_TYPE vImgType);
bool            fYesNoMsgBox(QString vText,QString vInformativeText);
void            fOkMsgBox(QString vText,QString vInformativeText);
void            fConfigFileCheck(QString vPath);


