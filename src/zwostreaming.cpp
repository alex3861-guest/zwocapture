/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "zwostreaming.h"
#include "ui_zwostreaming.h"

zwoStreaming::zwoStreaming(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::zwoStreaming)
{
    vImage = nullptr;
    vVideoBuffer = nullptr;
    vPreviewOutBuffer = nullptr;
    vTrackingImageBuffer = nullptr;
    vPreviewInBuffer = nullptr;
    vScale = 0.25;
    vScaleChange = 0;
    vShooting = false;
    vVideoRecording = false;
    vPaused = false;
    vStreaming = false;
    ui->setupUi(this);
    ui->sbPictureRefreshTime->setEnabled(false);
    ui->sbPictureRefreshTime->setValue(20);
    ui->chbTrackingControlEnabled->setChecked(false);
    on_chbTrackingControlEnabled_toggled(false);
    vRefreshTimer = new QTimer();
    vRefreshTimer->setSingleShot(true);
    vRefreshTimer->setInterval(ui->sbPictureRefreshTime->value());
    vTimer1 = new QTimer();
    vTimer1->stop();
    vTimer1->setInterval(20);
    vTimer1->setSingleShot(false);
    vCountTimer = new QTimer();
    vCountTimer->setInterval(1000);
    vCountTimer->setSingleShot(false);
    vCountTimer->stop();
    connect(vTimer1, SIGNAL(timeout()), this, SLOT(on_Timer1()));
    connect(vCountTimer, SIGNAL(timeout()),this,SLOT(on_vCountTimer()));

    //tracking initialization
    vTracking = new Tracking();
    connect(vTracking,SIGNAL(statusChanged(QString)),this,SLOT(on_TrackingStatusChanged(QString)));
    connect(vTracking,SIGNAL(imageRequest()),this,SLOT(on_Tracking_calibrationImgRequest()));
    connect(vTracking,SIGNAL(driverControlCalibrated(Tracking::CalibrationData)),
            this,SLOT(on_Tracking_CalibrationCompleted(Tracking::CalibrationData)));
    connect(vTracking,SIGNAL(shiftCalculated(QPointF)),this,SLOT(on_TrackingShiftCalculated(QPointF)));
    connect(vTracking,SIGNAL(trackingDataCalculated(Tracking::TrackingData)),this,SLOT(on_TrackingDataCalculated(Tracking::TrackingData)));
    connect(vTracking,SIGNAL(DriversStoped()),this,SLOT(on_TrackingDriversstoped()));
    this->setUpdatesEnabled(true);
    vTrackingRunning = false;
    vTrackingDriversMoving = false;
    vCalibrationImageRequest = false;
    vCalibrationTimer = new QTimer();
    vCalibrationTimer->stop();
    vCalibrationTimer->setSingleShot(true);
    connect(vCalibrationTimer,SIGNAL(timeout()),this,SLOT(on_vCalibrationTimer_Timer()));
    {
        vBlobParams.blobColor =255;
        ui->sbBlobColor->setValue(255);
        ui->sbMinRepeatability->setValue(int(vBlobParams.minRepeatability));
        ui->dsMaxArea->setValue(double(vBlobParams.maxArea));
        ui->dsMinArea->setValue(double(vBlobParams.minArea));
        ui->dsMaxCircularity->setValue(double(vBlobParams.maxCircularity));
        ui->dsMinCircularity->setValue(double(vBlobParams.minCircularity));
        ui->dsMaxConvexity->setValue(double(vBlobParams.maxConvexity));
        ui->dsMinConvexity->setValue(double(vBlobParams.minConvexity));
        ui->dsMaxInertia->setValue(double(vBlobParams.maxInertiaRatio));
        ui->dsMinInertia->setValue(double(vBlobParams.minInertiaRatio));
        ui->dsMaxThreshold->setValue(double(vBlobParams.maxThreshold));
        ui->dsMinThreshold->setValue(double(vBlobParams.minThreshold));
        ui->dsMinDistance->setValue(double(vBlobParams.minDistBetweenBlobs));
        ui->dsThreshStep->setValue(double(vBlobParams.thresholdStep));
        ui->chbAreaFilter->setChecked(vBlobParams.filterByArea);
        ui->chbCirculFilter->setChecked(vBlobParams.filterByCircularity);
        ui->chbColorFilter->setChecked(vBlobParams.filterByColor);
        ui->chbConvexityFilter->setChecked(vBlobParams.filterByConvexity);
        ui->chbInertiaFilter->setChecked(vBlobParams.filterByInertia);
    }
    {
        ui->leCurrentLatitude->setInputMask("#00,00,00");
        ui->leTargetLatitude->setInputMask("#00,00,00");
        ui->leCurrentLongitude->setInputMask("00,00,00");
        ui->leTargetLongitude->setInputMask("00,00,00");
    }
}

zwoStreaming::~zwoStreaming()
{
    delete ui;
}

int zwoStreaming::timeFromString(QString string)
{
    int Time=0, vSign;
    QStringList stringList = string.split(",");
    if(stringList.count()>0)
        Time = stringList[0].toInt()*3600;
    if(Time>0)
        vSign = 1;
    else
        vSign = -1;
    Time += stringList[1].toInt()*vSign*60;
    Time += stringList[2].toInt()*vSign;
    return Time;
}

double zwoStreaming::getDecMovingTime()
{
    int vDecInitTime, vDecFinalTime;
    vDecInitTime = timeFromString(ui->leCurrentLatitude->text());
    vDecFinalTime = timeFromString(ui->leTargetLatitude->text());
    return ((vDecFinalTime - vDecInitTime)*1000)/15.0/ui->cbMountMovingSpeed->currentText().toDouble(); // time in milliseconds
}

int zwoStreaming::getRAMovingTime()
{
    int vHalfDaySec = 43200; //half of the day duration in seconds
    int vRAInitTime, vRAFinalTime, vRAMovingTime;

    vRAInitTime = timeFromString(ui->leCurrentLongitude->text());
    vRAFinalTime = timeFromString(ui->leTargetLongitude->text());

    vRAMovingTime = (vRAFinalTime - vRAInitTime);
    if(vRAMovingTime < -vHalfDaySec)
    {
        vRAMovingTime += vHalfDaySec*2;
    }
    else
    {
        if(vRAMovingTime > vHalfDaySec)
        {
            vRAMovingTime -= vHalfDaySec*2;
        }
    }
    vRAMovingTime *= 1000; //time in milliseconds
    return vRAMovingTime/ui->cbMountMovingSpeed->currentText().toInt();
}

void zwoStreaming::StartStriming(ASI_CAMERA_INFO* CameraInfo,int vWidth,int vHeight, ASI_IMG_TYPE vImageType, int guidingPorts)
{
    vCameraInfo = CameraInfo;
    vCameraID_1 = CameraInfo->CameraID;
    vWidth_1 = vWidth;
    vHeight_1 = vHeight;
    vImgType_1 = vImageType;
    vVBufferSize = vWidth * vHeight * fImgTypeSize(vImageType);
    vTrackingImageBufferSize = vWidth * vHeight;
    switch(vCameraInfo->BayerPattern)
    {
    case ASI_BAYER_BG:
        vBayerConversionCodePreview = cv::COLOR_BayerBG2RGB;
        vBayerConversionCode = cv::COLOR_BayerBG2BGR;
        vSerColorID = ser_Color_Bayer_BGGR;
        break;
    case ASI_BAYER_GB:
        vBayerConversionCodePreview = cv::COLOR_BayerGB2RGB;
        vBayerConversionCode = cv::COLOR_BayerGB2BGR;
        vSerColorID = ser_Color_Bayer_GBRG;
        break;
    case ASI_BAYER_GR:
        vBayerConversionCodePreview = cv::COLOR_BayerGR2RGB;
        vBayerConversionCode = cv::COLOR_BayerGR2BGR;
        vSerColorID = ser_Color_Bayer_GRBG;
        break;
    case ASI_BAYER_RG:
        vBayerConversionCodePreview = cv::COLOR_BayerRG2RGB;
        vBayerConversionCode = cv::COLOR_BayerRG2BGR;
        vSerColorID = ser_Color_Bayer_RGGB;
        break;
    }
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    if(vCameraInfo->IsColorCam &
       (vImgType_1!=ASI_IMG_Y8))
        vPreviewOutBufferSize = vWidth * vHeight * 3;
    else
        vPreviewOutBufferSize = vWidth * vHeight;
#else
    vPreviewOutBufferSize = vWidth * vHeight * 3;
#endif
    vVideoBuffer = new uchar[vVBufferSize];
    vPreviewOutBuffer = new uchar[vPreviewOutBufferSize];
    vPreviewInBuffer = new uchar[vWidth * vHeight];
    vTrackingImageBuffer = new uchar[vTrackingImageBufferSize];
    if((vImgType_1==ASI_IMG_Y8) |
            (vImgType_1==ASI_IMG_RGB24) |
            !vCameraInfo->IsColorCam)
        vOutputBufferSize = vVBufferSize;
    else
        vOutputBufferSize = vVBufferSize * 3;
    vOutputBuffer = new uchar[vOutputBufferSize];

    vVideoMode = true;
    fImageInit();
    vTimer1->start();
    vStreaming = true;
    ui->lbStatus->setText(zwoErrorProcessing(vError));
    vGuidingPorts = guidingPorts;
    setGuidingPortsEnabled(true);
    ui->tabWidget->setCurrentIndex(0);
}

void zwoStreaming::StartShooting(int Exposure,int vNum,QString FileName, IMG_FILE_FORMATS imageFormat)
{
    vShotsNumber = vNum;
    vExposure = Exposure;
    vImageFileFormat = imageFormat;
    vFileName = FileName;
    vTimer1->stop();
    vError = ASIStopVideoCapture(vCameraID_1);
    vError = ASIStartExposure(vCameraID_1,ASI_FALSE);
    vShooting = true;
    vFileIndex = 0;
    if((vImageFileFormat==IMG_FORMAT_FITS_MULTI) |
            (vImageFileFormat==IMG_FORMAT_SER))
        fCreateImageFile();
    vTimer1->start();
    vSingleShotEstimatedTime = vExposure/1000000;
    vSingleShotRemainingTime = vSingleShotEstimatedTime;
    vSingleShotElapsedTime = 0;
    vCurrentFilterEstimatedTime = vSingleShotRemainingTime * vShotsNumber;
    vCurrentFilterElapsedTime = 0;
    vCurrentFilterRemainingTime = vCurrentFilterEstimatedTime;
    QDateTime datetime = datetime.currentDateTime();
    datetime = datetime.addSecs(abs(vTotalRemainingTime));
    vEndTime = datetime.toString("hh:mm");
    vEndTime = "Estimated end time - "+vEndTime;
    vCountTimer->start();
}

void zwoStreaming::StopShooting()
{
    vShooting = false;
    if(vExposure>100000)
        vVideoMode = true;
    ASIStopExposure(vCameraID_1);
    vCountTimer->stop();
    if(vImageFileFormat==IMG_FORMAT_FITS_MULTI)
        vFits->SaveFile();
    if(vImageFileFormat==IMG_FORMAT_SER)
        vSerFile->SaveFile();
    emit ShootingConpleted();
}

void zwoStreaming::StopStreaming()
{
    if(vShooting)
    {
        if(!fYesNoMsgBox("Caution!!!","If you close video box, shooting will terminated! Do you realy want to close it?"))
            return;
        ASIStopExposure(vCameraID_1);
        vShooting = false;
    }
    vTimer1->stop();
    vError = ASIStopVideoCapture(vCameraID_1);
    if(vImage!=nullptr)
    {
        delete vImage;
        vImage = nullptr;
    }
    if(vVideoBuffer!=nullptr)
    {
        delete[] vVideoBuffer;
        vVideoBuffer = nullptr;
    }
    if(vPreviewOutBuffer!=nullptr)
    {
        delete[] vPreviewOutBuffer;
        vPreviewOutBuffer = nullptr;
    }
    if(vPreviewInBuffer!=nullptr)
    {
        delete[] vPreviewInBuffer;
        vPreviewInBuffer = nullptr;
    }
    if(vTrackingImageBuffer!=nullptr)
    {
        delete[] vTrackingImageBuffer;
        vTrackingImageBuffer = nullptr;
    }
    vStreaming = false;
}

// Exposure - microseconds
// VideoRecordingTime - seconds
// vNum - number of frames to be recorded if VideoRecordingTime is 0 otherwise ignored
void zwoStreaming::StartVideoRecording(int Exposure,int VideoRecordTime,int vNum,QString FileName, IMG_FILE_FORMATS imageFormat)
{
    vShotsNumber = vNum;
    vExposure = Exposure;
    vVideoRecordTime = VideoRecordTime;
    vImageFileFormat = imageFormat;
    vFileName = FileName;
    fCreateImageFile();
    vTimer1->stop();
    vVideoRecording = true;
    vFileIndex = 0;
    if(!vVideoMode)
    {
        vError = ASIStopExposure(vCameraID_1);
        vError = ASIStartVideoCapture(vCameraID_1);
    }
    vStartRcordTime = QDateTime().currentDateTime();
    vSingleShotEstimatedTime = vExposure/1000000;
    vSingleShotRemainingTime = vSingleShotEstimatedTime;
    vSingleShotElapsedTime = 0;
    if(VideoRecordTime == 0)
    {
        vTotalEstimatedTime = vSingleShotRemainingTime * vShotsNumber;
    }
    else
    {
        vTotalEstimatedTime = VideoRecordTime;
    }
    vTotalElapsedTime = 0;
    vTotalRemainingTime = vTotalEstimatedTime;
    QDateTime datetime = datetime.currentDateTime();
    datetime = datetime.addSecs(vTotalEstimatedTime);
    vEndTime = "Estimated end time - "+datetime.toString("hh:mm");
    vGetVideoThread = new GetVideoThread();
    vGetVideoThread->SetCameraID(vCameraID_1);
    vGetVideoThread->SetBuffer(vVideoBuffer);
    vGetVideoThread->SetBufferSize(vVBufferSize);
    vGetVideoThread->SetWaitingTime(vWait_ms);
    connect(vGetVideoThread,SIGNAL(finished()),this,SLOT(on_GetVideoDataFinished()));
    vGetVideoThread->start();
}

void zwoStreaming::GetProfile(zwoStreaming::stProfile *profile)
{
    profile->DisplayRefreshTime = ui->sbPictureRefreshTime->value();
    profile->DisplayRefreshTimeManual = ui->chbPictureRefreshTime->isChecked();

    profile->MinThreshold = ui->dsMinThreshold->value();
    profile->MaxThreshold = ui->dsMaxThreshold->value();
    profile->ThresholdStep = ui->dsThreshStep->value();
    profile->MinDistance = ui->dsMinDistance->value();
    profile->MinRepeatablility = ui->sbMinRepeatability->value();
    profile->ColorFlter = ui->sbBlobColor->value();
    profile->MinArea = ui->dsMinArea->value();
    profile->MaxArea = ui->dsMaxArea->value();
    profile->MinCircle = ui->dsMinCircularity->value();
    profile->MaxCircle = ui->dsMaxCircularity->value();
    profile->MinInertia = ui->dsMinInertia->value();
    profile->MaxInertia = ui->dsMaxInertia->value();
    profile->MinConvexity = ui->dsMinConvexity->value();
    profile->MaxConvexity = ui->dsMaxConvexity->value();
    profile->GaussFilter = ui->sbGaussK->value();
    profile->Tolerance = ui->dsToleranceBlobs->value();
    profile->KeypointsLimit = ui->sbKeyPointsLimit->value();
    profile->ColorFilterEnabled = ui->chbColorFilter->isChecked();
    profile->AreaFilterEnabled = ui->chbAreaFilter->isChecked();
    profile->CirculFilterEnabled = ui->chbCirculFilter->isChecked();
    profile->InertiaFilterEnabled = ui->chbInertiaFilter->isChecked();
    profile->ConvexityFilterEnabled = ui->chbConvexityFilter->isChecked();
    profile->RAsetpoint = ui->dsRASetpoint->value();
    profile->RAgain = ui->dsRAGain->value();
    profile->RAresetTime = ui->dsRAResetTime->value();
    profile->RAderivativeTime = ui->dsRADerivativeTime->value();
    profile->RAdeadBand = ui->dsRADeadBand->value();
    profile->RAoutputInversed = ui->chbInverseRApidOutput->isChecked();
    profile->DCsetpoint = ui->dsDCsetpoint->value();
    profile->DCgain = ui->dsDCGain->value();
    profile->DCresetTime = ui->dsDCResetTime->value();
    profile->DCderivativeTime = ui->dsDCDerivativeTime->value();
    profile->DCdeadBand = ui->dsDCDeadBand->value();
    profile->DCoutputInversed =ui->chbInverseDCpidOutput->isChecked();
}

bool zwoStreaming::isPaused()
{
    return  vPaused;
}

void zwoStreaming::StopVideoRecording()
{
    vVideoRecording = false;
}

void zwoStreaming::Terminate()
{
    on_pbStopStreaming_clicked();
}

void zwoStreaming::SaveImageToFile()
{
    if(vRightAligning &
       (vImgType_1==ASI_IMG_RAW16))
    {
        oImageProcessor.faa16_BitReduction(vVideoBuffer,vVBufferSize,16-vCameraInfo->BitDepth);
    }
    switch (vImageFileFormat)
    {
    case IMG_FORMAT_FITS_SINGLE:
        fCreateFITSFile();
        vFits->AddImage(vVideoBuffer);
        vFits->SaveFile();
        break;
    case IMG_FORMAT_FITS_MULTI:
        vFits->AddImage(vVideoBuffer);
        break;
    case IMG_FORMAT_SER:
        vSerFile->AddImage(vVideoBuffer);
        break;
    case IMG_FORMAT_TIFF:
        WriteImgFile(vFileName + "_" + vStr_1.setNum(vFileIndex) + ".tiff");
        break;
    case IMG_FORMAT_PNG:
        WriteImgFile(vFileName + "_" + vStr_1.setNum(vFileIndex) + ".png");
        break;
    }
}

void zwoStreaming::WriteImgFile(QString fileName)
{
    std::string str = fileName.toStdString();
    if(vCameraInfo->IsColorCam &
            ((vImgType_1==ASI_IMG_RAW8) |
             (vImgType_1==ASI_IMG_RAW16)))
    {
        cv::demosaicing(vMatIn,vMatOut,vBayerConversionCode);
    }
    cv::imwrite(str,vMatOut);
}

void zwoStreaming::SetObserver(QString Observer)
{
    vObserver = Observer.left(40);
}

void zwoStreaming::SetInstrument(QString Instrument)
{
    vInstrument = Instrument.left(40);
}

void zwoStreaming::SetPause(bool pause)
{
    ui->pbPauseRecording->setChecked(pause);
}

void zwoStreaming::SetTelescope(QString Telescope)
{
    vTelescope = Telescope.left(40);
}

void zwoStreaming::SetProfile(zwoStreaming::stProfile *profile)
{
    ui->sbPictureRefreshTime->setValue(profile->DisplayRefreshTime);
    ui->chbPictureRefreshTime->setChecked(profile->DisplayRefreshTimeManual);

    ui->dsMinThreshold->setValue(profile->MinThreshold);
    ui->dsMaxThreshold->setValue(profile->MaxThreshold);
    ui->dsThreshStep->setValue(profile->ThresholdStep);
    ui->dsMinDistance->setValue(profile->MinDistance);
    ui->sbMinRepeatability->setValue(profile->MinRepeatablility);
    ui->sbBlobColor->setValue(profile->ColorFlter);
    ui->dsMinArea->setValue(profile->MinArea);
    ui->dsMaxArea->setValue(profile->MaxArea);
    ui->dsMinCircularity->setValue(profile->MinCircle);
    ui->dsMaxCircularity->setValue(profile->MaxCircle);
    ui->dsMinInertia->setValue(profile->MinInertia);
    ui->dsMaxInertia->setValue(profile->MaxInertia);
    ui->dsMinConvexity->setValue(profile->MinConvexity);
    ui->dsMaxConvexity->setValue(profile->MaxConvexity);
    ui->sbGaussK->setValue(profile->GaussFilter);
    ui->dsToleranceBlobs->setValue(profile->Tolerance);
    ui->sbKeyPointsLimit->setValue(profile->KeypointsLimit);
    ui->chbColorFilter->setEnabled(profile->ColorFilterEnabled);
    ui->chbAreaFilter->setEnabled(profile->AreaFilterEnabled);
    ui->chbCirculFilter->setEnabled(profile->CirculFilterEnabled);
    ui->chbInertiaFilter->setEnabled(profile->InertiaFilterEnabled);
    ui->chbConvexityFilter->setEnabled(profile->ConvexityFilterEnabled);
    on_pbApplyTrackParamsChange_clicked();
    ui->dsRASetpoint->setValue(profile->RAsetpoint);
    ui->dsRAGain->setValue(profile->RAgain);
    ui->dsRAResetTime->setValue(profile->RAresetTime);
    ui->dsRADerivativeTime->setValue(profile->RAderivativeTime);
    ui->dsRADeadBand->setValue(profile->RAdeadBand);
    ui->chbInverseRApidOutput->setChecked(profile->RAoutputInversed);
    ui->dsDCsetpoint->setValue(profile->DCsetpoint);
    ui->dsDCGain->setValue(profile->DCgain);
    ui->dsDCResetTime->setValue(profile->DCresetTime);
    ui->dsDCDerivativeTime->setValue(profile->DCderivativeTime);
    ui->dsDCDeadBand->setValue(profile->DCdeadBand);
    ui->chbInverseDCpidOutput->setChecked(profile->DCoutputInversed);
}

void zwoStreaming::SetTotalEstimatedTime(int TimeSeconds)
{
    vTotalRemainingTime = TimeSeconds;
    vTotalEstimatedTime = TimeSeconds;
    vTotalElapsedTime = 0;
}

void zwoStreaming::SetFilterName(QString FilterName)
{
    vFilterName = "filter - " + FilterName;
}

//exposure - miliseconds;
void zwoStreaming::SetExposure(int vexposure)
{
    vExposure = vexposure;
    vWait_ms = vexposure/500;
    if(vWait_ms<100)
        vWait_ms = 100;
}

QString zwoStreaming::GetCountTimerString()
{
    QString string;
    if(vShooting)
    {
        string ="Total estimated time  - "+fSecondsToHMSString(vTotalEstimatedTime)+"\n";
        string +="Total elapsed time   - "+fSecondsToHMSString(vTotalElapsedTime)+"\n";
        string +="Total remaining time - "+fSecondsToHMSString(vTotalRemainingTime)+"\n";
        string +="Current filter estimated time - "+fSecondsToHMSString(vCurrentFilterEstimatedTime)+"\n";
        string +="Current filter elapsed time - "+fSecondsToHMSString(vCurrentFilterElapsedTime)+"\n";
        string +="Current filter remaining time - "+fSecondsToHMSString(vCurrentFilterRemainingTime)+"\n";
    }
    string +="Current frame elapsed time   - "+fSecondsToHMSString(vSingleShotElapsedTime)+"\n";
    string +="Current frame remaining time - "+fSecondsToHMSString(vSingleShotRemainingTime)+"\n";
    return string;
}

void zwoStreaming::GetTrackingShift(QImage *image)
{
    QPointF shift = vTracking->getShift(image);
    ui->lbTrackingShift->setText("shiftX: "+QString().setNum(shift.x(),'f',2)+"\n"+
                                 "shiftY: "+QString().setNum(shift.y(),'f',2));
}

void zwoStreaming::DrawTrackPixmap(QImage *image)
{
    vTrackPixmap.convertFromImage(*image);
    int w,h;
    w = image->width()/3;
    h = image->height()/3;
    ui->lbKeyPoints->setPixmap(vTrackPixmap.scaled(w,h));
}

void zwoStreaming::TrackingProc()
{
    if(!vTrackingEnabled)
        return;
    if(vCalibrationImageRequest)
    {
        vTracking->CalibrateDriverControl(vImage);
        vCalibrationImageRequest = false;
    }
    if(vTrackingRunning)
    {
        vTracking->setShiftThreshold(ui->dsShiftThreshold->value());
        vTrackingDriversMoving = true;
        vTracking->setCurrentImage(vImage);
    }
    else
        GetTrackingShift(vImage);
}

void zwoStreaming::setCalibrationData()
{
    vTracking->setDriverClibrationAuto(ui->rbDriversSettingsAuto->isChecked());
}

void zwoStreaming::setGuidingPortsEnabled(bool enabled)
{
    if(enabled)
    {
        if((vGuidingPorts&Tracking::Guiding_cameraST4port)!=0)
        {
            ui->rbZWOcameraGuidingPort->setEnabled(true);
        }
        else
        {
            ui->rbZWOcameraGuidingPort->setChecked(false);
            ui->rbZWOcameraGuidingPort->setEnabled(false);
        }
        if((vGuidingPorts&Tracking::Guiding_USB2ST4adapter)!=0)
            ui->rbZWO_USBguidingPort->setEnabled(true);
        else
        {
            ui->rbZWO_USBguidingPort->setEnabled(false);
            ui->rbZWO_USBguidingPort->setChecked(false);
        }
        ui->rbI2CguidingPort->setEnabled(true);
        ui->rbI2CanalogOutput->setEnabled(true);
    }
    else
    {
        ui->rbZWOcameraGuidingPort->setEnabled(false);
        ui->rbZWO_USBguidingPort->setEnabled(false);
        ui->rbI2CguidingPort->setEnabled(false);
        ui->rbI2CanalogOutput->setEnabled(false);
    }

}

void zwoStreaming::fVideoRender()
{
    int i;
    int i1 = 0;
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    if(vCameraInfo->IsColorCam &
       (vImgType_1!=ASI_IMG_Y8))
    {
        if(vImgType_1==ASI_IMG_RGB24)
        {
            uchar r,b;
            for(i=0;i<vVBufferSize;i+=3)
            {
                r = vVideoBuffer[i+2];
                b = vVideoBuffer[i];
                vPreviewOutBuffer[i] = r;
                vPreviewOutBuffer[i+2] = b;
                vPreviewOutBuffer[i+1] = vVideoBuffer[i+1];            }
        }
        if(vImgType_1==ASI_IMG_RAW8)
        {
            cv::demosaicing(vMatInPreview,vMatOutPreview,vBayerConversionCodePreview);
        }
        if(vImgType_1==ASI_IMG_RAW16)
        {
            int i1=0;
            for(int i=0;i<vVBufferSize;i+=2)
            {
                vPreviewInBuffer[i1++] = vVideoBuffer[i+1];
            }
            cv::demosaicing(vMatInPreview,vMatOutPreview,vBayerConversionCodePreview);
        }
    }
    else
    {
        if(vImgType_1==ASI_IMG_RAW16)
        {
            for(i=1;i<vVBufferSize;i+=2)
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
        }
    }
#else
    if(vImgType_1!=ASI_IMG_RGB24)
    {
        if(vImgType_1==ASI_IMG_RAW16)
        {
            for(i=1;i<vVBufferSize;i+=2)
            {
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
            }
        }
        else
        {
            for(i=0;i<vVBufferSize;i++)
            {
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
                vPreviewOutBuffer[i1++] = vVideoBuffer[i];
            }
        }
    }
#endif
    ui->lbStatus->setText(vStr_1);
    if(vImage==nullptr)
        return;
    vPixmap.convertFromImage(*vImage);
    if(vScale<1)
    {
        int w = int(vWidth_1 * vScale);
        int h = int(vHeight_1 * vScale);
        vPixmap = vPixmap.scaled(w,h,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    }
    ui->lbPixmap->setPixmap(vPixmap);
    if(vScaleChange > 0)
    {
        this->adjustSize();
        vScaleChange--;
    }
}

void zwoStreaming::fImageInit()
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    if(vCameraInfo->IsColorCam &
       (vImgType_1!=ASI_IMG_Y8))
    {
        vImgFormat = QImage::Format_RGB888;
        if(vImgType_1==ASI_IMG_RAW8)
        {
            vMatInPreview = cv::Mat(vHeight_1,vWidth_1,CV_8U,vVideoBuffer);
            vMatIn = cv::Mat(vHeight_1,vWidth_1,CV_8U,vVideoBuffer);
            vMatOut = cv::Mat(vHeight_1,vWidth_1,CV_8UC3,vOutputBuffer);
        }
        if(vImgType_1==ASI_IMG_RAW16)
        {
            vMatInPreview = cv::Mat(vHeight_1,vWidth_1,CV_8U,vPreviewInBuffer);
            vMatIn = cv::Mat(vHeight_1,vWidth_1,CV_16U,vVideoBuffer);
            vMatOut = cv::Mat(vHeight_1,vWidth_1,CV_16UC3,vOutputBuffer);
        }
        if(vImgType_1==ASI_IMG_RGB24)
        {
            vMatOut = cv::Mat(vHeight_1,vWidth_1,CV_8UC3,vVideoBuffer);
        }
        vMatOutPreview = cv::Mat(vHeight_1,vWidth_1,CV_8UC3,vPreviewOutBuffer);
    }
    else
    {
        vImgFormat = QImage::Format_Grayscale8;
        if(vImgType_1==ASI_IMG_RAW16)
            vMatOut = cv::Mat(vHeight_1,vWidth_1,CV_16U,vVideoBuffer);
        else
            vMatOut = cv::Mat(vHeight_1,vWidth_1,CV_8U,vVideoBuffer);
    }
    if((vImgType_1==ASI_IMG_Y8) |
        ((vImgType_1==ASI_IMG_RAW8) &
        !vCameraInfo->IsColorCam))
        vImage = new QImage(vVideoBuffer,vWidth_1,vHeight_1,vImgFormat);
    else
    {
        vImage = new QImage(vPreviewOutBuffer,vWidth_1,vHeight_1,vImgFormat);
    }
#else
    vImgFormat = QImage::Format_RGB888;
    if(vImgType_1==ASI_IMG_RGB24)
        vImage = new QImage(vVideoBuffer,vWidth_1,vHeight_1,vImgFormat);
    else
        vImage = new QImage(vPreviewOutBuffer,vWidth_1,vHeight_1,vImgFormat);
#endif
}

void zwoStreaming::fCreateImageFile()
{
    switch (vImageFileFormat)
    {
        case IMG_FORMAT_FITS_MULTI: fCreateFITSFile();
            break;
        case IMG_FORMAT_SER: fCreateSerFile();
            break;
    }
}

void zwoStreaming::fCreateFITSFile()
{
    vStr = vFileName + "_" + vStr_1.setNum(vFileIndex) + ".fits";
    vFits = new FitsIOClass(vStr,"Image file",vWidth_1,vHeight_1,vImgType_1);
}

void zwoStreaming::fCreateSerFile()
{
    vStr = vFileName + ".ser";
    vSerFile = new SerIOClass(vStr,"zwocapture",vWidth_1,vHeight_1,vImgType_1);
    vSerFile->SetParameter(ser_Observer,vObserver);
    vSerFile->SetParameter(ser_Instrument,vInstrument);
    vSerFile->SetParameter(ser_Telescope,vTelescope);
    if((vCameraInfo->IsColorCam) &
        !(vImgType_1==ASI_IMG_Y8))
    {
        if(vImgType_1==ASI_IMG_RGB24)
        {
            vSerFile->SetParameter(ser_ColorID,ser_Color_BGR);
        }
        else
            vSerFile->SetParameter(ser_ColorID,vSerColorID);
    }
    else
    {
        vSerFile->SetParameter(ser_ColorID,ser_Color_MONO);
    }
//    if(vImgType_1==ASI_IMG_RAW16)
//        vSerFile->SetParameter(ser_PixelDepthPerPlane,16);
//    else
//        vSerFile->SetParameter(ser_PixelDepthPerPlane,8);
}

void zwoStreaming::on_Timer1()
{
    if(vShooting)
    {
        if(vFileIndex < vShotsNumber)
        {
            vError = ASIGetExpStatus(vCameraID_1,&vExpStatus);
            switch (vExpStatus)
            {
                case ASI_EXP_FAILED:
                    {
                        vError = ASIStopExposure(vCameraID_1);
                        vError = ASIStartExposure(vCameraID_1,ASI_FALSE);
                        vStr_1 = "exp failed";
                    }
                    break;
                case ASI_EXP_IDLE:
                    {
                        vStr_1 = "exp idle";
                        if(!vTrackingDriversMoving)
                        {
                            vError = ASIStartExposure(vCameraID_1,ASI_FALSE);
                            vSingleShotRemainingTime = vSingleShotEstimatedTime;
                            vSingleShotElapsedTime = 0;
                        }
                    }
                    break;
                case ASI_EXP_SUCCESS:
                    {
                        vStr_1 = "exp success";
                        vError = ASIGetDataAfterExp(vCameraID_1,vVideoBuffer,vVBufferSize);
                        if(!vPaused)
                        {
                            SaveImageToFile();
                            vFileIndex++;
                        }
                        if(!vRefreshTimer->isActive())
                        {
                            fVideoRender();
                            vRefreshTimer->start();
                            TrackingProc();
                        }
                    }
                    break;
                case ASI_EXP_WORKING: vStr_1 = "exp working";
                    break;
            }
            vStr_1 ="Shooting in progress: "+ vFilterName + "; fraime - " + vStr.setNum(vFileIndex)+"; status:"+vStr_1+"\n"+
                    GetCountTimerString()+"\n"+vEndTime;
            if(vError!=ASI_SUCCESS)
                vStr_1 = zwoErrorProcessing(vError);
        }
        else
        {
            StopShooting();
        }

    }
    else
    {
        if(vExposure>100000 or
           ui->chbCaptureMode->isChecked())
        {
            if(vVideoMode)
            {
                ASIStopVideoCapture(vCameraID_1);
                vVideoMode = false;
                vCountTimer->start();
            }
            vError = ASIGetExpStatus(vCameraID_1,&vExpStatus);
            switch (vExpStatus)
            {
                case ASI_EXP_FAILED:
                    {
                        vError = ASIStopExposure(vCameraID_1);
                        vError = ASIStartExposure(vCameraID_1,ASI_FALSE);
                        vStr_1 = "exp failed";
                    }
                    break;
                case ASI_EXP_IDLE:
                    {
                        vStr_1 = "exp idle";
                        if(!vTrackingDriversMoving)
                        {
                            vError = ASIStartExposure(vCameraID_1,ASI_FALSE);
                            vSingleShotEstimatedTime = vExposure/1000000;
                            vSingleShotRemainingTime = vSingleShotEstimatedTime;
                            vSingleShotElapsedTime = 0;
                        }
                    }
                    break;
                case ASI_EXP_SUCCESS:
                    {
                        vStr_1 = "exp success";
                        vError = ASIGetDataAfterExp(vCameraID_1,vVideoBuffer,vVBufferSize);
                        if(!vRefreshTimer->isActive())
                        {
                            fVideoRender();
                            vRefreshTimer->start();
                            TrackingProc();
                         }
                    }
                    break;
                case ASI_EXP_WORKING: vStr_1 = "exp working";
                    break;
            }
            vStr_1 = "Snap Mode: "+vStr_1+"\n"+GetCountTimerString();
            if(vError!=ASI_SUCCESS)
                vStr_1 = zwoErrorProcessing(vError);
        }
        else
        {
            if(!vVideoMode)
            {
                ASIStopExposure(vCameraID_1);
                ASIStartVideoCapture(vCameraID_1);
                vVideoMode = true;
                vCountTimer->stop();
            }
            vError = ASIGetDroppedFrames(vCameraID_1,&vDroptFrames);
            vError = ASIGetVideoData(vCameraID_1,vVideoBuffer,vVBufferSize,vWait_ms);
            vStr_1 = "Video Mode: Dropt Frames- "+vStr.setNum(vDroptFrames)+"; errors:"+zwoErrorProcessing(vError);
            if(vError!=ASI_SUCCESS)
            {
                if(vTimeOutStatus<10)
                    vTimeOutStatus++;
                else
                {
                    vTimeOutStatus = 0;
                    vError = ASIStopVideoCapture(vCameraID_1);
                    vError = ASIStartVideoCapture(vCameraID_1);
                    return;
                }
            }
            else
                vTimeOutStatus = 0;
            if(!vRefreshTimer->isActive())
            {
                fVideoRender();
                vRefreshTimer->start();
                TrackingProc();
            }
        }
    }
    ui->lbStatus->setText(vStr_1);
}

void zwoStreaming::on_vCountTimer()
{
    if(!vPaused)
    {
        vTotalElapsedTime++;
        vTotalRemainingTime--;
        vCurrentFilterElapsedTime++;
        vCurrentFilterRemainingTime--;
    }
    vSingleShotElapsedTime++;
    vSingleShotRemainingTime--;
}


void zwoStreaming::on_chbInverseRAdirection_toggled(bool checked)
{
    vTracking->setInverseDirection(checked,ui->chbInverseDCdirection->isChecked());
}

void zwoStreaming::on_chbInverseDCdirection_toggled(bool checked)
{
    vTracking->setInverseDirection(ui->chbInverseRAdirection->isChecked(),checked);
}

void zwoStreaming::on_chbPictureRefreshTime_toggled(bool checked)
{
    if(checked)
    {
        ui->sbPictureRefreshTime->setEnabled(true);
    }
    else
    {
        ui->sbPictureRefreshTime->setEnabled(false);
        ui->sbPictureRefreshTime->setValue(20);
        vRefreshTimer->setInterval(ui->sbPictureRefreshTime->value());
    }
}

void zwoStreaming::on_pbStopStreaming_clicked()
{
    StopStreaming();
    this->close();
}

void zwoStreaming::on_pbSetBaseImage_clicked()
{
    vTracking->setBaseImage(vImage);
    QImage *image;
    image = vTracking->getBaseImage();
    DrawTrackPixmap(image);
    delete image;
}

void zwoStreaming::on_pbApplyTrackParamsChange_clicked()
{
    vBlobParams.blobColor = uchar(ui->sbBlobColor->value());
    vBlobParams.minRepeatability = ulong(ui->sbMinRepeatability->value());
    vBlobParams.maxArea = float(ui->dsMaxArea->value());
    vBlobParams.minArea = float(ui->dsMinArea->value());
    vBlobParams.maxCircularity = float(ui->dsMaxCircularity->value());
    vBlobParams.minCircularity = float(ui->dsMinCircularity->value());
    vBlobParams.maxConvexity = float(ui->dsMaxConvexity->value());
    vBlobParams.minConvexity = float(ui->dsMinConvexity->value());
    vBlobParams.maxInertiaRatio = float(ui->dsMaxInertia->value());
    vBlobParams.minInertiaRatio = float(ui->dsMinInertia->value());
    vBlobParams.maxThreshold = float(ui->dsMaxThreshold->value());
    vBlobParams.minThreshold = float(ui->dsMinThreshold->value());
    vBlobParams.minDistBetweenBlobs = float(ui->dsMinDistance->value());
    vBlobParams.thresholdStep = float(ui->dsThreshStep->value());
    vBlobParams.filterByArea = ui->chbAreaFilter->isChecked();
    vBlobParams.filterByCircularity = ui->chbCirculFilter->isChecked();
    vBlobParams.filterByColor = ui->chbColorFilter->isChecked();
    vBlobParams.filterByConvexity = ui->chbConvexityFilter->isChecked();
    vBlobParams.filterByInertia = ui->chbInertiaFilter->isChecked();
    ui->dsMaxThreshold->setValue(double(vBlobParams.maxThreshold));
    ui->dsMinThreshold->setValue(double(vBlobParams.minThreshold));
    ui->dsMinDistance->setValue(double(vBlobParams.minDistBetweenBlobs));
    ui->dsThreshStep->setValue(double(vBlobParams.thresholdStep));
    ui->chbAreaFilter->setChecked(vBlobParams.filterByArea);
    ui->chbCirculFilter->setChecked(vBlobParams.filterByCircularity);
    ui->chbColorFilter->setChecked(vBlobParams.filterByColor);
    ui->chbConvexityFilter->setChecked(vBlobParams.filterByConvexity);
    ui->chbInertiaFilter->setChecked(vBlobParams.filterByInertia);
    vTracking->setParams(vBlobParams,ulong(ui->sbKeyPointsLimit->value()),
                        ulong(ui->dsToleranceBlobs->value()));
    vTracking->setGaussKsize(ui->sbGaussK->value());
}

void zwoStreaming::on_pbTestBlobDetectParams_clicked()
{
    QImage *image;
    int KeyPointsFound;
    image = vTracking->testParams(&KeyPointsFound);
    DrawTrackPixmap(image);
    QMessageBox msgbox;
    msgbox.setText(QString().setNum(KeyPointsFound)+" found");
    msgbox.exec();
    delete image;
}

void zwoStreaming::on_pbCalibrateTracking_clicked()
{
    vTracking->setCalibrationMovingTime(ui->sbCalibrationMovingTime->value());
    vTracking->setShiftThreshold(ui->dsShiftThreshold->value());
    vTracking->setShiftEstimationTime(ui->sbShiftEstimationTime->value());
    setCalibrationData();
    vTracking->CalibrateDriverControl(vImage);
}

void zwoStreaming::on_pbGetBaseImage_clicked()
{
    QImage *image;
    image = vTracking->getBaseImage();
    DrawTrackPixmap(image);
    delete image;
}

void zwoStreaming::on_pbGetBaseKeypointsImage_clicked()
{
    QImage *image;
    image = vTracking->getKeyPointsImage();
    DrawTrackPixmap(image);
    delete image;
}

void zwoStreaming::on_pbGetCurrentKeypointsImage_clicked()
{
    QImage *image;
    image = vTracking->getKeyPointsImage(false);
    DrawTrackPixmap(image);
    delete image;
}

void zwoStreaming::on_pbForward_clicked()
{
    vTracking->MoveEast(ui->sbMovingTime->value());
}

void zwoStreaming::on_pbBackward_clicked()
{
    vTracking->MoveWest(ui->sbMovingTime->value());
}

void zwoStreaming::on_pbUp_clicked()
{
    vTracking->MoveNorth(ui->sbMovingTime->value());
}

void zwoStreaming::on_pbDown_clicked()
{
    vTracking->MoveSouth(ui->sbMovingTime->value());
}

void zwoStreaming::on_pbSetCalParameters_clicked()
{
    vTracking->setCalRAvelocity(ui->dsRAdriveVelocity->value());
    vTracking->setCalDCvelocity(ui->dsDCdriveVelocity->value());
    vTracking->setCalTrAngle(ui->dsTrasformationAngle->value());
    vTracking->setDriverClibrationAuto(ui->rbDriversSettingsAuto->isChecked());
}

void zwoStreaming::on_pbStopCalibration_clicked()
{
    vTracking->StopCalibration();
}

void zwoStreaming::on_pbStopDrive_clicked()
{
    vTracking->StopMoving();
}

void zwoStreaming::on_pbI2Cconnect_clicked()
{
    vTracking->SetI2C(ui->leI2cFileName->text(),uchar(ui->sbI2Caddress->value()));
}

void zwoStreaming::on_sbPictureRefreshTime_valueChanged(int arg1)
{
    vRefreshTimer->setInterval(arg1);
}

void zwoStreaming::on_sbGaussK_valueChanged(int arg1)
{
    if(arg1%2==0)
        ui->sbGaussK->setValue(++arg1);
}

void zwoStreaming::on_GetVideoDataFinished()
{
    ASI_ERROR_CODE Error = vGetVideoThread->GetErrorCode();
    if(Error == ASI_SUCCESS)
    {
        if(!vRefreshTimer->isActive())
        {
            vError = ASIGetDroppedFrames(vCameraID_1,&vDroptFrames);
            vStr_1 = "Video recoring:\nDropt Frames: "+QString().setNum(vDroptFrames)+
                    "\nerrors: "+zwoErrorProcessing(vError)+
                    "\nframes recorded: "+QString().setNum(vFileIndex);
            fVideoRender();
            vRefreshTimer->start();
        }
        if(!vPaused)
        {
            SaveImageToFile();
            vFileIndex++;
        }
    }
    QDateTime CurrentDateTime = QDateTime().currentDateTime();
    vTotalElapsedTime = int(vStartRcordTime.secsTo(CurrentDateTime));
    if(vVideoRecordTime != 0)
    {
        if((vVideoRecordTime < vTotalElapsedTime) |
                !vVideoRecording)
        {
            vVideoRecording = false;
            vVideoMode =false;
            vError = ASIStopVideoCapture(vCameraID_1);
            vTimer1->start();
            vGetVideoThread->deleteLater();
            return;
        }
    }
    else
    {
        if((vFileIndex > vShotsNumber) |
                !vVideoRecording)
        {
            vVideoRecording = false;
            vVideoMode =false;
            vError = ASIStopVideoCapture(vCameraID_1);
            vTimer1->start();
            vGetVideoThread->deleteLater();
            return;
        }
    }
    vGetVideoThread->start();
}

void zwoStreaming::on_TrackingStatusChanged(QString str)
{
    ui->lbTrackingStatus->setText(str);
}

void zwoStreaming::on_Tracking_calibrationImgRequest()
{
    vCalibrationTimer->start(vExposure/400);
}

void zwoStreaming::on_vCalibrationTimer_Timer()
{
    vCalibrationImageRequest = true;
}

void zwoStreaming::on_Tracking_CalibrationCompleted(Tracking::CalibrationData data)
{
    QString s = "Moving time" +QString().setNum(data.MovingTime_ms)+"\n";
    s += "RA shift: dx="+QString().setNum(data.RAshift.x(),'f',1)+", dy="+QString().setNum(data.RAshift.y(),'f',1)+"\n";
    s += "DC shift: dx="+QString().setNum(data.DCshift.x(),'f',1)+", dy="+QString().setNum(data.DCshift.y(),'f',1)+"\n";
    s += "DC gap: x="+QString().setNum(data.DCgap.x(),'f',1)+", y="+QString().setNum(data.DCgap.y(),'f',1)+"\n";
    s += "RA velocity: "+ QString().setNum(data.RAvelocity,'f',2)+"\n";
    s += "DC velocity: "+QString().setNum(data.DCvelocity,'f',2)+"\n";
    s += "transformation angle: "+QString().setNum(data.TrAngle.x(),'f',2)+";"+QString().setNum(data.TrAngle.y(),'f',2)+"\n";
    if(data.PolarDirection>0)
        s += "direction clockwise\n";
    else
        s += "direction counter-clockwise\n";
    s += "estimated shift dx: "+QString().setNum(data.EstimatedShift.x(),'f',2)+"\n";
    s += "estimated shift dy: "+QString().setNum(data.EstimatedShift.y(),'f',2)+"\n";
    s += "estimated RA velocity: "+QString().setNum(data.RAestimatedVelocity,'f',5)+"\n";
    s += "estimated DC velocity: "+QString().setNum(data.DCestimatedVelocity,'f',5);
    ui->lbCalibrationData->setText(s);
}

void zwoStreaming::on_TrackingShiftCalculated(QPointF shift)
{
    ui->lbTrackingShift->setText("shiftX: "+QString().setNum(shift.x(),'f',2)+"\n"+
                                 "shiftY: "+QString().setNum(shift.y(),'f',2));
}

void zwoStreaming::on_TrackingDataCalculated(Tracking::TrackingData data)
{
    QString s = "RA move ms:"+QString().setNum((data.RAtrackInterval))+"\n";
    s += "DC move ms:"+QString().setNum(data.DCtrackInterval);
    ui->lbTrackingData->setText(s);
}

void zwoStreaming::on_TrackingDriversstoped()
{
    vTrackingDriversMoving = false;
}

void zwoStreaming::on_dsDCsetpoint_valueChanged(double arg1)
{
    vTracking->DCpidcontrol.setSetpoint(arg1);
}

void zwoStreaming::on_dsDCGain_valueChanged(double arg1)
{
    vTracking->DCpidcontrol.setGain(arg1);
}

void zwoStreaming::on_dsDCResetTime_valueChanged(double arg1)
{
    vTracking->DCpidcontrol.setResetTime(arg1);
}

void zwoStreaming::on_dsDCDerivativeTime_valueChanged(double arg1)
{
    vTracking->DCpidcontrol.setDerivativeTime(arg1);
}

void zwoStreaming::on_chbInverseDCpidOutput_toggled(bool checked)
{
    vTracking->DCpidcontrol.setOutputInversed(checked);
}

void zwoStreaming::on_dsRASetpoint_valueChanged(double arg1)
{
    vTracking->RApidcontrol.setSetpoint(arg1);
}

void zwoStreaming::on_dsRAGain_valueChanged(double arg1)
{
    vTracking->RApidcontrol.setGain(arg1);
}

void zwoStreaming::on_dsRAResetTime_valueChanged(double arg1)
{
    vTracking->RApidcontrol.setResetTime(arg1);
}

void zwoStreaming::on_dsRADerivativeTime_valueChanged(double arg1)
{
    vTracking->RApidcontrol.setDerivativeTime(arg1);
}

void zwoStreaming::on_chbInverseRApidOutput_toggled(bool checked)
{
    vTracking->RApidcontrol.setOutputInversed(checked);
}

void zwoStreaming::on_dsRADeadBand_valueChanged(double arg1)
{
    vTracking->RApidcontrol.setDeadBand(arg1);
}

void zwoStreaming::on_dsDCDeadBand_valueChanged(double arg1)
{
    vTracking->DCpidcontrol.setDeadBand(arg1);
}

void zwoStreaming::on_pbPauseRecording_toggled(bool checked)
{
    vPaused = checked;
    if(checked)
        ui->pbPauseRecording->setText("Resume rec");
    else
        ui->pbPauseRecording->setText("Pause rec");
}

void zwoStreaming::on_comboBox_currentIndexChanged(const QString &arg1)
{
    double scalePersent = arg1.toDouble();
    vScale = scalePersent/100;
    vScaleChange = 2;
    fVideoRender();
}

void zwoStreaming::on_pbStartTracking_toggled(bool checked)
{
    if(checked)
    {
        vTracking->RApidcontrol.setSetpoint(ui->dsRASetpoint->value());
        vTracking->RApidcontrol.setGain(ui->dsRAGain->value());
        vTracking->RApidcontrol.setResetTime(ui->dsRAResetTime->value());
        vTracking->RApidcontrol.setDerivativeTime(ui->dsRADerivativeTime->value());
        vTracking->RApidcontrol.setOutputInversed(ui->chbInverseRApidOutput->isChecked());
        vTracking->RApidcontrol.setDeadBand(ui->dsRADeadBand->value());

        vTracking->DCpidcontrol.setSetpoint(ui->dsDCsetpoint->value());
        vTracking->DCpidcontrol.setGain(ui->dsDCGain->value());
        vTracking->DCpidcontrol.setResetTime(ui->dsDCResetTime->value());
        vTracking->DCpidcontrol.setDerivativeTime(ui->dsDCDerivativeTime->value());
        vTracking->DCpidcontrol.setOutputInversed(ui->chbInverseDCpidOutput->isChecked());
        vTracking->DCpidcontrol.setDeadBand(ui->dsDCDeadBand->value());

        vTracking->StartTracking(ui->rbOngoingCorrection->isChecked());
        vTrackingRunning = true;
        ui->pbStartTracking->setText("Stop tracking");
        setGuidingPortsEnabled(false);
    }
    else
    {
        vTrackingRunning = false;
        vTracking->StopTracking();
        ui->pbStartTracking->setText("Start tracking");
        setGuidingPortsEnabled(true);
    }
}

void zwoStreaming::on_rbZWOcameraGuidingPort_clicked(bool checked)
{
    if(checked)
    {
        vTracking->setGuidingOutput(vCameraID_1,Tracking::Guiding_cameraST4port);
    }
}

void zwoStreaming::on_rbZWO_USBguidingPort_clicked(bool checked)
{
    if(checked)
        vTracking->setGuidingOutput(1,Tracking::Guiding_USB2ST4adapter);
}

void zwoStreaming::on_rbI2CguidingPort_clicked(bool checked)
{
    if(checked)
        vTracking->setGuidingOutput(0,Tracking::Guiding_customI2C_GPIO);
}

void zwoStreaming::on_rbI2CanalogOutput_clicked(bool checked)
{
    if(checked)
        vTracking->setGuidingOutput(0,Tracking::Guiding_customI2C_GPIOanalog);
}

void zwoStreaming::on_pbTargetToCurrentCoordinates_clicked()
{
    ui->leCurrentLatitude->setText(ui->leTargetLatitude->text());
    ui->leCurrentLongitude->setText(ui->leTargetLongitude->text());
}

void zwoStreaming::on_pbStartMovingToTarget_clicked()
{

}

void zwoStreaming::on_chbTrackingControlEnabled_toggled(bool checked)
{
    if(checked)
    {
        ui->tabWidget->setTabEnabled(1,true);
        ui->tabWidget->setTabEnabled(3,true);
        ui->pbCalibrateTracking->setEnabled(true);
        ui->pbStopCalibration->setEnabled(true);
        ui->pbStartTracking->setEnabled(true);
        vTrackingEnabled = true;
    }
    else
    {
        ui->tabWidget->setTabEnabled(1,false);
        ui->tabWidget->setTabEnabled(2,false);
        ui->pbCalibrateTracking->setEnabled(false);
        ui->pbStopCalibration->setEnabled(false);
        ui->pbStartTracking->setEnabled(false);
        vTrackingEnabled = false;
    }
}
