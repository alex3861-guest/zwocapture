/*
This module is a part of zwocapture programm.
Zwocanture softwore is intended to manipulate with ZWO ASI astronomical
cameras through the libASICamera2.so which is avalable from
https://astronomy-imaging-camera.com/
The foremetioned library is not included into this software and shall
be instaled separately.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ZWOSTREAMING_H
#define ZWOSTREAMING_H

#include <ASICamera2.h>
#include <QObject>
#include <QDialog>
#include <QPixmap>
#include <QImage>
#include <QTimer>
#include <QThread>
#include <qdatetime.h>
#include <QJsonObject>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <OpenImageIO/imageio.h>

#include "zwoFunctions.h"
#include "fitsioclass.h"
#include "faaimageprocessor.h"
#include "serioclass.h"
#include "tracking.h"
#include "imagereg.h"

namespace Ui {
class zwoStreaming;
}

class GetVideoThread : public QThread
{
    Q_OBJECT
public:
    void run() Q_DECL_OVERRIDE
    {
        vError = ASIGetVideoData(vCameraID,vBuffer,vBufferSize,vWaitingTime);
    }
    void    SetBuffer(uchar *Buffer)
    {
        vBuffer = Buffer;
    }
    void    SetBufferSize(long BufferSize)
    {
        vBufferSize = BufferSize;
    }
    void    SetCameraID(int CameraID)
    {
        vCameraID = CameraID;
    }
    void    SetWaitingTime(int WaitingTime)
    {
        vWaitingTime = WaitingTime;
    }
    ASI_ERROR_CODE GetErrorCode()
    {
        return vError;
    }
signals:
    void DataIsReady();
private:
    uchar   *vBuffer;
    long    vBufferSize;
    int     vCameraID;
    int     vWaitingTime;
    ASI_ERROR_CODE  vError;
};

class zwoStreaming : public QDialog
{
    Q_OBJECT

public:
    typedef enum IMG_FILE_FORMATS
    {
        IMG_FORMAT_FITS_SINGLE=0,
        IMG_FORMAT_FITS_MULTI,
        IMG_FORMAT_SER,
        IMG_FORMAT_TIFF,
        IMG_FORMAT_PNG
    }IMG_FILE_FORMATS;
    struct  stProfile
    {
        //registration
        double  ThresholdStep;
        double  MaxThreshold;
        double  MinThreshold;
        int     MinRepeatablility;
        double  MinDistance;
        int     ColorFlter;
        double  MinArea;
        double  MaxArea;
        double  MinCircle;
        double  MaxCircle;
        double  MinInertia;
        double  MaxInertia;
        double  MinConvexity;
        double  MaxConvexity;
        int     GaussFilter;
        double  Tolerance;
        int     KeypointsLimit;
        bool    ColorFilterEnabled;
        bool    AreaFilterEnabled;
        bool    CirculFilterEnabled;
        bool    InertiaFilterEnabled;
        bool    ConvexityFilterEnabled;
        //pid control
        double  RAsetpoint;
        double  RAgain;
        double  RAresetTime;
        double  RAderivativeTime;
        double  RAdeadBand;
        bool    RAoutputInversed;
        double  DCsetpoint;
        double  DCgain;
        double  DCresetTime;
        double  DCderivativeTime;
        double  DCdeadBand;
        bool    DCoutputInversed;
        //display
        bool    DisplayRefreshTimeManual;
        int     DisplayRefreshTime;
    };

    explicit zwoStreaming(QWidget *parent = 0);
    ~zwoStreaming();    
    void StartStriming(ASI_CAMERA_INFO* CameraInfo, int vWidth, int vHeight, ASI_IMG_TYPE vImageType, int guidingPorts);
    void StartShooting(int Exposure, int vNum, QString FileName, IMG_FILE_FORMATS imageFormat);
    void StartVideoRecording(int Exposure,int VideoRecordTime,int vNum,QString FileName, IMG_FILE_FORMATS imageFormat);
    bool                vStreaming;
    bool                vShooting;
    bool                vRightAligning;
    bool                vVideoRecording;
//    int                 vRightShift;
    int                 vOffset;
    ASI_CAMERA_INFO*    vCameraInfo;
    void                GetProfile(stProfile *profile);
    bool                isPaused();
    void                Terminate();
    void                SetExposure(int vexposure);
    void                StopStreaming();
    void                StopShooting();
    void                StopVideoRecording();
    void                SetObserver(QString Observer);
    void                SetInstrument(QString Instrument);
    void                SetPause(bool pause);
    void                SetTelescope(QString Telescope);
    void                SetProfile(stProfile *profile);
    void                SetTotalEstimatedTime(int TimeSeconds);
    void                SetFilterName(QString FilterName);

signals:
    void    ShootingConpleted();

private slots:
    void on_pbStopStreaming_clicked();
    void on_pbSetBaseImage_clicked();
    void on_pbApplyTrackParamsChange_clicked();
    void on_pbTestBlobDetectParams_clicked();
    void on_pbCalibrateTracking_clicked();
    void on_pbGetBaseImage_clicked();
    void on_pbGetBaseKeypointsImage_clicked();
    void on_pbGetCurrentKeypointsImage_clicked();
    void on_pbForward_clicked();
    void on_pbBackward_clicked();
    void on_pbUp_clicked();
    void on_pbDown_clicked();
    void on_pbStopDrive_clicked();
    void on_pbI2Cconnect_clicked();
    void on_pbStopCalibration_clicked();
    void on_pbSetCalParameters_clicked();

    void on_chbPictureRefreshTime_toggled(bool checked);
    void on_chbInverseRAdirection_toggled(bool checked);
    void on_chbInverseDCdirection_toggled(bool checked);

    void on_sbPictureRefreshTime_valueChanged(int arg1);
    void on_sbGaussK_valueChanged(int arg1);

    void on_Timer1();
    void on_vCountTimer();

    void on_GetVideoDataFinished();
    void on_TrackingStatusChanged(QString str);
    void on_Tracking_calibrationImgRequest();
    void on_vCalibrationTimer_Timer();
    void on_Tracking_CalibrationCompleted(Tracking::CalibrationData data);
    void on_TrackingShiftCalculated(QPointF shift);
    void on_TrackingDataCalculated(Tracking::TrackingData data);
    void on_TrackingDriversstoped();


    void on_dsDCsetpoint_valueChanged(double arg1);
    void on_dsDCGain_valueChanged(double arg1);
    void on_dsDCResetTime_valueChanged(double arg1);
    void on_dsDCDerivativeTime_valueChanged(double arg1);
    void on_chbInverseDCpidOutput_toggled(bool checked);
    void on_dsRASetpoint_valueChanged(double arg1);
    void on_dsRAGain_valueChanged(double arg1);
    void on_dsRAResetTime_valueChanged(double arg1);
    void on_dsRADerivativeTime_valueChanged(double arg1);
    void on_chbInverseRApidOutput_toggled(bool checked);    
    void on_dsRADeadBand_valueChanged(double arg1);

    void on_dsDCDeadBand_valueChanged(double arg1);

    void on_pbPauseRecording_toggled(bool checked);

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_pbStartTracking_toggled(bool checked);

    void on_rbZWOcameraGuidingPort_clicked(bool checked);

    void on_rbZWO_USBguidingPort_clicked(bool checked);

    void on_rbI2CguidingPort_clicked(bool checked);

    void on_rbI2CanalogOutput_clicked(bool checked);

    void on_pbTargetToCurrentCoordinates_clicked();

    void on_pbStartMovingToTarget_clicked();

    void on_chbTrackingControlEnabled_toggled(bool checked);

private:
    Ui::zwoStreaming *ui;

    QPixmap             vPixmap;
    QPixmap             vTrackPixmap;
    QImage              *vImage;
    long                vVBufferSize;
    long                vOutputBufferSize;
    long                vPreviewOutBufferSize;
    long                vTrackingImageBufferSize;
    int                 vExposure;
    QDateTime           vStartRcordTime;
    int                 vVideoRecordTime;
    int                 vCurrentFilterEstimatedTime;
    int                 vCurrentFilterRemainingTime;
    int                 vCurrentFilterElapsedTime;
    int                 vTotalEstimatedTime;
    int                 vTotalRemainingTime;
    int                 vTotalElapsedTime;
    int                 vSingleShotEstimatedTime;
    int                 vSingleShotElapsedTime;
    int                 vSingleShotRemainingTime;
    int                 vWait_ms;
    int                 vDroptFrames;
    int                 vCameraID_1;
    ASI_ERROR_CODE      vError;
    ASI_IMG_TYPE        vImgType_1;
    ASI_EXPOSURE_STATUS vExpStatus;
    QTimer              *vTimer1;
    QTimer              *vCountTimer;
    QTimer              *vRefreshTimer;
    QTimer              *vCalibrationTimer;
    FitsIOClass         *vFits;
    SerIOClass          *vSerFile;
    Ser_ColorIDs        vSerColorID;
    GetVideoThread      *vGetVideoThread;
    double              vScale;
    int                 vWidth_1;
    int                 vHeight_1;
    int                 vScaleChange;
    int                 vShotsNumber;
    int                 vTimeOutStatus = 0;
    bool                vVideoMode = false;
    bool                vPaused;
    IMG_FILE_FORMATS    vImageFileFormat;
    QImage::Format      vImgFormat;
    QString             vFileName;
    int                 vFileIndex = 0;
    QString             vEndTime;
    QString             vFilterName;

    QString             vObserver;
    QString             vInstrument;
    QString             vTelescope;

    uchar               *vVideoBuffer;
    uchar               *vOutputBuffer;
    uchar               *vPreviewInBuffer;
    uchar               *vPreviewOutBuffer;
    uchar               *vTrackingImageBuffer;
    cv::Mat         vMatIn;    //input matrix for CV processing
    cv::Mat         vMatOut;   //output matrix for CV processing
    cv::Mat         vMatInPreview;   //preview matrix for CV processing
    cv::Mat         vMatOutPreview;   //preview output matrix for CV processing
    cv::ColorConversionCodes    vBayerConversionCode;
    cv::ColorConversionCodes    vBayerConversionCodePreview;

    int                 vI;
    QString             vStr,vStr_1;
    faaImageProcessor   oImageProcessor;
    void            fVideoRender();
    void            fImageInit();
    void            fCreateFITSFile();
    void            fCreateImageFile();
    void            fCreateSerFile();
    QString         GetCountTimerString();
    void            SaveImageToFile();
    void            WriteImgFile(QString fileName);

    //tracking vars
    void    GetTrackingShift(QImage *image);
    void    DrawTrackPixmap(QImage *image);
    void    TrackingProc();
    void    setCalibrationData();
    void    setGuidingPortsEnabled(bool enabled);
    Tracking            *vTracking;
    cv::SimpleBlobDetector::Params  vBlobParams;
    int             vGuidingPorts;
    bool            vCalibrationImageRequest;
    bool            vTrackingRunning;
    bool            vTrackingDriversMoving;
    bool            vTrackingEnabled;

    //P2P move
    double getDecMovingTime();
    int getRAMovingTime();
    int timeFromString(QString string);
};

#endif // ZWOSTREAMING_H
